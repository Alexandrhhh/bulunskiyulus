<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLabelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('labels', function (Blueprint $table) {
            $table->id();
            $table->integer('file_id')->nullable();
            $table->integer('human_id')->nullable();
            $table->double('height')->nullable();
            $table->double('width')->nullable();
            $table->double('img_height')->nullable();
            $table->double('img_width')->nullable();
            $table->double('left')->nullable();
            $table->double('top')->nullable();
            $table->double('leftTopX')->nullable();
            $table->double('leftTopY')->nullable();
            $table->double('rightBottomX')->nullable();
            $table->double('rightBottomY')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('labels');
    }
}
