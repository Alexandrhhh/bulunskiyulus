<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roads', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->integer('color')->default(0);
            $table->timestamp('open_date')->nullable();
            $table->timestamp('close_date')->nullable();
            $table->string('max_tonns')->nullable();
            $table->string('technics')->nullable();
            $table->timestamp('next_clean')->nullable();
            $table->text('about')->nullable();
            $table->string('tracks')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roads');
    }
}
