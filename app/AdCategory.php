<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdCategory extends Model
{
    protected $fillable = ['title'];

    public function ads()
    {
        return $this->hasMany(Ad::class, 'category_id');
    }

    public function last3()
    {
        return \App\Ad::where('category_id', $this->id)->take(3)->latest()->get();
    }
}
