<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['text', 'user_id'];

    public function commentable()
    {
        return $this->morphTo('commetable');
    }

    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'shortguid');
    }

    public static function add_comment($model, $text = '')
    {
        return $model->comments()->save(
            new self([
                'text' => $text,
                'user_id' => \Auth::user()->id
            ])
        );
    }
}
