<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Poll extends Model
{
    protected $fillable = [
        'title', 'subtitle', 'type', 'end_date'
    ];

    public $dates = [
        'created_at', 'updated_at', 'end_date'
    ];

    public function answers()
    {
        return $this->hasMany(Answer::class);
    }

    public function all_count()
    {
        $i = 0;
        foreach ($this->answers as $key => $answer) {
            $i += $answer->count;
        }
        return $i;
    }

    public function answer_user()
    {
        return $this->hasMany(AnswerUser::class);
    }

    public function newses()
    {
        return $this->hasMany(News::class);
    }

    public function current_user()
    {
        // return $this->hasMany(AnswerUser::class)->where([
        //     ['user_id', '=', 0],
        //     ['poll_id', '=', $this->id]
        // ])->get();
        if(\Auth::check()) {
            return $this->hasMany(AnswerUser::class)->where([
                ['user_id', '=', \Auth::user()->id],
                ['poll_id', '=', $this->id]
            ]);
        }
        return false;
    }
}
