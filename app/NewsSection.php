<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsSection extends Model
{
    protected $fillable = [
        'title'
    ];

    public function newses()
    {
        return $this->hasMany(News::class, 'section_id');
    }
}
