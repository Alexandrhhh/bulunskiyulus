<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Schema\Builder;
use Illuminate\Database\Eloquent\Relations\Relation;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // $cities = \App\City::all();
        // $curl = curl_init();
        // foreach ($cities as $key => $city) {
        //     curl_setopt_array($curl, array(
        //       CURLOPT_URL => "https://api.gismeteo.net/v2/weather/current/".'?longitude='.$city->longitude.'&latitude='.$city->latitude,
        //       CURLOPT_RETURNTRANSFER => true,
        //       CURLOPT_ENCODING => "",
        //       CURLOPT_MAXREDIRS => 10,
        //       CURLOPT_TIMEOUT => 30,
        //       CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        //       CURLOPT_CUSTOMREQUEST => "GET",
        //       CURLOPT_HTTPHEADER => array(
        //         "Accept: application/json",
        //         "Accept-Encoding: gzip, deflate",
        //         "Content-Type: application/json; charset=utf-8",
        //         "cache-control: no-cache",
        //         "X-Gismeteo-Token: 5ea2ce22847055.90937220"
        //       ),
        //     ));
        //     $weather = json_decode(curl_exec($curl));
        //     if($weather) {
        //         $weather = $weather->response;
        //         \App\Weather::create([
        //             'city_id' => $city->id,
        //             'wind_id' => $weather->wind->direction->scale_8,
        //             'wind_speed' =>$weather->wind->speed->m_s,
        //             'temperature' => $weather->temperature->air->C,
        //             'about' => rtrim($weather->description->full, ', '),
        //             'icon_name' => $weather->icon
        //         ]);
        //     }
        // }
        date_default_timezone_set('Asia/Yakutsk');
        $cities = \App\City::all();
        $bus = \App\Transport\Bus::find(1);
        $mayak_vipusk = \App\Mayak::orderBy('publish_at', 'desc')->latest()->take(1)->first();
        $planes_block = \App\Plane::orderBy('departure_at', 'asc')->whereVisible(1)->take(2)->get();
        Relation::morphMap([
            'newses' => 'App\News',
            'comments' => 'App\Comment',
            'ads' => 'App\Ad',
            'events' => 'App\Event',
            'measures' => 'App\Measure'
        ]);
        Builder::defaultStringLength(191);
        view()->share(compact('cities', 'bus', 'mayak_vipusk', 'planes_block'));
    }
}
