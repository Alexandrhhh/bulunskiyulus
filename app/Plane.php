<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\PlaneCompany;

class Plane extends Model
{
    protected $fillable = [
        'departure',
        'departure_at',
        'flight',
        'arrival',
        'arrival_at',
        'time_at',
        'visible',
        'description',
        'status_id',
        'company_id'
    ];

    public $dates = [
        'created_at',
        'updated_at',
        'departure_at',
        'arrival_at'
    ];

    public function status()
    {
        return $this->belongsTo(PlaneStatus::class);
    }

    public function company()
    {
        return $this->belongsTo(PlaneCompany::class, 'company_id');
    }
}
