<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TopicMessage extends Model
{
    protected $fillable = [
        'user_id', 'topic_id', 'message'
    ];

    public function topic()
    {
        return $this->belongsTo(Topic::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'shortguid');
    }
}
