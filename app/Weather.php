<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Weather extends Model
{
    protected $fillable = [
        'city_id', 'wind_id', 'temperature', 'wind_speed', 'icon_name', 'about'
    ];
    
    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function wind()
    {
        return $this->belongsTo(Wind::class);
    }
}
