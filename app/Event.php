<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\EventSection;

class Event extends Model
{
    protected $fillable = [
        'title', 'category_id', 'short', 'long', 'user_id', 'date_event', 'style', 'section_id', 'style'
    ];

    public $dates = [
        'created_at', 'updated_at', 'date_event'
    ];

    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable');
    }

    public function likes()
    {
        return $this->morphMany('App\Like', 'liketable');
    }

    public function category()
    {
        return $this->belongsTo(EventCategory::class, 'category_id');
    }

    public function section()
    {
        return $this->belongsTo(EventSection::class, 'section_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'shortguid');
    }

    public function tags()
    {
        return $this->belongsToMany(EventTag::class);
    }
}
