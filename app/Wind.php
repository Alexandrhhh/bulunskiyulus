<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wind extends Model
{
    protected $fillable = ['title'];
}
