<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlaneCompany extends Model
{
    protected $fillable = ['title'];

    protected $guarded = ['logotype'];

    public function planes()
    {
        return $this->hasMany(Plane::class, 'company_id');
    }
}
