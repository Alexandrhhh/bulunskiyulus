<?php

namespace App\Http\Controllers;

use App\Notifications\InboxMessage;
use App\Http\Controllers\Controller;
use App\Http\Requests\AbuseFormRequest;
use App\Admin;

class AbuseFormController extends Controller
{
    public function mail_to_administration(AbuseFormRequest $message, Admin $admin)
    {
        $admin->notify(new InboxMessage($message));
        return redirect()->back()->with('success', 'Спасибо за обращение! Ваше сообщение успешно отправлено!');
    }
}
