<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdministrationController extends Controller
{
    public function index()
    {
        return view('administration.index');
    }

    public function complaint()
    {
        return view('administration.complaint');
    }

    public function measures()
    {
        $measures = \App\Measure::orderBy('created_at', 'desc')->get();
        return view('administration.measures', compact('measures'));
    }

    public function measure($id)
    {
        $measure = \App\Measure::find($id);
        $like = \App\Like::hasLike($measure);
        return view('administration.measure', compact('measure', 'like'));
    }
}
