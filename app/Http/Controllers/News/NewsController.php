<?php

namespace App\Http\Controllers\News;

use App\Http\Controllers\Controller;
use App\NewsSection;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class NewsController extends Controller
{
    public function index()
    {
        $sections = \App\NewsSection::all();
        foreach ($sections as $key => $section) {
            return redirect(route('news.section', ['id' => $section->id]));
        }
    }

    public function section($id)
    {
        $sections = \App\NewsSection::all();
        $newses = \App\News::where('section_id', $id)->orderBy('id', 'desc')->with('comments')->with('comments.comments')->paginate(10);
        // dd($newses);
        return view('news.index', compact('newses', 'sections'));
    }

    public function news($id)
    {
        $news = \App\News::find($id);
        $news->increment('reads');
        $like = \App\Like::hasLike($news);
        return view('news.news', compact('news', 'like'));
    }

    public function create(Request $request)
    {
        $news = \App\News::create($request->all() + ['user_id' => \Auth::user()->id]);
        $request->hasFile('image') ? $image_url = \Storage::putFile('public/images', $request->file('image')) : $image_url = null;
        if($request->has('tags')) {
            foreach ($request->tags as $key => $tag) {
                $tags[] = \App\NewsTag::firstOrCreate(['title'=> $tag])->id;
            }
            $news->tags()->attach($tags);
        }
        $news->image_url = $image_url;
        if($request->has('short')) $news->short = htmlentities($request->short);
        if($request->has('content')) $news->content = htmlentities($request->content);
        $news->save();
        return redirect(route('news.index'))->with('success', 'Новость успешно создана');
    }

    public function update(Request $request, $id)
    {
        $news = \App\News::find($id);
        $news->update($request->all());
        if($request->hasFile('image')) {
            \Storage::delete($news->image_url);
            $news->image_url = \Storage::putFile('public/images', $request->file('image'));
        }
        if($request->has('tags')) {
            $news->tags()->detach();
            foreach ($request->tags as $key => $tag) {
                $tags[] = \App\NewsTag::firstOrCreate(['title'=> $tag])->id;
            }
            $news->tags()->attach($tags);
        }
        if($request->has('short')) $news->short = htmlentities($request->short);
        if($request->has('content')) $news->content = htmlentities($request->content);
        $news->save();
        return redirect(route('news.index'))->with('success', 'Новость успешно обновлена');
    }
}
