<?php

namespace App\Http\Controllers\Event;

use App\Http\Controllers\Controller;
use App\EventSection;
use Illuminate\Database\Eloquent\Builder;

use Illuminate\Http\Request;

class EventController extends Controller
{
    public function index()
    {
        $sections = \App\EventSection::all();
        foreach ($sections as $key => $section) {
            return redirect(route('events.section', ['section_id' => $section->id]));
        }
    }

    public function section($section_id)
    {
        $sections = \App\EventSection::all();
        $events = \App\Event::where('section_id', $section_id)->paginate(12);
        $films = \App\Film::whereHas('sessions', function(Builder $query) {
            $query->where('display_at', '>', now()->format('Y-m-d 00:00:00'));
            $query->where('display_at', '<', now()->addDays(7)->format('Y-m-d 00:00:00'));
        })->latest()->take(5)->get();
        return view('events.index', compact('events', 'sections', 'films'));
    }

    public function event($id)
    {
        $event = \App\Event::find($id);
        $event->increment('reads');
        $like = \App\Like::hasLike($event);
        return view('events.event', compact('event', 'like'));
    }
}
