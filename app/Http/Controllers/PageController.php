<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Weather;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class PageController extends Controller
{
    public function index()
    {
        $newses = \App\News::take(3)->latest()->get();
        $newses_bulunsk = \App\News::where('section_id', 2)->take(5)->latest()->get();
        $ads = \App\Ad::orderBy('created_at', 'desc')->take(3)->latest()->get();
        $topics = \App\Topic::orderBy('created_at', 'desc')->take(9)->latest()->get();
        $films = \App\Film::whereHas('sessions', function(Builder $query) {
            $query->where('display_at', '>', now()->format('Y-m-d 00:00:00'));
            $query->where('display_at', '<', now()->addDays(7)->format('Y-m-d 00:00:00'));
        })->latest()->take(5)->get();
        return view('index', compact('newses', 'newses_bulunsk', 'ads', 'topics', 'films'));
    }

    public function login(Request $request)
    {
        $user = \App\User::where([['user_name', $request->login],['user_pswd', $request->password]])->first();
        if($user) {
            \Auth::loginUsingId($user->shortguid, 1);
            return redirect()->back();
        } else {
            return redirect()->back();
        }
    }

    public function logout(Request $request) {
        \Auth::logout();
        return redirect(route('index'));
    }

    public function road()
    {
        $roads = \App\Road::all();
        return view('road', compact('roads'));
    }

    public function change_city(Request $request, $id)
    {
        if(\Auth::check()) {
            $user = \Auth::user();
            $user->city_id = $id;
            $user->save();
        }

        return back();
    }

    public function bus()
    {
        $bus = \App\Transport\Bus::find(1);
        return view('bus', compact('bus'));
    }

    public function weather()
    {
        $weather = \App\Setting::whereTitle('weather')->first();
        $weather = json_decode($weather->setting)->response;
        $weather = collect($weather);
        $weather = $weather->groupBy(function($item) {
            return now()->
            setTimestamp(strtotime($item->date->local))
            ->format('d.m.Y');
        });
        return view('weather', compact('weather'));
    }

    public function aero()
    {
        $planes = \App\Plane::orderBy('departure_at', 'asc')->whereVisible(1)->get();
        return view('aero', compact('planes'));
    }

    public function afisha()
    {
        $films = \App\Film::orderBy('created_at')->get();
        return view('afisha', compact('films'));
    }

    public function download_file($id)
    {
        $file = \App\MeasureFile::find($id);
        return \Storage::disk('public')->download($file->url, $file->title);
    }
}
