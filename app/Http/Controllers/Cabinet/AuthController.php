<?php

namespace App\Http\Controllers\Cabinet;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function index()
	{
		return view('cabinet.auth');
	}

	public function register()
	{
		return view('cabinet.register');
	}
}
