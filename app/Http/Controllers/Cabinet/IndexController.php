<?php

namespace App\Http\Controllers\Cabinet;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index()
	{
//	    $device = \Auth::user()->getTrafficDevice();
		return view('cabinet.index');
	}
}
