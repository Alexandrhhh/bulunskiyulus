<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Archive\Human;
use App\Archive\Label;

class TagController extends Controller
{
    public function add(Request $request)
    {

        $human = Human::firstOrCreate([
            'title' => $request->item_title
        ]);
        $label = Label::create([
            'file_id' => 0,
            'human_id' => $human->id,
            'height' => $request->height,
            'width' => $request->width,
            'img_height' => $request->img_height,
            'img_width' => $request->img_width,
            'left' => $request->left,
            'top' => $request->top,
            'leftTopX' => $request->leftTopX,
            'leftTopY' => $request->leftTopY,
            'rightBottomX' => $request->rightBottomX,
            'rightBottomY' => $request->rightBottomY
        ]);
        return collect(['tag_id' => $label->id, 'success' => true])->toJson();
    }

    public function remove(Request $request)
    {
        return ['tag_id' => rand(0, 9999)];
    }
}
