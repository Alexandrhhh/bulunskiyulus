<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        return view('user.index');
    }

    public function update(Request $request)
    {
        $user = \Auth::user();
        if($request->has('first_name')) $user->first_name = $request->first_name;
        if($request->has('middle_name')) $user->middle_name = $request->middle_name;
        if($request->has('last_name')) $user->last_name = $request->last_name;
        if($request->has('date_birth')) $user->date_birth = $request->date_birth;
        if($request->file('photo_id')) {
            if($user->photo_id) \Storage::delete($user->photo_id);
            $user->photo_id = \Storage::disk('public')->putFile('avatars', $request->photo_id);
        }
        $user->save();
        return redirect(route('lk.index'))->with('success', 'Профиль обновлен');
    }

    public function news()
    {
        $newses = \App\News::where('user_id', \Auth::user()->id)->paginate(1);
        return view('user.news', compact('newses'));
    }

    public function events()
    {
        $events = \App\Event::where('user_id', \Auth::user()->id)->paginate(1);
        return view('user.events', compact('events'));
    }

    public function ads()
    {
        $ads = \App\Ad::where('user_id', \Auth::user()->id)->paginate(1);
        return view('user.ads', compact('ads'));
    }

    public function balance()
    {
        return view('user.balance');
    }

    public function traffic_info(Request $request)
    {
        if($request->has('date')) {
            $dates = explode(' - ', $request->date);
            $startdate = now()->setTimestamp(strtotime($dates[0]));
            $enddate = now()->setTimestamp(strtotime($dates[1]))->addDay();
        } else {
            $startdate = now()->subDays(5);
            $enddate = now()->addDay();
        }
        $traffics = [
            'dates' => array(),
            'traff' => array()
        ];
        while($startdate < $enddate) {
            $traffics['dates'][] = $startdate->format('d.m');
            if(\Schema::connection('mysql2')->hasTable('trafflow2_'.$startdate->format('Y-m-d'))) {
                $traff = \DB::connection('mysql2')->table('trafflow2_'.$startdate->format('Y-m-d'))->where('usr', \Auth::user()->shortguid2)->get()->sum('trafsize');
                $traffics['traff'][] = ($traff > 0) ? round($traff/1048576, 2) : 0;
            } else {
                $traffics['traff'][] = 0;
            }

            $startdate = $startdate->addDay();
        }
        return view('user.traffic_info', compact('traffics'));
    }

    public function tariff()
    {
        $followingtarifs = \DB::connection('mysql2')->table('tarifs')->where('tarif_guid', \Auth::user()->tarif_guid)->first()->followingtarifs;
        $tariffs = [];
        if(strlen($followingtarifs) > 3) {
            $followingtarifs = explode('||', $followingtarifs);
            foreach ($followingtarifs as $key => $name) {
                if($name != \Auth::user()->tarif) {
                    $tariffs[] =  \DB::connection('mysql2')->table('tarifs')->where('tarif_name', $name)->first();
                }
            }
        }
        return view('user.tariff_info', compact('tariffs'));
    }

    public function documents()
    {
        return view('user.documents');
    }

    public function change_tariff(Request $request)
    {
        if($request->has('tarif') && \DB::connection('mysql2')->table('tarifs')->where('tarif_name', $request->tarif)->get()->count()) {
            $tarif = \DB::connection('mysql2')->table('tarifs')->where('tarif_name', $request->tarif)->first();
            if(intval(explode(' ', \Auth::user()->ballance)) > intval($tarif->changetarifcost)) {
                $uid = uniqid();
                \App\ActionLog::create([
                    'dateday' => now()->format('Y-m-d H:i:s'),
                    'action' => 'CHANGE_TARIF',
                    'value1' => \Auth::user()->user_name,
                    'value2' => $tarif->tarif_name,
                    'value3' => $uid,
                    'initiator' => \Auth::user()->user_name
                ]);

                App\Action::create([
                    'actiontext' => 'CHANGE_TARIF',
                    'value1' => \Auth::user()->user_name,
                    'value2' => $tarif->tarif_name,
                    'value3' => now()->format('Y-m-d H:i:s'),
                    'guid' => $uid
                ]);

                return redirect(route('lk.tariff'))->with('success', 'Заявка на изменение тарифа принята');

            } else {
                return redirect(route('lk.tariff'))->with('error', 'Недостаточно средств для изменения тарифа');
            }
        }
        return redirect(route('lk.tariff'))->with('error', 'Произошла ошибка, попробуйте немного позже');
    }

    public function yandex_pay(Request $request)
    {
        if($request->has('sum') && $request->sum > 0) {
            $shop_id = 514377;
            $shop_key = 'live_QENxmdy_mnA9o_ItQiQWEkNmnw47Z7y1kLf-zQ29uU4';
            $pinfoA = explode('||',\Auth::user()->pinfo);
            $parameters='{
    	        "amount": {
    	          "value": "'.$request->sum.'",
    	          "currency": "RUB"
    	        },
    	        "confirmation": {
    	          "type": "redirect",
    	          "return_url": "'.url()->previous().'"
    	        },
    	        "capture": true,
    	        "description": "Payment '.date("d.m.y H:i:s").' for  '. \Auth::user()->shortguid .'",
    			"receipt": {
    			  "customer": {
    				"full_name": "'.\Auth::user()->FIO.'",
    				"phone": "'.$pinfoA[0].'"
    			  },
    			  "items": [
    				{
    				  "description": "'.\Auth::user()->tarif.'",
    				  "quantity": "1.00",
    				  "amount": {
    					"value": "'.$request->sum.'",
    					"currency": "RUB"
    				  },
    				  "vat_code": "2",
    				  "payment_mode": "full_prepayment",
    				  "payment_subject": "commodity"
    				}
    			  ]
    			}
    	    }';

            $ch=curl_init();

    		curl_setopt($ch,CURLOPT_URL,'https://payment.yandex.net/api/v3/payments');

    		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; ru-RU; rv:1.7.12) Gecko/20050919 Firefox/1.0.7");
    		curl_setopt($ch, CURLOPT_TIMEOUT, 8);
    		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,0);
    		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,0);
    		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

    		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
    		curl_setopt($ch,CURLOPT_POST,1);
    		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    			'Idempotence-Key: MB-'.\Str::random(5).'-'.\Auth::user()->shortguid,
    			'Content-Type: application/json'
    		));

    		curl_setopt($ch,CURLINFO_HEADER_OUT,true);
    		curl_setopt($ch,CURLOPT_POSTFIELDS,$parameters);
    		curl_setopt($ch,CURLOPT_USERPWD,"$shop_id:$shop_key");


    		$r=curl_exec($ch);
    		$info = curl_getinfo($ch);
    		$error = curl_error($ch);
    		curl_close($ch);

    		$r=json_decode($r,true);

            if (strlen($error)>0){
    			echo 'Error = ' . $error;
    			exit();
    		}

    		if(isset($r['confirmation']['confirmation_url'])){
                \App\InvoiceYAKassa::create([
                    'shortguid' => \Auth::user()->shortguid,
                    'sum' => $request->sum,
                    'operation_id' => $r['id'],
                    'status' => $r['status'],
                    'actiondate' => date("Y-m-d H:i:s")
                ]);
                return redirect($r['confirmation']['confirmation_url']);
            }
        }
    }
}
