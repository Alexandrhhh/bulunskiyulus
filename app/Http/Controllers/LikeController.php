<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LikeController extends Controller
{
    public function addLike(Request $request)
    {
        $class = '\\'.$request->class;
        $model = $class::find($request->id);
        $like = \App\Like::addLike($model, $request->user);
        return true;
    }
}
