<?php

namespace App\Http\Controllers\Ad;

use App\Http\Controllers\Controller;
use App\AdCategory;
use App\AdPhoto;
use Illuminate\Http\Request;

class AdController extends Controller
{
    public function index()
    {
        $categories = \App\AdCategory::all();
        return view('ads.index', compact('categories'));
    }

    public function category($category_id)
    {
        $category = \App\AdCategory::find($category_id);
        $ads = \App\Ad::where('category_id', $category_id)->orderBy('created_at', 'desc')->paginate(10);
        return view('ads.category', compact('ads', 'category'));
    }

    public function ad($id)
    {
        $ad = \App\Ad::find($id);
        $ad->increment('reads');
        $like = \App\Like::hasLike($ad);
        return view('ads.ad', compact('ad', 'like'));
    }

    public function add()
    {
        $categories = \App\AdCategory::all();
        return view('ads.new', compact('categories'));
    }

    public function create(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'category_id' => 'required',
            'city_id' => 'required',
            'price' => 'min:0'
        ]);
        $ad = \App\Ad::create($request->all() + ['user_id' => \Auth::user()->id]);
        if($request->has('images')) {
            foreach ($request->images as $key => $image) {
                $url = \Storage::disk('public')->putFile('images', $image);
                \App\AdPhoto::create([
                    'url' => $url,
                    'ad_id' => $ad->id
                ]);
            }
        }
        return redirect(route('ads.index'))->with('success', 'Объявление успешно добавлено');
    }

    public function update(Request $request, $id)
    {
        $ad = \App\Ad::find($id);
        if($request->has('images')) {
            foreach ($ad->photos as $key => $photo) {
                \Storage::disk('public')->delete($photo->url);
                $photo->delete();
            }
            foreach ($request->images as $key => $image) {
                $url = \Storage::disk('public')->putFile('images', $image);
                \App\AdPhoto::create([
                    'url' => $url,
                    'ad_id' => $ad->id
                ]);
            }
        }
        $ad->update($request->all());
        return redirect(route('ads.index'))->with('success', 'Объявление успешно обновлено!');
    }
}
