<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function create(Request $request)
    {
        if(mb_strlen($request->text) > 3) {
            $class = '\\'.$request->class;
            $model = $class::find($request->id);
            $comment = \App\Comment::add_comment($model, $request->text);
            return redirect()->back()->with('success', 'Коментарий успешно добавлен');
        } else {
            return redirect()->back()->with('error', 'Коментарий слишком короткий');
        }
    }
}
