<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\MeasureFile;
use App\Measure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class MeasureController extends Controller
{
    public function index()
    {
        $measures = Measure::orderBy('created_at', 'desc')->get();
        return view('admin.administration.measure.index', compact('measures'));
    }

    public function add()
    {
        return view('admin.administration.measure.add');
    }

    public function create(Request $request)
    {
        $measure = Measure::create($request->all() + ['user_id' => \Auth::check() ? \Auth::user()->id : 1]);
        if($request->has('content')) $measure->content = htmlentities($request->content);
        if($request->hasFile('image')) $measure->image = \Storage::disk('public')->putFile('images', $request->image);
        $measure->save();
        if($request->has('docs')) {
            foreach ($request->docs as $key => $file) {
                $title = $file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $url = \Storage::disk('public')->putFileAs('files', $file, \Str::random(10).$title);
                $newfile = MeasureFile::create([
                    'title' => $title,
                    'extension' => $extension,
                    'url' => $url,
                    'measure_id' => $measure->id
                ]);
            }
        }

        return redirect(route('admin.administration.measure.index'))->with('success', 'Успешно добавлено');
    }

    public function edit($id)
    {
        $measure = Measure::find($id);
        return view('admin.administration.measure.edit', compact('measure'));
    }

    public function update(Request $request, $id)
    {
        $measure = Measure::find($id);
        $measure->update($request->all());
        if($request->has('content')) $measure->content = htmlentities($request->content);
        if($request->hasFile('image')) {
            if($measure->image) \Storage::delete($measure->image);
            $measure->image = \Storage::disk('public')->putFile('images', $request->image);
        }
        if($request->has('docs')) {
            if($measure->files) {
                foreach ($measure->files as $key => $file) {
                    \Storage::delete($file->url);
                    $file->delete();
                }
            }
            foreach ($request->docs as $key => $file) {
                $title = $file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $url = \Storage::disk('public')->putFileAs('files', $file, \Str::random(10).$title);
                $newfile = MeasureFile::create([
                    'title' => $title,
                    'extension' => $extension,
                    'url' => $url,
                    'measure_id' => $measure->id
                ]);
            }
        }
        $measure->save();
        return redirect(route('admin.administration.measure.index'))->with('success', 'Успешно обновлено');
    }

    public function delete($id)
    {
        $measure = Measure::find($id);
        foreach ($measure->files as $key => $file) {
            \Storage::delete($file->url);
            $file->delete();
        }
        if($measure->image) \Storage::delete($measure->image);
        $measure->delete();

        return redirect(route('admin.administration.measure.index'))->with('success', 'Успешно удалено');
    }
}
