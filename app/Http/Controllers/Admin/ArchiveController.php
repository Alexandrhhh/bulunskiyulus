<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Archive\Section;
use App\Archive\File;
use Illuminate\Http\Request;

class ArchiveController extends Controller
{
    public function sections()
    {
        $sections = Section::all();

        return view('admin.archive.files.index', compact('sections'));
    }

    public function create_section(Request $request)
    {
        $request->validate([
            'title' => 'required|unique:sections',
            'image' => 'required|file'
        ]);
        $section = Section::create([
            'title' => $request->title,
            'image' => \Storage::disk('public')->putFile('images', $request->image)
        ]);

        return redirect(route('admin.archive.sections.index'));
    }

    public function edit_section($id)
    {
        $section = Section::find($id);

        return view('admin.archive.sections.edit', compact('section'));
    }

    public function update_section(Request $request, $id)
    {
		$section = Section::find($id);
        if($request->hasFile('image')) {
            if($section->image) \Storage::delete($section->image);
            $section->image = \Storage::disk('public')->putFile('images', $request->image);
        }
        $section->title = $request->title;
        $section->save();

        return redirect(route('admin.archive.sections.index'));
    }

    public function delete_section($id)
    {
        $section = Section::find($id);
        foreach ($section->files as $key => $file) {
            $file->labels()->delete();
            if($file->file) \Storage::delete($file->file);
            $file->delete();
        }
        if($section->image) \Storage::delete($section->image);
        $section->delete();

        return redirect(route('admin.archive.sections.index'));
	}

	public function show($id)
	{
		$files = File::whereSectionId($id)->orderBy('created_at', 'desc')->get();
		return view('admin.archive.files.files', compact('files'));
	}

	public function delete_file(Request $request, $id)
	{
		$file = File::find($id);
		if($file->type) {
			if($file->preview) \Storage::delete($file->preview);
		}
		if($file->file) \Storage::delete($file->file);
		$file->delete();
		return redirect()->back()->with('success', 'Файл успешно удалён');
	}

	public function show_file($id)
	{
		$file = File::find($id);
		return view('admin.archive.files.edit', compact('file'));
	}

	public function update_file(Request $request, $id)
	{
		$file = File::find($id);
		$file->update($request->all());
		$file->save();
		return redirect(route('admin.archive.sections.show', ['id' => $file->section->id]))->with('success', 'Файл успешно обновлен');
	}
}
