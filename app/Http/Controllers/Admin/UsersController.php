<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    public function all()
    {
        $users = \App\User::all();
        return view('admin.users.all', compact('users'));
    }

    public function edit($id)
    {
        $user = \App\User::find($id);
        $groups = \App\Group::all();
        return view('admin.users.edit', compact('user', 'groups'));
    }

    public function update(Request $request, $id)
    {
        $user = \App\User::find($id);
        $user->group_id = $request->group_id;
        $user->email = $request->email;
        $user->save();
        return redirect(route('admin.users.all'))->with('success', "Пользователь <strong>{$user->email}</strong> успешно обновлён.");
    }
}
