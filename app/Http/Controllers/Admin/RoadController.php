<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RoadController extends Controller
{
    public function index()
    {
        $roads = \App\Road::all();
        return view('admin.road.index', compact('roads'));
    }

    public function add()
    {
        return view('admin.road.add');
    }

    public function create(Request $request)
    {
        \App\Road::create($request->all());
        return redirect(route('admin.road.all'))->with('success', $request->title.' успешно создана');
    }

    public function edit($id)
    {
        $road = \App\Road::find($id);
        return view('admin.road.edit', compact('road'));
    }

    public function update(Request $request, $id)
    {
        $road = \App\Road::find($id);
        $road->update($request->all());
        return redirect(route('admin.road.all'))->with('success', $road->title.' успешно обновлена');
    }

    public function delete($id)
    {
        \App\Road::destroy($id);
        return redirect(route('admin.road.all'))->with('success', 'Дорога успешно удалена');
    }
}
