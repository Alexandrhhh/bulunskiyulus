<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SystemController extends Controller
{
    public function colors()
    {
        $colors = \App\Color::all();
        return view('admin.system.color.all', compact('colors'));
    }

    public function create_color(Request $request)
    {
        \App\Color::create($request->all());
        return redirect(route('admin.system.color.all'))->with('Цвет успешно добавлен');
    }
}
