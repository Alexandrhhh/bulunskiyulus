<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\NewsCategory;

class NewsController extends Controller
{
    public function all()
    {
        $news = \App\News::all();
        return view('admin.news.all', compact('news'));
    }

    public function new()
    {
        $categories = \App\NewsCategory::all();
        $sections = \App\NewsSection::all();
        $tags = \App\NewsTag::all();
        $polls = \App\Poll::orderBy('created_at', 'desc')->get();
        return view('admin.news.new', compact('categories', 'tags', 'sections', 'polls'));
    }

    public function edit($id)
    {
        $categories = \App\NewsCategory::all();
        $tags = \App\NewsTag::all();
        $news = \App\News::find($id);
        $polls = \App\Poll::orderBy('created_at', 'desc')->get();
        return view('admin.news.edit', compact('categories', 'tags', 'news', 'polls'));
    }

    public function delete(Request $request, $id)
    {
        $news = \App\News::find($id);
        $title = $news->title;
        $news->delete();
        return redirect(route('admin.news.all'))->with('success', 'Новость <strong>'.$title.'</strong>, успешно удалена!');
    }

    //Управление категориями
    public function all_categories()
    {
        $categories = \App\NewsCategory::all();
        $colors = \App\Color::all();
        return view('admin.news.categories', compact('categories', 'colors'));
    }

    public function edit_category($id)
    {
        $category = \App\NewsCategory::find($id);
        $colors = \App\Color::all();
        return view('admin.news.edit_category', compact('category', 'colors'));
    }

    public function update_category(Request $request, $id)
    {
        $category = \App\NewsCategory::find($id);
        $category->update($request->all());
        return redirect(route('admin.news.category.all'))->with('success', 'Категория успешно обновлена');
    }

    public function create_category(Request $request)
    {
        $request->validate([
            'title' => 'required|unique:news_categories',
        ]);
        $category = \App\NewsCategory::create($request->all());
        if(!$category) {
            return redirect(route('admin.news.category.all'))->with('error', 'При добавлении категории <strong>'.$request->title.'</strong> произошла ошибка');
        } else {
            return redirect(route('admin.news.category.all'))->with('success', 'Категория <strong>'.$category->title.'</strong> успешно добавлена');
        }
    }

    public function delete_category(Request $request, $id)
    {
        $category = \App\NewsCategory::find($id);
        $count = $category->newses->count();
        foreach ($category->newses as $key => $news) {
            $news->delete();
        }
        $title = $category->title;
        $category->delete();
        return redirect(route('admin.news.category.all'))->with('success', 'Категория '.$title.' удалена и '.$count.' новостей которые в ней были');
    }

    //Управление тегами
    public function all_tags()
    {
        $tags = \App\NewsTag::all();
        return view('admin.news.tags', compact('tags'));
    }

    public function create_tag(Request $request)
    {
        $request->validate([
             'title' => 'required|unique:news_tags'
        ]);
        $tag = \App\NewsTag::create($request->all());
        if(!$tag) {
            return redirect(route('admin.news.tags.all'))->with('error', 'При добавлении тега <strong>'.$request->title.'</strong> произошла ошибка.');
        } else {
            return redirect(route('admin.news.tags.all'))->with('success', 'Тег <strong>'.$request->title.'</strong> успешно добавлен.');
        }
    }

    //Управление разделами
    public function all_sections()
    {
        $sections = \App\NewsSection::all();
        return view('admin.news.section', compact('sections'));
    }

    public function create_section(Request $request)
    {
        $request->validate([
            'title' => 'required|unique:news_sections',
        ]);
        $section = \App\NewsSection::create($request->all());
        if(!$section) {
            return redirect(route('admin.news.sections.all'))->with('error', 'При добавлении раздела <strong>'.$request->title.'</strong> произошла ошибка');
        } else {
            return redirect(route('admin.news.sections.all'))->with('success', 'Раздел <strong>'.$section->title.'</strong> успешно добавлен');
        }
    }

    public function delete_section(Request $request, $id)
    {
        $section = \App\NewsSection::find($id);
        $count = $section->newses->count();
        foreach ($section->newses as $key => $news) {
            $news->delete();
        }
        $title = $section->title;
        $section->delete();
        return redirect(route('admin.news.category.all'))->with('success', 'Категория '.$title.' удалена и '.$count.' новостей которые в ней были');
    }
}
