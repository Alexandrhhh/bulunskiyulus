<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\EventSection;
use App\EventTag;

class EventsController extends Controller
{
    public function all()
    {
        $events = \App\Event::all();
        return view('admin.events.all', compact('events'));
    }

    public function new()
    {
        $categories = \App\EventCategory::all();
        $sections = \App\EventSection::all();
        $tags = \App\EventTag::all();
        return view('admin.events.new', compact('categories', 'tags', 'sections'));
    }
    public function edit($id)
    {
        $event = \App\Event::find($id);
        $categories = \App\EventCategory::all();
        $sections = \App\EventSection::all();
        $tags = \App\EventTag::all();
        return view('admin.events.edit', compact('event', 'categories', 'tags', 'sections'));
    }

    public function create(Request $request)
    {
        $event = \App\Event::create($request->all() + ['user_id' => \Auth::user()->id]);
        $request->hasFile('image') ? $image_url = \Storage::putFile('public/images', $request->file('image')) : $image_url = null;
        if($request->has('tags')) {
            foreach ($request->tags as $key => $tag) {
                $tags[] = \App\EventTag::firstOrCreate(['title'=> $tag])->id;
            }
            $event->tags()->attach($tags);
        }
        $event->image = $image_url;
        $event->short = htmlentities($request->short);
        $event->long = htmlentities($request->long);
        $event->save();
        return redirect(route('admin.events.all'))->with('success', 'Мероприятие успешно создано');
    }

    public function update(Request $request, $id)
    {
        $event = \App\Event::find($id);
        $event->update($request->all());
        if($request->hasFile('image')) {
            if($event->image) \Storage::delete($event->image);
            $event->image = \Storage::putFile('public/images', $request->file('image'));
        }
        if($request->has('tags')) {
            foreach ($request->tags as $key => $tag) {
                $tags[] = \App\EventTag::firstOrCreate(['title'=> $tag])->id;
            }
            $event->tags()->detach();
            $event->tags()->attach($tags);
        }
        $event->short = htmlentities($request->short);
        $event->long = htmlentities($request->long);
        $event->save();
        return redirect(route('admin.events.all'))->with('success', 'Мероприятие успешно обновлено');
    }

    public function delete($id)
    {
        $event = \App\Event::find($id);
        if($event->image) \Storage::delete($event->image);
        $event->delete();
        return redirect(route('admin.events.all'))->with('success', 'Мероприятие успешно удалено');
    }

    public function all_categories()
    {
        $categories = \App\EventCategory::all();
        $colors = \App\Color::all();
        return view('admin.events.categories', compact('categories', 'colors'));
    }

    public function edit_category($id)
    {
        $category = \App\EventCategory::find($id);
        $colors = \App\Color::all();
        return view('admin.events.edit_category', compact('category', 'colors'));
    }

    public function update_category(Request $request, $id)
    {
        $category = \App\EventCategory::find($id);
        $category->update($request->all());
        return redirect(route('admin.events.categories.all'))->with('success', 'Категория успешно обновлена');
    }

    public function create_category(Request $request)
    {
        $request->validate([
            'title' => 'required|unique:event_categories'
        ]);
        $category = \App\EventCategory::create($request->all());
        if(!$category) {
            return redirect(route('admin.events.categories.all'))->with('error', 'При добавлении категории <strong>'.$request->title.'</strong> произошла ошибка');
        } else {
            return redirect(route('admin.events.categories.all'))->with('success', 'Категория <strong>'.$category->title.'</strong> успешно добавлена');
        }
    }

    public function delete_category(Request $request, $id)
    {
        $category = \App\EventCategory::find($id);
        $category->events()->delete();
        $title = $category->title;
        $category->delete();
        return redirect(route('admin.events.categories.all'))->with('success', 'Категория <strong>'.$title.'</strong> успешно удалена!');
    }

    public function all_sections()
    {
        $sections = \App\EventSection::all();
        return view('admin.events.sections', compact('sections'));
    }

    public function create_section(Request $request)
    {
        $request->validate([
            'title' => 'required|unique:event_sections'
        ]);
        $section = \App\EventSection::create($request->all());
        if(!$section) {
            return redirect(route('admin.events.sections.all'))->with('error', 'При добавлении категории <strong>'.$request->title.'</strong> произошла ошибка');
        } else {
            return redirect(route('admin.events.sections.all'))->with('success', 'Раздел <strong>'.$section->title.'</strong> успешно добавлена');
        }
    }

    public function delete_section(Request $request, $id)
    {
        $section = \App\EventSection::find($id);
        $section->events()->delete();
        $title = $section->title;
        $section->delete();
        return redirect(route('admin.events.sections.all'))->with('success', 'Раздел <strong>'.$title.'</strong> успешно удален!');
    }
}
