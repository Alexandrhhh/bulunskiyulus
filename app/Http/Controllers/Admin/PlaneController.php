<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\PlaneStatus;
use App\PlaneCompany;
use App\Plane;
use Illuminate\Http\Request;

class PlaneController extends Controller
{
    public function index()
    {
        $planes = Plane::all();
        return view('admin.road.plane.index', compact('planes'));
    }

    public function add()
    {
        $statuses = PlaneStatus::all();
        $companies = PlaneCompany::all();
        return view('admin.road.plane.add', compact('statuses', 'companies'));
    }

    public function create(Request $request)
    {
        $plane = \App\Plane::create($request->has('visible') ? $request->all() : $request->all() + ['visible' => 0]);
        return redirect(route('admin.road.plane.index'))->with('success', 'Рейс успешно создан');
    }

    public function edit($id)
    {
        $plane = Plane::find($id);
        $companies = PlaneCompany::all();
        $statuses = PlaneStatus::all();
        return view('admin.road.plane.edit', compact('plane', 'companies', 'statuses'));
    }

    public function update(Request $request, $id)
    {
        $plane = \App\Plane::find($id);
        $plane->update($request->has('visible') ? $request->all() : $request->all() + ['visible' => 0]);
        return redirect(route('admin.road.plane.index'))->with('success', 'Рейс успешно обновлен');
    }

    public function delete($id)
    {
        $plane = \App\Plane::find($id);
        $plane->delete();
        return redirect(route('admin.road.plane.index'))->with('success', 'Рейс успешно удален');
    }

    public function statuses()
    {
        $statuses = PlaneStatus::all();
        return view('admin.road.plane.statuses', compact('statuses'));
    }

    public function create_status(Request $request)
    {
        PlaneStatus::create($request->all());
        return redirect(route('admin.road.plane.status.index'))->with('success', 'Успешно создано');
    }

    public function edit_status($id)
    {
        $status = PlaneStatus::find($id);
        return view('admin.road.plane.edit_status', compact('status'));
    }

    public function update_status(Request $request, $id)
    {
        $status = PlaneStatus::find($id);
        $status->update($request->all());
        return redirect(route('admin.road.plane.status.index'))->with('success', 'Успешно обновлен');
    }

    public function companies()
    {
        $companies = PlaneCompany::all();
        return view('admin.road.plane.companies', compact('companies'));
    }

    public function create_company(Request $request)
    {
        $request->hasFile('logotype') ? $logotype = \Storage::disk('public')->putFile('images', $request->logotype) : $logotype = null;
        $company = PlaneCompany::create($request->all());
        $company->logotype = $logotype;
        $company->save();
        return redirect(route('admin.road.plane.company.index'))->with('success', 'Успешно создано');
    }

    public function edit_company($id)
    {
        $company = PlaneCompany::find($id);
        return view('admin.road.plane.edit_company', compact('company'));
    }

    public function update_company(Request $request, $id)
    {
        $company = PlaneCompany::find($id);
        $company->update($request->all());
        if($request->hasFile('logotype')) {
            $company->logotype = \Storage::disk('public')->putFile('images', $request->logotype);
        }
        $company->save();
        return redirect(route('admin.road.plane.company.index'))->with('success', 'Успешно обновлен');
    }
}
