<?php

namespace App\Http\Controllers\Admin\Transport;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class StopDayController extends Controller
{
    public function index()
    {
        // code...
    }

    public function add()
    {
        // code...
    }

    public function create(Request $request)
    {
        $day = \App\Transport\StopDay::create($request->all());
        return redirect(route('admin.road.buses.all'))->with("success", "День {$day->title} успешно создан");
    }

    public function edit($id)
    {
        // code...
    }

    public function update(Request $request, $id)
    {
        // code...
    }
}
