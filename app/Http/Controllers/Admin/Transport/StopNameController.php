<?php

namespace App\Http\Controllers\Admin\Transport;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class StopNameController extends Controller
{
    public function index()
    {
        // code...
    }

    public function add()
    {
        // code...
    }

    public function create(Request $request)
    {
        $name = \App\Transport\StopName::create($request->all());
        return redirect(route('admin.road.buses.all'))->with("success", "Остановка {$name->title} успешно создана");
    }

    public function edit($id)
    {
        // code...
    }

    public function update(Request $request, $id)
    {
        // code...
    }
}
