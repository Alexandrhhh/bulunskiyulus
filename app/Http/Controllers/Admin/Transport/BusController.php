<?php

namespace App\Http\Controllers\Admin\Transport;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BusController extends Controller
{
    public function index()
    {
        $buses = \App\Transport\Bus::all();
        return view('admin.road.bus.buses', compact('buses'));
    }

    public function create(Request $request)
    {
        $bus = \App\Transport\Bus::create($request->all());
        return redirect(route('admin.road.buses.edit', ['id' => $bus->id]));
    }

    public function edit($id)
    {
        $bus = \App\Transport\Bus::find($id);
        $stopnames = \App\Transport\StopName::all();
        $stopdays = \App\Transport\StopDay::all();
        return view('admin.road.bus.edit', compact('bus', 'stopnames', 'stopdays'));
    }

    public function update(Request $request, $id)
    {
        $bus = \App\Transport\Bus::find($id);
        $bus->stops()->delete();
        foreach ($request->stop['title'] as $key => $title) {
            if($title != null) {
                \App\Transport\Stop::create([
                    'bus_id' => $bus->id,
                    'stop_name_id' => $title,
                    'weekend' => $request->stop['weekend'][$key],
                    'stop_day_id' => $request->stop['days'][$key],
                    'time' => $request->stop['time'][$key]
                ]);
            }
        }
        return redirect(route('admin.road.buses.all'))->with("success", "Автобус {$bus->title} успешно обновлен");
    }
}
