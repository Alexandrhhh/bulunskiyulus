<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GroupsController extends Controller
{
    public function all()
    {
        $groups = \App\Group::all();

        return view('admin.groups.all', compact('groups'));
    }

    public function create(Request $request)
    {
        $request->validate([
            'title' => 'required|unique:groups,title'
        ]);

        \App\Group::create($request->all());

        return redirect(route('admin.groups.all'))->with('success', "Группа <strong>{$request->title}</strong> успешно создана!");
    }

    public function edit($id)
    {
        $group = \App\Group::find($id);
        
        return view('admin.groups.edit', compact('group'));
    }

    public function update(Request $request, $id)
    {
        $group = \App\Group::find($id);
        $request->validate([
            'title' => "required|unique:groups,title,{$group->id}",
        ]);
        $group->title = $request->title;
        $group->description = $request->description;
        $group->permissions = json_encode($request->permissions);
        $group->save();
        return redirect(route('admin.groups.all'))->with('success', "Группа <strong>{$request->title}</strong> успешно обновлена");
    }
}
