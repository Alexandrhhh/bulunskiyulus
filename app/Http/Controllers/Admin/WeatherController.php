<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class WeatherController extends Controller
{
    public function index()
    {
        $alarmTerms = \App\AlarmTerm::all();
//        $weather = \App\Weather::latest()->first();
//        \Cache::pull('alert');
//        foreach ($alarmTerms->groupBy('temperature') as $temperature => $group) {
//            $temperature = explode(';', $temperature);
//            if($temperature[0] >= $weather->temperature && $weather->temperature >= $temperature[1]) {
//                foreach ($group as $alert) {
//                    $wind = explode(';', $alert->wind);
//                    if($wind[0] <= $weather->wind_speed && $weather->wind_speed <= $wind[1]) {
//                        \Cache::set('alert', $alert->alert);
//                    }
//                }
//            }
//        }
//        \App\AlarmTerm::create(['wind' => '0;6', 'temperature' => '100;1']);
//        \App\AlarmTerm::create(['wind' => '7;9', 'temperature' => '100;1']);
//        \App\AlarmTerm::create(['wind' => '10;14', 'temperature' => '100;1']);
//        \App\AlarmTerm::create(['wind' => '15;19', 'temperature' => '100;1']);
//        \App\AlarmTerm::create(['wind' => '20;24', 'temperature' => '100;1']);
//        \App\AlarmTerm::create(['wind' => '25;29', 'temperature' => '100;1']);
//        \App\AlarmTerm::create(['wind' => '30;35', 'temperature' => '100;1']);
//        \App\AlarmTerm::create(['wind' => '36;40', 'temperature' => '100;1']);
//        \App\AlarmTerm::create(['wind' => '41;100', 'temperature' => '100;1']);
//        \App\AlarmTerm::create(['wind' => '0;6', 'temperature' => '0;-5']);
//        \App\AlarmTerm::create(['wind' => '0;6', 'temperature' => '-6;-10']);
//        \App\AlarmTerm::create(['wind' => '0;6', 'temperature' => '-11;-15']);
//        \App\AlarmTerm::create(['wind' => '0;6', 'temperature' => '-16;-20']);
//        \App\AlarmTerm::create(['wind' => '0;6', 'temperature' => '-21;-25']);
//        \App\AlarmTerm::create(['wind' => '0;6', 'temperature' => '-26;-30']);
//        \App\AlarmTerm::create(['wind' => '0;6', 'temperature' => '-31;-35']);
//        \App\AlarmTerm::create(['wind' => '0;6', 'temperature' => '-36;-40', 'alert' => '1-6 класс']);
//        \App\AlarmTerm::create(['wind' => '0;6', 'temperature' => '-41;-45', 'alert' => '1-8 класс']);
//        \App\AlarmTerm::create(['wind' => '0;6', 'temperature' => '-46;-50', 'alert' => '1-11 класс, дет. сад, женский труд']);
//
//        \App\AlarmTerm::create(['wind' => '7;9', 'temperature' => '0;-5']);
//        \App\AlarmTerm::create(['wind' => '7;9', 'temperature' => '-6;-10']);
//        \App\AlarmTerm::create(['wind' => '7;9', 'temperature' => '-11;-15']);
//        \App\AlarmTerm::create(['wind' => '7;9', 'temperature' => '-16;-20']);
//        \App\AlarmTerm::create(['wind' => '7;9', 'temperature' => '-21;-25']);
//        \App\AlarmTerm::create(['wind' => '7;9', 'temperature' => '-26;-30', 'alert' => '1-2 класс']);
//        \App\AlarmTerm::create(['wind' => '7;9', 'temperature' => '-31;-35', 'alert' => '1-4 класс']);
//        \App\AlarmTerm::create(['wind' => '7;9', 'temperature' => '-36;-40', 'alert' => '1-6 класс']);
//        \App\AlarmTerm::create(['wind' => '7;9', 'temperature' => '-41;-45', 'alert' => '1-11 класс, детсад жен.']);
//        \App\AlarmTerm::create(['wind' => '7;9', 'temperature' => '-46;-50', 'alert' => '1-11 класс, детсад жен.']);
//        \App\AlarmTerm::create(['wind' => '10;14', 'temperature' => '0;-5']);
//        \App\AlarmTerm::create(['wind' => '10;14', 'temperature' => '-6;-10']);
//        \App\AlarmTerm::create(['wind' => '10;14', 'temperature' => '-11;-15']);
//        \App\AlarmTerm::create(['wind' => '10;14', 'temperature' => '-16;-20']);
//        \App\AlarmTerm::create(['wind' => '10;14', 'temperature' => '-21;-25', 'alert' => '1-2 класс']);
//        \App\AlarmTerm::create(['wind' => '10;14', 'temperature' => '-26;-30', 'alert' => '1-6 класс']);
//        \App\AlarmTerm::create(['wind' => '10;14', 'temperature' => '-31;-35', 'alert' => '1-7 класс']);
//        \App\AlarmTerm::create(['wind' => '10;14', 'temperature' => '-36;-40', 'alert' => '1-11 класс, детсад жен.']);
//        \App\AlarmTerm::create(['wind' => '10;14', 'temperature' => '-41;-45', 'alert' => '1-11 класс, детсад жен.']);
//        \App\AlarmTerm::create(['wind' => '10;14', 'temperature' => '-46;-50', 'alert' => '1-11 класс, детсад жен.,жен. труд']);
//        \App\AlarmTerm::create(['wind' => '15;19', 'temperature' => '0;-5']);
//        \App\AlarmTerm::create(['wind' => '15;19', 'temperature' => '-6;-10', 'alert' => '1-4 класс']);
//        \App\AlarmTerm::create(['wind' => '15;19', 'temperature' => '-11;-15', 'alert' => '1-4 класс']);
//        \App\AlarmTerm::create(['wind' => '15;19', 'temperature' => '-16;-20', 'alert' => '1-6 класс']);
//        \App\AlarmTerm::create(['wind' => '15;19', 'temperature' => '-21;-25', 'alert' => '1-8 класс']);
//        \App\AlarmTerm::create(['wind' => '15;19', 'temperature' => '-26;-30', 'alert' => '1-8 класс']);
//        \App\AlarmTerm::create(['wind' => '15;19', 'temperature' => '-31;-35', 'alert' => '1-11 класс, детсад жен.']);
//        \App\AlarmTerm::create(['wind' => '15;19', 'temperature' => '-36;-40', 'alert' => '1-11 класс, детсад жен.']);
//        \App\AlarmTerm::create(['wind' => '15;19', 'temperature' => '-41;-45', 'alert' => '1-11 класс, детсад жен.']);
//        \App\AlarmTerm::create(['wind' => '15;19', 'temperature' => '-46;-50', 'alert' => '1-11 класс, детсад жен., жен. труд']);
//        \App\AlarmTerm::create(['wind' => '20;24', 'temperature' => '0;-5', 'alert' => '1-6 класс']);
//        \App\AlarmTerm::create(['wind' => '20;24', 'temperature' => '-6;-10', 'alert' => '1-6 класс']);
//        \App\AlarmTerm::create(['wind' => '20;24', 'temperature' => '-11;-15', 'alert' => '1-6 класс']);
//        \App\AlarmTerm::create(['wind' => '20;24', 'temperature' => '-16;-20', 'alert' => '1-8 класс']);
//        \App\AlarmTerm::create(['wind' => '20;24', 'temperature' => '-21;-25', 'alert' => '1-11 класс, детсад жен.']);
//        \App\AlarmTerm::create(['wind' => '20;24', 'temperature' => '-26;-30', 'alert' => '1-11 класс, детсад жен.']);
//        \App\AlarmTerm::create(['wind' => '20;24', 'temperature' => '-31;-35', 'alert' => '1-11 класс, детсад жен., жен. труд']);
//        \App\AlarmTerm::create(['wind' => '20;24', 'temperature' => '-36;-40', 'alert' => '1-11 класс, детсад жен., жен. труд']);
//        \App\AlarmTerm::create(['wind' => '20;24', 'temperature' => '-41;-45', 'alert' => '1-11 класс, детсад жен.,
// жен. труд']);
//        \App\AlarmTerm::create(['wind' => '20;24', 'temperature' => '-46;-50', 'alert' => 'Всем']);
//        \App\AlarmTerm::create(['wind' => '25;29', 'temperature' => '0;-5', 'alert' => '1-8 класс']);
//        \App\AlarmTerm::create(['wind' => '25;29', 'temperature' => '-6;-10', 'alert' => '1-8 класс']);
//        \App\AlarmTerm::create(['wind' => '25;29', 'temperature' => '-11;-15', 'alert' => '1-9 класс']);
//        \App\AlarmTerm::create(['wind' => '25;29', 'temperature' => '-16;-20', 'alert' => '1-11 класс, детсад жен., жен. труд']);
//        \App\AlarmTerm::create(['wind' => '25;29', 'temperature' => '-21;-25', 'alert' => '1-11 класс, детсад жен., жен. труд']);
//        \App\AlarmTerm::create(['wind' => '25;29', 'temperature' => '-26;-30', 'alert' => '1-11 класс, детсад жен., жен. труд']);
//        \App\AlarmTerm::create(['wind' => '25;29', 'temperature' => '-31;-35', 'alert' => '1-11 класс, детсад жен., жен. труд']);
//        \App\AlarmTerm::create(['wind' => '25;29', 'temperature' => '-36;-40', 'alert' => '1-11 класс, детсад жен., жен. труд']);
//        \App\AlarmTerm::create(['wind' => '25;29', 'temperature' => '-41;-45', 'alert' => 'Всем']);
//        \App\AlarmTerm::create(['wind' => '25;29', 'temperature' => '-46;-50', 'alert' => 'Всем']);
//        \App\AlarmTerm::create(['wind' => '30;35', 'temperature' => '0;-5', 'alert' => '1-11 класс, детсад жен.']);
//        \App\AlarmTerm::create(['wind' => '30;35', 'temperature' => '-6;-10', 'alert' => '1-11 класс, детсад жен.']);
//        \App\AlarmTerm::create(['wind' => '30;35', 'temperature' => '-11;-15', 'alert' => '1-11 класс, детсад жен., жен. труд']);
//        \App\AlarmTerm::create(['wind' => '30;35', 'temperature' => '-16;-20', 'alert' => '1-11 класс, детсад жен., жен. труд']);
//        \App\AlarmTerm::create(['wind' => '30;35', 'temperature' => '-21;-25', 'alert' => '1-11 класс, детсад жен., жен. труд']);
//        \App\AlarmTerm::create(['wind' => '30;35', 'temperature' => '-26;-30', 'alert' => '1-11 класс, детсад жен., жен. труд']);
//        \App\AlarmTerm::create(['wind' => '30;35', 'temperature' => '-31;-35', 'alert' => '1-11 класс, детсад жен.,
// жен. труд']);
//        \App\AlarmTerm::create(['wind' => '30;35', 'temperature' => '-36;-40', 'alert' => 'Всем']);
//        \App\AlarmTerm::create(['wind' => '30;35', 'temperature' => '-41;-45', 'alert' => 'Всем']);
//        \App\AlarmTerm::create(['wind' => '30;35', 'temperature' => '-46;-50', 'alert' => 'Всем']);
//        \App\AlarmTerm::create(['wind' => '36;40', 'temperature' => '0;-5', 'alert' => '1-11 класс, детсад жен.']);
//        \App\AlarmTerm::create(['wind' => '36;40', 'temperature' => '-6;-10', 'alert' => '1-11 класс, детсад жен.']);
//        \App\AlarmTerm::create(['wind' => '36;40', 'temperature' => '-11;-15', 'alert' => '1-11 класс, детсад жен., жен. труд']);
//        \App\AlarmTerm::create(['wind' => '36;40', 'temperature' => '-16;-20', 'alert' => '1-11 класс, детсад жен., жен. труд']);
//        \App\AlarmTerm::create(['wind' => '36;40', 'temperature' => '-21;-25', 'alert' => '1-11 класс, детсад жен., жен. труд']);
//        \App\AlarmTerm::create(['wind' => '36;40', 'temperature' => '-26;-30', 'alert' => '1-11 класс, детсад жен., жен. труд']);
//        \App\AlarmTerm::create(['wind' => '36;40', 'temperature' => '-31;-35', 'alert' => 'Всем']);
//        \App\AlarmTerm::create(['wind' => '36;40', 'temperature' => '-36;-40', 'alert' => 'Всем']);
//        \App\AlarmTerm::create(['wind' => '36;40', 'temperature' => '-41;-45', 'alert' => 'Всем']);
//        \App\AlarmTerm::create(['wind' => '36;40', 'temperature' => '-46;-50', 'alert' => 'Всем']);
//        \App\AlarmTerm::create(['wind' => '41;100', 'temperature' => '0;-5', 'alert' => 'Всем']);
//        \App\AlarmTerm::create(['wind' => '41;100', 'temperature' => '-6;-10', 'alert' => 'Всем']);
//        \App\AlarmTerm::create(['wind' => '41;100', 'temperature' => '-11;-15', 'alert' => 'Всем']);
//        \App\AlarmTerm::create(['wind' => '41;100', 'temperature' => '-16;-20', 'alert' => 'Всем']);
//        \App\AlarmTerm::create(['wind' => '41;100', 'temperature' => '-21;-25', 'alert' => 'Всем']);
//        \App\AlarmTerm::create(['wind' => '41;100', 'temperature' => '-26;-30', 'alert' => 'Всем']);
//        \App\AlarmTerm::create(['wind' => '41;100', 'temperature' => '-31;-35', 'alert' => 'Всем']);
//        \App\AlarmTerm::create(['wind' => '41;100', 'temperature' => '-36;-40', 'alert' => 'Всем']);
//        \App\AlarmTerm::create(['wind' => '41;100', 'temperature' => '-41;-45', 'alert' => 'Всем']);
//        \App\AlarmTerm::create(['wind' => '41;100', 'temperature' => '-46;-50', 'alert' => 'Всем']);

        $winds = \App\Wind::all();
        return view('admin.system.weather.index', compact('winds', 'alarmTerms'));
    }

    public function create(Request $request)
    {
        \App\Wind::create($request->all());
        return redirect(route('admin.system.weather.index'));
    }
}
