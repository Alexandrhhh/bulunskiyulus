<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\FilmCategory;
use App\FilmSession;
use App\Film;
use Illuminate\Http\Request;

class FilmController extends Controller
{
    public function index()
    {
        $films = \App\Film::latest()->get();
        return view('admin.films.index', compact('films'));
    }

    public function add()
    {
        $genres = \App\FilmCategory::all();
        return view('admin.films.add', compact('genres'));
    }

    public function create(Request $request)
    {
        $film = Film::create($request->all());
        if($request->hasFile('image')) {
            $film->image = \Storage::disk('public')->putFile('images', $request->image);
        }
        // dd($film);
        if($request->has('sessions')) {
            foreach ($request->sessions as $key => $session) {
                if($session !== null) {
                    FilmSession::updateOrCreate([
                        'film_id' => $film->id,
                        'display_at' => now()->setTimestamp(strtotime($session))
                    ]);
                }
            }
        }
        if($request->has('genres')) {
            foreach ($request->genres as $key => $genre) {
                $genres[] = FilmCategory::firstOrCreate(['title' => $genre])->id;
            }
            $film->genres()->attach($genres);
        }
        $film->save();
        return redirect(route('admin.film.index'))->with('success', 'Успешно добавлен');
    }

    public function edit($id)
    {
        $film = Film::find($id);
        $genres = FilmCategory::all();
        return view('admin.films.edit', compact('film', 'genres'));
    }

    public function update(Request $request, $id)
    {
        $film = Film::find($id);
        if($request->hasFile('image')) {
            $film->image = \Storage::disk('public')->putFile('images', $request->image);
        }
        if($request->has('sessions')) {
            $film->sessions()->delete();
            foreach ($request->sessions as $key => $session) {
                if($session !== null) {
                    FilmSession::updateOrCreate([
                        'film_id' => $film->id,
                        'display_at' => now()->setTimestamp(strtotime($session))
                    ]);
                }
            }
        }
        if($request->has('genres')) {
            $film->genres()->detach();
            foreach ($request->genres as $key => $genre) {
                $genres[] = FilmCategory::firstOrCreate(['title' => $genre])->id;
            }
            $film->genres()->attach($genres);
        }
        $film->save();
        return redirect(route('admin.film.index'))->with('success', 'Успешно обновлен');
    }

    public function delete($id)
    {
        $film = Film::find($id);
        if($film->image) \Storage::disk('public')->delete($film->image);
        if($film->genres) $film->genres()->detach();
        if($film->sessions) $film->sessions()->delete();
        $film->delete();
        return redirect(route('admin.film.index'))->with('success', 'Успешно удален');
    }
}
