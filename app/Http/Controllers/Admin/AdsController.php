<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdsController extends Controller
{
    public function all()
    {
        $ads = \App\Ad::paginate(20);
        return view('admin.ads.all', compact('ads'));
    }

    public function new()
    {
        $categories = \App\AdCategory::all();
        $cities = \App\City::all();
        return view('admin.ads.new', compact('categories', 'cities'));
    }

    public function edit($id)
    {
        $categories = \App\AdCategory::all();
        $cities = \App\City::all();
        $ad = \App\Ad::find($id);
        return view('admin.ads.edit', compact('categories', 'cities', 'ad'));
    }

    public function delete($id)
    {
        $ad = \App\Ad::find($id);
        foreach ($ad->photos as $key => $photo) {
            \Storage::disk('public')->delete($photo->url);
            $photo->delete();
        }
        $ad->delete();
        return redirect(route('admin.ads.all'))->with('success', 'Объявление успешно удалено');
    }

    public function all_categories()
    {
        $categories = \App\AdCategory::all();
        return view('admin.ads.categories', compact('categories'));
    }

    public function create_category(Request $request)
    {
        $request->validate([
            'title' => 'required|unique:ad_categories',
        ]);
        $category = \App\AdCategory::create($request->all());
        if(!$category) {
            return redirect(route('admin.ads.categories.all'))->with('error', 'При добавлении категории <strong>'.$request->title.'</strong> произошла ошибка');
        } else {
            return redirect(route('admin.ads.categories.all'))->with('success', 'Категория <strong>'.$category->title.'</strong> успешно добавлена');
        }
    }

    public function delete_category(Request $request, $id)
    {
        $category = \App\AdCategory::find($id);
        $title = $category->title;
        $category->ads()->delete();
        $category->delete();
        return redirect(route('admin.ads.category.all'))->with('success', 'Категория <strong>'.$title.'</strong> успешно удалена');
    }

    public function all_cities()
    {
        $cities = \App\City::all();
        return view('admin.ads.cities', compact('cities'));
    }

    public function create_city(Request $request)
    {
        $request->validate([
            'title' => 'required|unique:cities',
        ]);
        $city = \App\City::create($request->all());
        if(!$city) {
            return redirect(route('admin.ads.cities.all'))->with('error', 'При добавлении города <strong>'.$request->title.'</strong> произошла ошибка');
        } else {
            return redirect(route('admin.ads.cities.all'))->with('success', 'Город <strong>'.$city->title.'</strong> успешно добавлен');
        }
    }

    public function delete_city(Request $request, $id)
    {
        $city = \App\City::find($id);
        $title = $city->title;
        $city->ads()->delete();
        $city->delete();
        return redirect(route('admin.ads.cities.all'))->with('success', 'Город <strong>'.$title.'</strong> успешно удален');
    }
}
