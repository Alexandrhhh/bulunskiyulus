<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\MayakTag;
use Illuminate\Http\Request;

class MayakController extends Controller
{
    public function index()
    {
        $mayaks = \App\Mayak::orderBy('created_at', 'desc')->get();
        return view('admin.archive.mayak.index', compact('mayaks'));
    }

    public function add()
    {
        $tags = \App\MayakTag::all();
        return view('admin.archive.mayak.add', compact('tags'));
    }

    public function create(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'image' => 'required',
            'publish_at' => 'required',
            'file' => 'required'
        ]);

        $mayak = \App\Mayak::create([
            'title' => $request->title,
            'publish_at' => $request->publish_at
        ]);
        $file = \Storage::disk('public')->putFile('mayak/pdf', $request->file);
        $image = \Storage::disk('public')->putFile('mayak/images', $request->image);
        if($request->has('tags')) {
            foreach ($request->tags as $key => $tag) {
                $tags[] = \App\MayakTag::firstOrCreate(['title'=> $tag])->id;
            }
            $mayak->tags()->attach($tags);
        }
        $mayak->file = $file;
        $mayak->image = $image;
        $mayak->save();

        return redirect(route('admin.archive.mayak.index'))->with('success', 'Выпуск успешно добавлен');
    }

    public function edit($id)
    {
        $mayak = \App\Mayak::find($id);
        $tags = \App\MayakTag::all();
        return view('admin.archive.mayak.edit', compact('mayak', 'tags'));
    }

    public function update(Request $request, $id)
    {
        $mayak = \App\Mayak::find($id);
        $mayak->title = $request->title;
        $mayak->publish_at = $request->publish_at;
        if($request->hasFile('image')) {
            if($mayak->image) {
                \Storage::delete($mayak->image);
            }
            $mayak->image = \Storage::disk('public')->putFile('mayak/images', $request->image);
        }
        if($request->hasFile('file')) {
            if($mayak->file) {
                \Storage::delete($mayak->file);
            }
            $mayak->file = \Storage::disk('public')->putFile('mayak/pdf', $request->file);
        }
        if($request->has('tags')) {
            $mayak->tags()->detach();
            foreach ($request->tags as $key => $tag) {
                $tags[] = \App\MayakTag::firstOrCreate(['title'=> $tag])->id;
            }
            $mayak->tags()->attach($tags);
        }
        $mayak->save();

        return redirect(route('admin.archive.mayak.index'))->with('success', 'Выпуск успешно обновлен');
    }
}
