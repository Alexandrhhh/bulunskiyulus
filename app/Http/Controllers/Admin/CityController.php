<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CityController extends Controller
{
    public function index()
    {
        $cities = \App\City::all();
        return view('admin.system.cities.all', compact('cities'));
    }

    public function create(Request $request)
    {
        $request->validate([
            'title' => 'required|unique:cities',
            'latitude' => 'required',
            'longitude' => 'required'
        ]);
        \App\City::create($request->all());
        return redirect(route('admin.system.cities.all'))->with('success', 'Город успешно создан');
    }

    public function edit($id)
    {
        $city = \App\City::find($id);
        return view('admin.system.cities.edit', compact('city'));
    }

    public function update(Request $request, $id)
    {
        $city = \App\City::find($id);
        $city->update($request->all());
        return redirect(route('admin.system.cities.all'))->with('success', 'Город успешно обновлен');
    }
}
