<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Answer;
use Illuminate\Http\Request;

class PollController extends Controller
{
    public function index()
    {
        $polls = \App\Poll::all();
        return view('admin.poll.index', compact('polls'));
    }

    public function add()
    {
        return view('admin.poll.add');
    }

    public function create(Request $request)
    {
        $poll = \App\Poll::create([
            'title' => $request->title,
            'end_date' => $request->end_date,
            'type' => $request->type
        ]);
        foreach ($request->answer as $key => $answer) {
            if($answer !== null) {
                \App\Answer::create([
                    'title' => $answer,
                    'poll_id' => $poll->id
                ]);
            }
        }

        return redirect(route('admin.poll.all'))->with('success', 'Опрос успешно добавлен');
    }

    public function edit($id)
    {
        $poll = \App\Poll::find($id);
        return view('admin.poll.edit', compact('poll'));
    }

    public function update(Request $request, $id)
    {
        $poll = \App\Poll::find($id);
        $poll->title = $request->title;
        $poll->end_date = $request->end_date;
        $poll->type = $request->type;
        $poll->save();
        foreach ($request->answers as $key => $ans) {
            $answer = \App\Answer::find($key);
            $answer->title = $ans['title'];
            $answer->count = $ans['count'];
            $answer->save();
        }
        foreach ($request->answer as $key => $answer) {
            if($answer !== null) {
                \App\Answer::create([
                    'title' => $answer,
                    'poll_id' => $poll->id
                ]);
            }
        }

        return redirect(route('admin.poll.all'))->with('success', 'Опрос успешно обновлен');
    }

    public function answer(Request $request)
    {
        $news = \App\News::find($request->news);
        if($request->has('answers')) {
            foreach ($request->answers as $key => $id) {
                $answer = \App\Answer::find($id);
                \App\AnswerUser::create([
                    'user_id' => \Auth::check() ? Auth::user()->id : 0,
                    'poll_id' => $request->poll,
                    'answer_id' => $answer->id
                ]);
                $answer->increment('count');
            }
        } else {
            $answer = \App\Answer::find($request->answer);
            \App\AnswerUser::create([
                'user_id' => \Auth::check() ? Auth::user()->id : 0,
                'poll_id' => $request->poll,
                'answer_id' => $answer->id
            ]);
            $answer->increment('count');
        }
        return view('include.poll', compact('news'));
    }
}
