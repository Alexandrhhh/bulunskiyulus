<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ArchiveController extends Controller
{
    public function index()
    {
        $sections = \App\Archive\Section::all();
        return view('archive.index', compact('sections'));
    }

    public function mayak(Request $request)
    {
        if($request->has('asc')) {
            $mayaks = \App\Mayak::orderBy('publish_at', 'asc')->get();
        } else {
            $mayaks = \App\Mayak::orderBy('publish_at', 'desc')->get();
        }
        return view('archive.mayak', compact('mayaks'));
    }

    public function show_mayak($id)
    {
        $mayak = \App\Mayak::find($id);
        return view('archive.show_mayak', compact('mayak'));
    }

    public function add()
    {
        $sections = \App\Archive\Section::all();
        $tags = \App\Archive\Tag::orderBy('title', 'asc')->get();
        $humans = \App\Archive\Human::orderBy('title', 'asc')->get();
        return view('archive.add', compact('sections', 'tags', 'humans'));
    }

    public function upload(Request $request)
    {
        $getID3 = new \getID3;
        $duration = null;
        if($request->hasFile('file')) {
            $path = \Storage::disk('public')->putFile('archive', $request->file);
            $url = \Storage::url($path);
            $file = $getID3->analyze($request->file);
            if(isset($file['playtime_seconds'])) $duration = round($file['playtime_seconds']);
        } else {
            $url = false;
        }
        if($duration != null) {
            return collect([
                'url' => $url,
                'duration' => $duration,
            ])->toJson();
        } else {
            return collect([
                'url' => $url
            ])->toJson();
        }
    }

    public function create(Request $request)
    {
        $file = \App\Archive\File::create([
            'title' => $request->has('title') ? $request->title : null,
            'section_id' => $request->section_id,
            'description' => $request->description,
            'create_day' => $request->create_day,
            'duration' => $request->has('duration') ? $request->duration : null,
            'type' => $request->type,
            'file' => $request->file,
            'preview' => $request->has('duration') ? \Storage::disk('public')->putFile('archive/preview', $request->preview) : null,
            'user_id' => \Auth::check() ? \Auth::user()->id : 1,
        ]);

        if($request->has('tags')) {
            foreach ($request->tags as $key => $tag) {
                $tags[] = \App\Archive\Tag::firstOrCreate([
                    'title' => $tag
                ])->id;
            }
            $file->tags()->attach($tags);
        }
        if($request->has('labels')) {
            foreach ($request->labels as $key => $label) {
                $label = \App\Archive\Label::find($label);
                $label->file_id = $file->id;
                $label->save();
            }
        }
        return redirect(route('archive.add'))->with('success', 'Файл успешно загружен');
		}

		public function section($id)
		{
			$section = \App\Archive\Section::find($id);
			$files = \App\Archive\File::whereSectionId($section->id)->paginate(15);
			return view('archive.section', compact('section', 'files'));
		}
}
