<?php

namespace App\Http\Controllers\Forum;

use App\Http\Controllers\Controller;
use App\TopicMessage;
use Illuminate\Http\Request;

class TopicController extends Controller
{
    public function index(Request $request)
    {
        if($request->has('search') && $request->search != '') {
            $topics = \App\Topic::where('title', 'like', '%'.$request->search.'%')->with('last')->get();
        } else {
            $topics = \App\Topic::with('last')->get();
        }

        return view('forum.index', compact('topics'));
    }

    public function add()
    {
        return view('forum.create');
    }

    public function create(Request $request)
    {
        $topic = \App\Topic::create($request->all() + ['user_id' => \Auth::user()->id]);
        return redirect(route('forum.topic', ['id' => $topic->id]));
    }

    public function create_message(Request $request, $id)
    {
        $request->validate([
            'message' => 'required'
        ]);
        $message = \App\TopicMessage::create([
            'user_id' => \Auth::user()->id,
            'topic_id' => $id,
            'message' => $request->message
        ]);
        return redirect(route('forum.topic', ['id' => $id]));
    }

    public function topic($id)
    {
        $topic = \App\Topic::find($id);
        $messages = \App\TopicMessage::where('topic_id', $id)->orderBy('created_at', 'desc')->paginate(10);
        return view('forum.topic', compact('topic', 'messages'));
    }
}
