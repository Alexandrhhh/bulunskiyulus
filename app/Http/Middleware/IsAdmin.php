<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //Получаем из адреса только первые два значения, admin.news.category.all => оставит только admin.news
        $array = \explode('/' , $request->route()->action['prefix']); //получили массив из префикса
        //Удалили если нулевой массив для url /admin и так далее
        if($array[0] == '') {
            unset($array[0]);
            $array = array_values($array);
        }
        //Если осталось больше 2 в массиве, то присвоить 2
        if(count($array) > 2) {
            $count = 2;
        } else {
            $count = count($array);
        }
        //Создание префикса
        $prefix = '';
        for ($i=0; $i < $count; $i++) {
            $prefix .= $array[$i].'.';
        }
        $prefix = \rtrim($prefix, '.');
        //Проверочка
        if(Auth::check() && Auth::user()->hasPermission($prefix)) {
            return $next($request);
        }
        abort(403);

    }
}
