<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsCategory extends Model
{

    protected $fillable = [
        'title', 'color_id'
    ];

    public function newses()
    {
        return $this->hasMany(News::class, 'category_id');
    }

    public function color()
    {
        return $this->belongsTo(Color::class);
    }
}
