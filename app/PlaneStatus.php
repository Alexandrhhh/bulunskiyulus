<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlaneStatus extends Model
{
    protected $fillable = [
        'title', 'color'
    ];

    public function planes()
    {
        return $this->hasMany(Plane::class, 'status_id');
    }
}
