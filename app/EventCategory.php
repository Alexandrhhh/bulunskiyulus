<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventCategory extends Model
{
    protected $fillable = [
        'title', 'color_id'
    ];

    public function events()
    {
        return $this->hasMany(Event::class, 'category_id');
    }

    public function color()
    {
        return $this->belongsTo(Color::class);
    }
}
