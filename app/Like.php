<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Like extends Model
{
    protected $fillable = ['user_id'];

    public function liketable()
    {
        return $this->morphTo('liketable');
    }

    public static function addLike($model, $id)
    {
        if($model->likes()->whereUserId($id)->get()->count()) {
            $model->likes()->whereUserId($id)->delete();
            return false;
        }  else {
            $model->likes()->save(
                new self([
                    'user_id' => $id
                ])
            );
            return true;
        }
    }

    public static function hasLike($model)
    {
        if(Auth::check()) {
			if ($model->likes()->whereUserId(Auth::user()->id)->get()->count()) {
				return true;
			}
		}
		return false;
    }
}
