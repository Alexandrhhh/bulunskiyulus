<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    protected $fillable = ['title', 'title_eng', 'description', 'duration'];

    protected $guarded = ['image'];

    public function sessions()
    {
        return $this->hasMany(FilmSession::class);
    }

    public function actual_sessions()
    {
        return $this->sessions->
        where('display_at', '>', now()->format('Y-m-d 00:00:00'))->
        where('display_at', '<', now()->addDays(7)->format('Y-m-d 00:00:00'))->
        sortBy('display_at')->
        groupBy(function($item) {
            return $item->display_at->format('d.m.Y');
        });
    }

    public function genres()
    {
        return $this->belongsToMany(FilmCategory::class, 'film_category', 'film_id', 'category_id');
    }
}
