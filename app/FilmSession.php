<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FilmSession extends Model
{
    protected $fillable = [
        'display_at', 'film_id'
    ];

    public $dates = [
        'created_at', 'updated_at', 'display_at'
    ];

    public function film()
    {
        return $this->belongsTo(Film::class);
    }
}
