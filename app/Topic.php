<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\TopicMessage;

class Topic extends Model
{
    protected $fillable = [
        'title', 'user_id', 'message', 'about', 'rating'
    ];

    public function messages()
    {
        return $this->hasMany(TopicMessage::class);
    }

    public function last()
    {
        return $this->hasOne(TopicMessage::class)->latest();
    }

    public function first()
    {
        return $this->hasOne(TopicMessage::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'shortguid');
    }

}
