<?php

namespace App\Transport;

use Illuminate\Database\Eloquent\Model;

class Stop extends Model
{
    protected $fillable = [
        'stop_name_id', 'time', 'stop_day_id', 'bus_id', 'weekend'
    ];

    public $dates = [
        'created_at', 'updated_at', 'time'
    ];

    public function name()
    {
        return $this->belongsTo(StopName::class, 'stop_name_id');
    }

    public function day()
    {
        return $this->belongsTo(StopDay::class, 'stop_day_id');
    }

    public function bus()
    {
        return $this->hasMany(Bus::class);
    }
}
