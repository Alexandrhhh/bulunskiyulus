<?php

namespace App\Transport;

use Illuminate\Database\Eloquent\Model;

class StopName extends Model
{
    protected $fillable = [
        'title', 'description'
    ];

    public function stops()
    {
        return $this->hasMany(Stop::class, 'stop_name_id');
    }
}
