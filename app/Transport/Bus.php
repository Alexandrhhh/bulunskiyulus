<?php

namespace App\Transport;

use Illuminate\Database\Eloquent\Model;

class Bus extends Model
{

    protected $fillable = [
        'title'
    ];

    public function stops()
    {
        return $this->hasMany(Stop::class)->orderBy('time');
    }
}
