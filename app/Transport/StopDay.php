<?php

namespace App\Transport;

use Illuminate\Database\Eloquent\Model;

class StopDay extends Model
{
    protected $fillable = [
        'title', 'eng'
    ];

    public function stops()
    {
        return $this->hasMany(Stop::class, 'stop_day_id');
    }
}
