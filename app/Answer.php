<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\AnswerUser;

class Answer extends Model
{
    protected $fillable = [
        'title', 'count', 'poll_id'
    ];

    public function poll()
    {
        return $this->belongsTo(Poll::class);
    }

    public function user_answers()
    {
        return $this->hasMany(AnswerUser::class);
    }

    public function current()
    {
        return \App\AnswerUser::where([['answer_id', '=', $this->id], ['user_id', '=', 0]])->get();
    }
}
