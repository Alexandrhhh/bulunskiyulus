<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceYAKassa extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'operations_yandexkassa';
    public $timestamps = false;

    protected $fillable = [
        'shortguid', 'sum', 'operation_id', 'status', 'actiondate'
    ];

}
