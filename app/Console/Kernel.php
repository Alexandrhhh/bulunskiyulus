<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function() {
            $cities = \App\City::all();
            $curl = curl_init();
            foreach ($cities as $key => $city) {
                curl_setopt_array($curl, array(
                  CURLOPT_URL => "https://api.gismeteo.net/v2/weather/current/".'?longitude='.$city->longitude.'&latitude='.$city->latitude,
                  CURLOPT_RETURNTRANSFER => true,
                  CURLOPT_ENCODING => "",
                  CURLOPT_MAXREDIRS => 10,
                  CURLOPT_TIMEOUT => 30,
                  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                  CURLOPT_CUSTOMREQUEST => "GET",
                  CURLOPT_HTTPHEADER => array(
                    "Accept: application/json",
                    "Accept-Encoding: gzip, deflate",
                    "Content-Type: application/json; charset=utf-8",
                    "cache-control: no-cache",
                    "X-Gismeteo-Token: 5ea2ce22847055.90937220"
                  ),
                ));
                $weather = json_decode(curl_exec($curl));
                if($weather) {
                    $weather = $weather->response;
                    \App\Weather::create([
                        'city_id' => $city->id,
                        'wind_id' => $weather->wind->direction->scale_8,
                        'wind_speed' =>$weather->wind->speed->m_s,
                        'temperature' => $weather->temperature->air->C,
                        'about' => rtrim($weather->description->full, ', '),
                        'icon_name' => $weather->icon
                    ]);
                }
            }
        })->cron('*/30 * * * *');

        $schedule->call(function() {
            $response = \Http::withHeaders([
                'X-Gismeteo-Token' => '5ea2ce22847055.90937220'
            ])->get('https://api.gismeteo.net/v2/weather/forecast/by_day_part/', [
                'latitude' => 72.000112,
                'longitude' => 129.128496,
                'days' => 7
            ]);
            $seting = \App\Setting::updateOrCreate([
                'title' => "weather"
            ],[
                'setting' => $response->body()
            ]);
        })->cron('* */3 * * *');

        $schedule->call(function() {
            $alarmTerms = \App\AlarmTerm::all();
            $weather = \App\Weather::latest()->first();
            \Cache::pull('alert');
            foreach ($alarmTerms->groupBy('temperature') as $temperature => $group) {
                $temperature = explode(';', $temperature);
                if($temperature[0] >= $weather->temperature && $weather->temperature >= $temperature[1]) {
                    foreach ($group as $alert) {
                        $wind = explode(';', $alert->wind);
                        if($wind[0] <= $weather->wind_speed && $weather->wind_speed <= $wind[1]) {
                            \Cache::set('alert', $alert->alert);
                        }
                    }
                }
            }
        })->cron('1 6,13 * * *');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
