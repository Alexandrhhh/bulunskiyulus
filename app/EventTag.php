<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventTag extends Model
{
    protected $fillable = [
        'title'
    ];

    public function newses()
    {
        return $this->belongsToMany(Event::class);
    }

    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = ucfirst($value);
    }
}
