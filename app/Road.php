<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Road extends Model
{
    protected $fillable = [
        'title', 'open_date', 'close_date', 'max_tonns', 'technics', 'next_clean', 'about', 'tracks', 'color'
    ];

    public $dates = [
        'created_at', 'updated_at', 'open_date', 'close_date', 'next_clean'
    ];
}
