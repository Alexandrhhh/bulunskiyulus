<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnswerUser extends Model
{
    protected $fillable = [
        'answer_id', 'user_id', 'poll_id'
    ];

    public function answer()
    {
        $this->belongsTo(Answer::class);
    }

    public function user()
    {
        $this->belongsTo(User::class, 'user_id', 'shortguid');
    }

    public function poll()
    {
        $this->belongsTo(Poll::class);
    }
}
