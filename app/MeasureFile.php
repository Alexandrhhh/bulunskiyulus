<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MeasureFile extends Model
{
    protected $fillable = [
        'title', 'extension', 'url', 'measure_id'
    ];

    public function measure()
    {
        return $this->belongsTo(Measure::class);
    }

    public function getIconAttribute()
    {
        return 'docx';
    }
}
