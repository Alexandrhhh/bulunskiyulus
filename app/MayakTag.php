<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MayakTag extends Model
{
    protected $fillable = ['title'];

    public function mayaks()
    {
        return $this->belongsToMany(Mayak::class, 'mayak_tag', 'tag_id', 'mayak_id');
    }
}
