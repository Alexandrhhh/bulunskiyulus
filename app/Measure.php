<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Measure extends Model
{
    protected $fillable = [
        'title', 'short', 'content', 'user_id'
    ];

    protected $guarded = [
        'image', 'reads'
    ];

    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable');
    }

    public function likes()
    {
        return $this->morphMany('App\Like', 'liketable');
    }

    public function files()
    {
        return $this->hasMany(MeasureFile::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'shortguid');
    }
}
