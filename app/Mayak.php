<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\MayakTag;

class Mayak extends Model
{
    protected $fillable = [
        'title', 'publish_at', 'image', 'file',
    ];

    public $dates = [
        'created_at', 'updated_at', 'publish_at'
    ];

    public function tags()
    {
        return $this->belongsToMany(MayakTag::class, 'mayak_tag', 'mayak_id', 'tag_id');
    }

}
