<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsTag extends Model
{

    protected $fillable = [
        'title'
    ];

    public function newses()
    {
        return $this->belongsToMany(News::class);
    }

    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = ucfirst($value);
    }
}
