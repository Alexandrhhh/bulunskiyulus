<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $connection = 'mysql2';
    protected $table = 'stat';
    protected $primaryKey = 'shortguid';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'name', 'email', 'password', 'first_name', 'last_name', 'middle_name', 'date_birth', 'photo_id'
    ];

    protected $hidden = [
        'password', 'remember_token', 'user_pswd'
    ];

    public $dates = [
        'date_birth'
    ];

    public function getIdAttribute()
    {
        return $this->shortguid;
    }

    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    public function hasPermission($permission)
    {
        if($this->group_id == 0) return false;
        return $this->group->hasPermission($permission);
    }

    public function moneys()
    {
        return \DB::connection('mysql2')->table('moneys')->where('user_name', $this->shortguid)->get();
    }

    public function tariffs()
    {
        return \DB::connection('mysql2')->table('tarifs')->get();
    }

    public function city()
    {
        return $this->setConnection('mysql')->belongsTo(City::class);
    }

    public function files()
    {
        return $this->connection('mysql')->hasMany(\App\Archive\File::class);
    }

    public function getPhotoAttribute()
    {
        return $this->photo_id;
    }

    public function getTitleDevice($ip)
    {
        $device = \App\Device::find($ip);
        if($device) {
            return $device->title;
        }
        return '';
    }

    public function getDevicesAttribute()
    {
        $devices = explode(';', $this->usrip);
        $collect = collect();
        foreach ($devices as $key => $item) {
            $device = null;
            if($item != '' && $item != null) {
                $device = \App\Device::find($item);
                $traffic = $this->getTrafficDevice($key);
                $n = [
                    'ip' => $item,
                    'title' => $device ? $device->title : 'Без названия',
                    'type' => $device ? $device->type : 0,
                    'traffic' => isset($traffic[$key]) ? $traffic[$key] : 0
                ];
                $collect = $collect->push($n);
            }
        }
        return $collect;
    }

    public function getTrafficDevice()
    {
        $startDate = now()->subMonth();
        $endDate = now();
        $traffics = [];
        while($startDate < $endDate) {
            if(\Schema::connection('mysql2')->hasTable('trafflow2_'.$startDate->format('Y-m-d'))) {
                $traff[] = \DB::connection('mysql2')->select('SELECT acc_index, SUM(trafsize) AS result FROM  ' . '`trafflow2_'
                    .$startDate->format('Y-m-d') . '` WHERE usr = 2198716 GROUP BY acc_index');
                foreach ($traff as $key => $day) {
                    foreach ($day as $device) {
                        $traffics[$device->acc_index] = isset($traffics[$device->acc_index]) ?
                            $traffics[$device->acc_index] + $device->result : $device->result;
                    }
                }
            }
            $startDate = $startDate->addDay();
        }
        return $traffics;
    }

    public function getTarifAttribute()
    {
        return \DB::connection('mysql2')->table('tarifs')->where('tarif_guid', $this->tarif_guid)->first();
    }

    public function getChangeTariffsAttribute()
    {
        $tariffs = explode('||', $this->tarif->followingtarifs);
        foreach ($tariffs as $tariff) {
            $return[] = \DB::connection('mysql2')->table('tarifs')->where('tarif_name', $tariff)->first();
        }
        return collect($return);
    }
}
