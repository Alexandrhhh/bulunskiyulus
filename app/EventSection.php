<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventSection extends Model
{
    protected $fillable = [
        'title'
    ];

    public function events()
    {
        return $this->hasMany(Event::class, 'section_id');
    }
}
