<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{
    protected $fillable = [
        'title', 'city_id', 'category_id', 'price', 'text', 'addres', 'phone', 'user_id'
    ];

    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable');
    }

    public function likes()
    {
        return $this->morphMany('App\Like', 'liketable');
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function category()
    {
        return $this->belongsTo(AdCategory::class, 'category_id');
    }

    public function photos()
    {
        return $this->hasMany(AdPhoto::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'shortguid');
    }
}
