<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class News extends Model
{

    protected $fillable = [
        'title', 'short', 'content', 'image_url', 'category_id', 'section_id', 'poll_id', 'user_id'
    ];

    public function tags()
    {
        return $this->belongsToMany(NewsTag::class);
    }

    public function category()
    {
        return $this->belongsTo(NewsCategory::class, 'category_id');
    }

    public function section()
    {
        return $this->belongsTo(NewsSection::class, 'section_id');
    }

    public function poll()
    {
        return $this->belongsTo(Poll::class);
    }

    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable');
    }

    public function likes()
    {
        return $this->morphMany('App\Like', 'liketable');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'shortguid');
    }
}
