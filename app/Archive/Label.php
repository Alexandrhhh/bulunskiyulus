<?php

namespace App\Archive;

use Illuminate\Database\Eloquent\Model;

class Label extends Model
{
    protected $fillable = [
        'file_id',
        'human_id',
        'height',
        'width',
        'img_height',
        'img_width',
        'left',
        'top',
        'leftTopX',
        'leftTopY',
        'rightBottomX',
        'rightBottomY'
    ];

    public function human()
    {
        return $this->belongsTo(Human::class);
    }

    public function file()
    {
        return $this->belongsTo(File::class);
    }
}
