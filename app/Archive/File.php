<?php

namespace App\Archive;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $fillable = [
        'title', 'create_day', 'description', 'section_id', 'user_id', 'file', 'type', 'preview', 'duration'
    ];

    public $dates = [
        'created_at', 'updated_at', 'create_day',
    ];

    public function section()
    {
        return $this->belongsTo(Section::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'file_tag', 'file_id', 'tag_id');
    }

    public function user()
    {
        return $this->belongsTo(\App\User::class, 'user_id', 'shortguid');
    }
}
