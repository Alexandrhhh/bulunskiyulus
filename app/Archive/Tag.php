<?php

namespace App\Archive;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = ['title'];

    public function files()
    {
        return $this->belongsToMany(File::class, 'file_tag', 'tag_id', 'file_id');
    }
}
