<?php

namespace App\Archive;

use Illuminate\Database\Eloquent\Model;

class Human extends Model
{
    protected $fillable = ['title'];

    public function files()
    {
        return $this->hasMany(File::class);
    }

    public function labels()
    {
        return $this->hasMany(Label::class);
    }
}
