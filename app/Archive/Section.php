<?php

namespace App\Archive;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    protected $fillable = [
        'title', 'image'
    ];

    public function files()
    {
        return $this->hasMany(File::class);
    }
}
