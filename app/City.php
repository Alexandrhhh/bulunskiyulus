<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $fillable = ['title', 'latitude', 'longitude'];

    public function ads()
    {
        return $this->hasMany(Ad::class);
    }

    public function weathers()
    {
        return $this->hasMany(Weather::class);
    }

    public function weather()
    {
        return $this->hasOne(Weather::class)->latest();
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
