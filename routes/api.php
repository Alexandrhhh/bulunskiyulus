<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('poll', 'Admin\PollController@answer');
Route::post('upload_file', 'ArchiveController@upload');
Route::post('add_tag', 'TagController@add')->name('add_tag');
Route::post('remove_tag', 'TagController@remove')->name('remove_tag');
Route::post('like', 'LikeController@addLike')->name('add.like');
