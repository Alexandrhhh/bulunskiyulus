<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@index')->name('index');

//Новости
Route::group(['namespace' => 'News', 'as' => 'news.', 'prefix' => 'news'], function() {
    Route::get('/', 'NewsController@index')->name('index');
    Route::get('section/{id}', 'NewsController@section')->name('section');
    Route::post('add', 'NewsController@create')->name('create');
    Route::post('test', 'NewsController@test')->name('test');
    Route::put('update/{id}', 'NewsController@update')->name('update');
    Route::get('{id}', 'NewsController@news')->name('news');
});

//Объявления
Route::group(['namespace' => 'Ad', 'as' => 'ads.', 'prefix' => 'ads'], function() {
    Route::get('/', 'AdController@index')->name('index');
    Route::get('add', 'AdController@add')->name('add');
    Route::post('add', 'AdController@create')->name('create');
    Route::put('edit/{id}', 'AdController@update')->name('update');
    Route::get('category/{category_id}', 'AdController@category')->name('category');
    Route::get('ad/{id}', 'AdController@ad')->name('ad');
});

// Мероприятния
Route::group(['namespace' => 'Event', 'as' => 'events.', 'prefix' => 'events'], function() {
    Route::get('/', 'EventController@index')->name('index');
    Route::get('section/{section_id}', 'EventController@section')->name('section');
    Route::get('event/{id}', 'EventController@event')->name('event');
});

//Форум
Route::group(['as' => 'forum.', 'prefix' => 'forum', 'namespace' => 'Forum'], function() {
    Route::get('/', 'TopicController@index')->name('index');
    Route::get('add', 'TopicController@add')->name('add');
    Route::post('add', 'TopicController@create')->name('create');
    Route::get('topic/{id}', 'TopicController@topic')->name('topic');
    Route::post('topic/{id}', 'TopicController@create_message')->name('create_message');
});

Route::get('road', 'PageController@road')->name('road');
Route::get('bus', 'PageController@bus')->name('bus');
Route::get('weather', 'PageController@weather')->name('weather');
Route::get('aero', 'PageController@aero')->name('aero');
Route::get('afisha', 'PageController@afisha')->name('afisha');

//Администрация
Route::group(['as' => 'administration.', 'prefix' => 'administration'], function() {
    Route::get('/', 'AdministrationController@index')->name('index');
    Route::get('complaint', 'AdministrationController@complaint')->name('complaint');
    Route::post('complaint', 'AbuseFormController@mail_to_administration')->name('mail_to_administration');
    Route::group(['prefix' => 'measure', 'as' => 'measure.'], function() {
        Route::get('/', 'AdministrationController@measures')->name('index');
        Route::get('show/{id}', 'AdministrationController@measure')->name('show');
    });
});

//Архив
Route::group(['as' => 'archive.', 'prefix' => 'archive'], function() {
    Route::get('/', 'ArchiveController@index')->name('index');
    Route::get('upload', 'ArchiveController@upload')->name('upload.file');
    Route::get('mayak', 'ArchiveController@mayak')->name('mayak');
    Route::get('mayak/{id}', 'ArchiveController@show_mayak')->name('show_mayak');
    Route::get('add', 'ArchiveController@add')->name('add');
    Route::post('add', 'ArchiveController@create')->name('create');
    Route::get('section/{id}', 'ArchiveController@section')->name('section');
});

//Лк
Route::group(['prefix' => 'lk', 'as' => 'lk.', 'middleware' => 'auth'], function() {
    Route::get('/', 'UserController@index')->name('index');
    Route::put('/', 'UserController@update')->name('update');
    Route::get('news', 'UserController@news')->name('news');
    Route::get('ads', 'UserController@ads')->name('ads');
    Route::get('events', 'UserController@events')->name('events');
    Route::get('balance', 'UserController@balance')->name('balance');
    Route::get('traffic_info', 'UserController@traffic_info')->name('traffic_info');
    Route::get('tariff', 'UserController@tariff')->name('tariff');
    Route::post('tariff', 'UserController@change_tariff')->name('change_tariff');
    Route::get('documents', 'UserController@documents')->name('documents');
    Route::post('yandex_pay', 'UserController@yandex_pay')->name('yandex_pay');
});


Route::post('ckeditor/image_upload', 'CKEditorController@upload')->name('upload.image');
Route::get('download/{id}', 'PageController@download_file')->name('download_file');

Route::group(['prefix' => 'comment', 'as' => 'comment.'], function() {
    Route::post('add', 'CommentController@create')->name('create');
});

//Админка
Route::group(['prefix' => 'admin', 'as' => 'admin.'], function(){
    Route::get('/', 'AdminController@index')->name('index');

    Route::group(['namespace' => 'Admin'], function() {
        //Новости
        Route::group(['prefix' => 'news', 'as' => 'news.'], function() {
            Route::get('all', 'NewsController@all')->name('all');
            Route::get('/', 'NewsController@new')->name('new');
            Route::get('edit/{id}', 'NewsController@edit')->name('edit');
            Route::delete('{id}', 'NewsController@delete')->name('delete');
            //Категории новостей
            Route::group(['prefix' => 'category', 'as' => 'category.'], function(){
                Route::get('/', 'NewsController@all_categories')->name('all');
                Route::post('/', 'NewsController@create_category')->name('create');
                Route::get('{id}', 'NewsController@edit_category')->name('edit');
                Route::put('{id}', 'NewsController@update_category')->name('update');
                Route::delete('{id}', 'NewsController@delete_category')->name('delete');
            });
            //Теги новостей
            Route::group(['prefix' => 'tags', 'as' => 'tags.'], function(){
                Route::get('/', 'NewsController@all_tags')->name('all');
                Route::post('new', 'NewsController@create_tag')->name('create');
                Route::delete('{id}', 'NewsController@delete_tag')->name('delete');
            });
            //Секции новостей
            Route::group(['prefix' => 'sections', 'as' => 'sections.'], function(){
                Route::get('/', 'NewsController@all_sections')->name('all');
                Route::post('new', 'NewsController@create_section')->name('create');
                Route::delete('{id}', 'NewsController@delete_section')->name('delete');
            });
        });

        //Объявления
        Route::group(['prefix' => 'ads', 'as' => 'ads.'], function(){
            Route::get('all', 'AdsController@all')->name('all');
            Route::get('/', 'AdsController@new')->name('new');
            Route::get('edit/{id}', 'AdsController@edit')->name('edit');
            Route::delete('{id}', 'AdsController@delete')->name('delete');
            //Категории объявлений
            Route::group(['prefix' => 'categories', 'as' => 'categories.'], function(){
                Route::get('/', 'AdsController@all_categories')->name('all');
                Route::post('/', 'AdsController@create_category')->name('create');
                Route::delete('{id}', 'AdsController@delete_category')->name('delete');
            });
            //Города объявлений
            Route::group(['prefix' => 'cities', 'as' => 'cities.'], function(){
                Route::get('/', 'AdsController@all_cities')->name('all');
                Route::post('new', 'AdsController@create_city')->name('create');
                Route::delete('{id}', 'AdsController@delete_city')->name('delete');
            });
        });

        //Мероприятия
        Route::group(['prefix' => 'events', 'as' => 'events.'], function(){
            Route::get('all', 'EventsController@all')->name('all');
            Route::get('/', 'EventsController@new')->name('new');
            Route::post('/', 'EventsController@create')->name('create');
            Route::get('edit/{id}', 'EventsController@edit')->name('edit');
            Route::put('edit/{id}', 'EventsController@update')->name('update');
            Route::delete('{id}', 'EventsController@delete')->name('delete');

            //Категории мероприятий
            Route::group(['prefix' => 'categories', 'as' => 'categories.'], function(){
                Route::get('/', 'EventsController@all_categories')->name('all');
                Route::post('/', 'EventsController@create_category')->name('create');
                Route::get('{id}', 'EventsController@edit_category')->name('edit');
                Route::put('{id}', 'EventsController@update_category')->name('update');
                Route::delete('{id}', 'EventsController@delete_category')->name('delete');
            });

            //Секции новостей
            Route::group(['prefix' => 'sections', 'as' => 'sections.'], function(){
                Route::get('/', 'EventsController@all_sections')->name('all');
                Route::post('new', 'EventsController@create_section')->name('create');
                Route::delete('{id}', 'EventsController@delete_section')->name('delete');
            });
        });

        //Пользователи
        Route::group(['prefix' => 'users', 'as' => 'users.'], function() {
            Route::get('/', 'UsersController@all')->name('all');
            Route::get('edit/{id}', 'UsersController@edit')->name('edit');
            Route::put('edit/{id}', 'UsersController@update')->name('update');
            Route::delete('delete/{id}', 'UsersController@delete')->name('delete');
        });

        //Группы
        Route::group(['prefix' => 'groups', 'as' => 'groups.'], function() {
            Route::get('/', 'GroupsController@all')->name('all');
            Route::post('/', 'GroupsController@create')->name('create');
            Route::get('edit/{id}', 'GroupsController@edit')->name('edit');
            Route::put('edit/{id}', 'GroupsController@update')->name('update');
            Route::delete('delete/{id}', 'GroupsController@delete')->name('delete');
        });

        //Опросы
        Route::group(['prefix' => 'poll', 'as' => 'poll.'], function(){
            Route::get('/', 'PollController@index')->name('all');
            Route::get('add', 'PollController@add')->name('add');
            Route::post('add', 'PollController@create')->name('create');
            Route::get('poll/{id}', 'PollController@edit')->name('edit');
            Route::put('poll/{id}', 'PollController@update')->name('update');
            Route::delete('poll/{id}', 'NewsController@delete')->name('delete');
        });

        //Дороги
        Route::group(['prefix' => 'road', 'as' => 'road.'], function(){
            Route::get('/', 'RoadController@index')->name('all');
            Route::get('add', 'RoadController@add')->name('add');
            Route::post('add', 'RoadController@create')->name('create');
            Route::get('road/{id}', 'RoadController@edit')->name('edit');
            Route::put('road/{id}', 'RoadController@update')->name('update');
            Route::delete('roll/{id}', 'RoadController@delete')->name('delete');

            //Автобусы
            Route::group(['prefix' => 'buses', 'as' => 'buses.', 'namespace' => 'Transport'], function() {
                Route::get('/', 'BusController@index')->name('all');
                Route::post('add', 'BusController@create')->name('create');
                Route::get('edit/{id}', 'BusController@edit')->name('edit');
                Route::put('edit/{id}', 'BusController@update')->name('update');

                // Остановки
                Route::group(['prefix' => 'stops', 'as' => 'stops.'], function() {
                    Route::get('/', 'StopController@index')->name('all');
                    Route::post('add', 'StopController@create')->name('create');
                    Route::get('edit/{id}', 'StopController@edit')->name('edit');
                    Route::put('edit/{id}', 'StopController@update')->name('update');

                    //Название
                    Route::group(['prefix' => 'names', 'as' => 'names.'], function() {
                        Route::get('/', 'StopNameController@index')->name('all');
                        Route::post('add', 'StopNameController@create')->name('create');
                        Route::get('edit/{id}', 'StopNameController@edit')->name('edit');
                        Route::put('edit/{id}', 'StopNameController@update')->name('update');
                    });

                    //Дни
                    Route::group(['prefix' => 'days', 'as' => 'days.'], function() {
                        Route::get('/', 'StopDayController@index')->name('all');
                        Route::post('add', 'StopDayController@create')->name('create');
                        Route::get('edit/{id}', 'StopDayController@edit')->name('edit');
                        Route::put('edit/{id}', 'StopDayController@update')->name('update');
                    });
                });
            });
            //Авиа
            Route::group(['prefix' => 'plane', 'as' => 'plane.'], function() {
                Route::get('/', 'PlaneController@index')->name('index');
                Route::get('add', 'PlaneController@add')->name('add');
                Route::post('add', 'PlaneController@create')->name('create');
                Route::get('edit/{id}', 'PlaneController@edit')->name('edit');
                Route::put('edit/{id}', 'PlaneController@update')->name('update');
                Route::delete('delete/{id}', 'PlaneController@delete')->name('delete');
                Route::group(['prefix' => 'status', 'as' => 'status.'], function() {
                    Route::get('/', 'PlaneController@statuses')->name('index');
                    Route::post('/', 'PlaneController@create_status')->name('create');
                    Route::get('edit/{id}', 'PlaneController@edit_status')->name('edit');
                    Route::put('edit/{id}', 'PlaneController@update_status')->name('update');
                    Route::delete('delete/{id}', 'PlaneController@delete_status')->name('delete');
                });
                Route::group(['prefix' => 'company', 'as' => 'company.'], function() {
                    Route::get('/', 'PlaneController@companies')->name('index');
                    Route::post('/', 'PlaneController@create_company')->name('create');
                    Route::get('edit/{id}', 'PlaneController@edit_company')->name('edit');
                    Route::put('edit/{id}', 'PlaneController@update_company')->name('update');
                    Route::delete('delete/{id}', 'PlaneController@delete_company')->name('delete');
                });
            });
        });

        //Архив
        Route::group(['prefix' => 'archive', 'as' => 'archive.'], function() {

            //Маяк
            Route::group(['prefix' => 'mayak', 'as' => 'mayak.'], function() {
                Route::get('/', 'MayakController@index')->name('index');
                Route::get('add', 'MayakController@add')->name('add');
                Route::post('add', 'MayakController@create')->name('create');
                Route::get('edit/{id}', 'MayakController@edit')->name('edit');
                Route::put('edit/{id}', 'MayakController@update')->name('update');
                Route::delete('delete/{id}', 'MayakController@delete')->name('delete');
            });

            //Разделы архива
            Route::group(['prefix' => 'sections', 'as' => 'sections.'], function() {
                Route::get('/', 'ArchiveController@sections')->name('index');
                Route::post('/', 'ArchiveController@create_section')->name('create');
                Route::get('{id}', 'ArchiveController@edit_section')->name('edit');
				Route::put('{id}', 'ArchiveController@update_section')->name('update');
				Route::delete('{id}', 'ArchiveController@delete_section')->name('delete');
				Route::get('show/{id}', 'ArchiveController@show')->name('show');
				Route::get('file/{id}', 'ArchiveController@show_file')->name('file.show');
				Route::put('file/{id}', 'ArchiveController@update_file')->name('file.update');
				Route::delete('file/{id}', 'ArchiveController@delete_file')->name('file.delete');
            });
        });

        //Фильмы
        Route::group(['prefix' => 'film', 'as' => 'film.'], function() {
            Route::get('/', 'FilmController@index')->name('index');
            Route::get('add', 'FilmController@add')->name('add');
            Route::post('add', 'FilmController@create')->name('create');
            Route::get('edit/{id}', 'FilmController@edit')->name('edit');
            Route::put('edit/{id}', 'FilmController@update')->name('update');
            Route::delete('delete/{id}', 'FilmController@delete')->name('delete');
        });

        //Системные
        Route::group(['prefix' => 'system', 'as' => 'system.'], function() {
            //Цвета
            Route::group(['prefix' => 'color', 'as' => 'color.'], function() {
                Route::get('/', 'SystemController@colors')->name('all');
                Route::post('/', 'SystemController@create_color')->name('create');
                Route::delete('{id}', 'SystemController@delete_color')->name('delete');
            });
            //Города
            Route::group(['prefix' => 'cities', 'as' => 'cities.'], function() {
                Route::get('/', 'CityController@index')->name('all');
                Route::post('/', 'CityController@create')->name('create');
                Route::get('{id}', 'CityController@edit')->name('edit');
                Route::put('{id}', 'CityController@update')->name('update');
            });
            //Погода
            Route::group(['prefix' => 'weather', 'as' => 'weather.'], function() {
                Route::get('/', 'WeatherController@index')->name('index');
                Route::post('/', 'WeatherController@create')->name('wind.create');
            });
        });

        //Администрация
        Route::group(['prefix' => 'administration', 'as' => 'administration.'], function() {
            //Меры поддержки
            Route::group(['prefix' => 'measure', 'as' => 'measure.'], function() {
                Route::get('/', 'MeasureController@index')->name('index');
                Route::get('add', 'MeasureController@add')->name('add');
                Route::post('add', 'MeasureController@create')->name('create');
                Route::get('edit/{id}', 'MeasureController@edit')->name('edit');
                Route::put('edit/{id}', 'MeasureController@update')->name('update');
                Route::delete('{id}', 'MeasureController@delete')->name('delete');
            });
        });

    });
});

Route::post('login', 'PageController@login')->name('login');
Route::get('logout', 'PageController@logout')->name('logout');
Route::get('change_city/{id}', 'PageController@change_city')->name('change_city');

Route::group(['namespace' => 'Cabinet', 'as' => 'cabinet.'], function() {
	Route::get('auth', 'AuthController@index')->name('auth');
	Route::group(['prefix' => 'cabinet'], function() {
		Route::get('/', 'IndexController@index')->name('home');
		Route::get('add_traffic', 'TrafficController@add_traffic')->name('add_traffic');
		Route::get('payment', 'PaymentController@payment')->name('payment');
		Route::get('change_tarif', 'TariffController@change')->name('change_tarif');
	});
});