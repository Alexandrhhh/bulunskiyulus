/*! lg-autoplay - v1.2.1 - 2020-06-13
* http://sachinchoolur.github.io/lightGallery
* Copyright (c) 2020 Sachin N; Licensed GPLv3 */

/*! lg-thumbnail - v1.2.1 - 2020-06-13
* http://sachinchoolur.github.io/lightGallery
* Copyright (c) 2020 Sachin N; Licensed GPLv3 */

/*! lg-video - v1.3.0 - 2020-05-03
* http://sachinchoolur.github.io/lightGallery
* Copyright (c) 2020 Sachin N; Licensed GPLv3 */

/*! lg-zoom - v1.2.1 - 2020-06-13
* http://sachinchoolur.github.io/lightGallery
* Copyright (c) 2020 Sachin N; Licensed GPLv3 */
