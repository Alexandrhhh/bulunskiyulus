@if((!$news->poll->current_user()->count()) && (!$news->poll->end_date || $news->poll->end_date > now()))
    <form id="poll" action="javascript:void(0);">
        <input hidden name="poll" value="{{ $news->poll->id }}">
        <input hidden name="news" value="{{ $news->id }}">
        <ul>
            @foreach ($news->poll->answers as $key => $answer)
                <li>
                    @if($news->poll->type == 0)
                        <input type="radio" name="answer" id="{{ $answer->id }}" value="{{ $answer->id }}">
                    @else
                        <input type="checkbox" name="answers[]" id="{{ $answer->id }}" value="{{ $answer->id }}">
                    @endif
                    <label for="{{ $answer->id }}">{{ $answer->title }}</label>
                </li>
            @endforeach
        </ul>
        <hr>
        <button type="submit" class="button small btn-poll" style="display: none">Голосовать</button>
    </form>
@else
    @if(!Auth::check() && $news->poll->current_user()->count())
        <ul>
            @foreach ($news->poll->answers as $key => $answer)
                <li>
                    @if($news->poll->type == 0)
                        <input type="radio" name="answer" id="{{ $answer->id }}" value="{{ $answer->id }}" @if($answer->current()->count()) checked @endif disabled>
                    @else
                        <input type="checkbox" name="answers[]" id="{{ $answer->id }}" value="{{ $answer->id }}" @if($answer->current()->count()) checked @endif disabled>
                    @endif
                    <label for="{{ $answer->id }}">
                        <span class="d-flex flex-column">
                            <span>{{ $answer->title }}</span>
                            <span class="line">
                                <span class="blue" style="width:{{ $answer->count/$news->poll->all_count() * 100 }}%"></span>
                            </span>
                        </span>
                    </label>
                    <span class="percent"><b>{{ round($answer->count/$news->poll->all_count() * 100) }}</b> %</span>
                </li>
            @endforeach
        </ul>
    @else
        <ul>
            @foreach ($news->poll->answers as $key => $answer)
                <li>
                    @if($news->poll->type == 0)
                        <input type="radio" name="answer" id="{{ $answer->id }}" value="{{ $answer->id }}" disabled>
                    @else
                        <input type="checkbox" name="answers[]" id="{{ $answer->id }}" value="{{ $answer->id }}" disabled>
                    @endif
                    <label for="{{ $answer->id }}">
                        <span class="d-flex flex-column">
                            <span>{{ $answer->title }}</span>
                            <span class="line">
                                <span class="blue" style="width:{{ round($answer->count/$news->poll->all_count() * 100) }}%"></span>
                            </span>
                        </span>
                    </label>
                    <span class="percent"><b>{{ round($answer->count/$news->poll->all_count() * 100) }}</b> %</span>
                </li>
            @endforeach
        </ul>
    @endif
@endif
