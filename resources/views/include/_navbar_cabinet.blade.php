<div class="bar d-flex my-2">
	<a class="to_back" href="#" onclick="window.history.back(); return false;">
		<i class="icon back"></i>
	</a>
	<ul>
		@foreach ($links as $link)
			@if(!$loop->last)
				<li><a href="{{ $link['url'] }}">{{ $link['title'] }}</a></li>
			@else
				<li>{{ $link['title'] }}</li>
			@endif
		@endforeach
	</ul>
</div>