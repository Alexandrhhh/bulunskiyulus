<ul class="nav nav-pills mb-3">
    <li class="nav-item"><a href="{{ route('lk.index') }}" class="nav-link @if(Route::is('lk.index')) active @endif">Личные данные</a></li>
    <li class="nav-item"><a href="{{ route('lk.balance') }}" class="nav-link @if(Route::is('lk.balance')) active @endif">Баланс аккаунта</a></li>
    <li class="nav-item"><a href="{{ route('lk.traffic_info') }}" class="nav-link @if(Route::is('lk.traffic_info')) active @endif">Информация по трафику</a></li>
    <li class="nav-item"><a href="{{ route('lk.tariff') }}" class="nav-link @if(Route::is('lk.tariff')) active @endif">Тарифный план</a></li>
</ul>
