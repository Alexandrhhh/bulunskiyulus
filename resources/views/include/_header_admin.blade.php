<nav class="navbar navbar-expand-md navbar-dark bg-dark sticky-top">
    <a class="navbar-brand" href="/">БулунскийПортал</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            {{-- @if(Auth::user()->hasPermission('admin.news')) --}}
                <li class="nav-item dropdown @if(Route::is('admin.news.*')) active @endif">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Новости</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown01">
                        <a class="dropdown-item @if(Route::is('admin.news.all')) active @endif" href="{{route('admin.news.all')}}">Все новости</a>
                        <a class="dropdown-item @if(Route::is('admin.news.new')) active @endif" href="{{route('admin.news.new')}}">Добавить новую</a>
                        <a class="dropdown-item @if(Route::is('admin.news.category.all')) active @endif" href="{{route('admin.news.category.all')}}">Категории</a>
                        <a class="dropdown-item @if(Route::is('admin.news.tags.all')) active @endif" href="{{route('admin.news.tags.all')}}">Теги</a>
                        <a class="dropdown-item @if(Route::is('admin.news.sections.all')) active @endif" href="{{route('admin.news.sections.all')}}">Разделы</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item @if(Route::is('admin.poll.all')) active @endif" href="{{ route('admin.poll.all') }}">Все опросы</a>
                        <a class="dropdown-item @if(Route::is('admin.poll.add')) active @endif" href="{{ route('admin.poll.add') }}">Создать новый</a>
                    </div>
                </li>
            {{-- @endif --}}
            {{-- @if(Auth::user()->hasPermission('admin.ads')) --}}
                <li class="nav-item dropdown @if(Route::is('admin.ads.*')) active @endif">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Объявления</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown01">
                        <a class="dropdown-item @if(Route::is('admin.ads.all')) active @endif" href="{{ route('admin.ads.all') }}">Все объявления</a>
                        <a class="dropdown-item @if(Route::is('admin.ads.new')) active @endif" href="{{ route('admin.ads.new') }}">Добавить новое</a>
                        <a class="dropdown-item @if(Route::is('admin.ads.categories.*')) active @endif" href="{{ route('admin.ads.categories.all') }}">Категории</a>
                        <a class="dropdown-item @if(Route::is('admin.ads.cities.*')) active @endif" href="{{ route('admin.ads.cities.all') }}">Города</a>
                    </div>
                </li>
            {{-- @endif --}}
            {{-- @if(Auth::user()->hasPermission('admin.events')) --}}
                <li class="nav-item dropdown @if(Route::is('admin.events.*')) active @endif">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Афиша</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown01">
                        <a class="dropdown-item @if(Route::is('admin.events.all')) active @endif" href="{{ route('admin.events.all') }}">Все события</a>
                        <a class="dropdown-item @if(Route::is('admin.events.new')) active @endif" href="{{ route('admin.events.new') }}">Создать новое</a>
                        <a class="dropdown-item @if(Route::is('admin.events.categories.*')) active @endif" href="{{ route('admin.events.categories.all') }}">Категории</a>
                            <a class="dropdown-item @if(Route::is('admin.events.sections.all')) active @endif" href="{{route('admin.events.sections.all')}}">Разделы</a>
                            <a class="dropdown-item @if(Route::is('admin.film.*')) active @endif" href="{{route('admin.film.index')}}">Киноафиша</a>
                    </div>
                </li>
            {{-- @endif --}}
            {{-- @if(Auth::user()->hasPermission('admin.road')) --}}
                <li class="nav-item dropdown @if(Route::is('admin.road.*')) active @endif">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Транспорт</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown01">
                        <a class="dropdown-item @if(Route::is('admin.road.all')) active @endif" href="{{ route('admin.road.all') }}">Дороги</a>
                        <a class="dropdown-item @if(Route::is('admin.road.buses.*')) active @endif" href="{{ route('admin.road.buses.all') }}">Автобусы</a>
                        <a class="dropdown-item @if(Route::is('admin.road.plane.*')) active @endif" href="{{ route('admin.road.plane.index') }}">Авиа</a>

                    </div>
                </li>
            {{-- @endif --}}
            {{-- @if(Auth::user()->hasPermission('admin.road')) --}}
                <li class="nav-item dropdown @if(Route::is('admin.archive.*')) active @endif">
					<a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Архив</a>
					<div class="dropdown-menu" aria-labelledby="dropdown01">
						<a class="dropdown-item @if(Route::is('admin.archive.sections.*')) active @endif" href="{{ route('admin.archive.sections.index') }}">Архив</a>
						<a class="dropdown-item @if(Route::is('admin.archive.mayak.*')) active @endif" href="{{ route('admin.archive.mayak.index') }}">Маяк</a>
                    </div>
                </li>
            {{-- @endif --}}
            {{-- @if(Auth::user()->hasPermission('admin.road')) --}}
                <li class="nav-item dropdown @if(Route::is('admin.administration.*')) active @endif">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Администрация</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown01">
                        <a class="dropdown-item @if(Route::is('admin.administration.measure.*')) active @endif" href="{{ route('admin.administration.measure.index') }}">Меры поддержки</a>
                    </div>
                </li>
            {{-- @endif --}}
            {{-- @if(Auth::user()->hasPermission('admin.users')) --}}
                <li class="nav-item dropdown @if(Route::is('admin.users.*')) active @endif">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Пользователи</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown01">
                        <a class="dropdown-item @if(Route::is('admin.users.*')) active @endif" href="{{route('admin.users.all')}}">Все</a>
                        <a class="dropdown-item @if(Route::is('admin.groups.*')) active @endif" href="{{route('admin.groups.all')}}">Группы</a>

                    </div>
                </li>
            {{-- @endif --}}
            {{-- @if(Auth::user()->hasPermission('admin.groups')) --}}
            <li class="nav-item dropdown @if(Route::is('admin.system.*')) active @endif">
                <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Системные</a>
                <div class="dropdown-menu" aria-labelledby="dropdown01">
                    <a class="dropdown-item @if(Route::is('admin.system.cities.all')) active @endif" href="{{ route('admin.system.cities.all') }}">Города</a>
                    <a class="dropdown-item @if(Route::is('admin.system.color.all')) active @endif" href="{{ route('admin.system.color.all') }}">Цвета</a>
                    <a class="dropdown-item @if(Route::is('admin.system.weather.index')) active @endif" href="{{ route('admin.system.weather.index') }}">Погода</a>
                </div>
            </li>
            {{-- @endif --}}
        </ul>
    </div>
</nav>
