<header class="position-sticky">
	<div class="content">
		<a href="/auth" class="logo">Булунский Портал</a>
		<nav>
			<ul>
				<li class="active"><a href="/cabinet">Главная</a></li>
				<li><a href="#">Устройства</a></li>
				<li><a href="#">Тарифы</a></li>
				<li><a href="#">Оплата</a></li>
			</ul>
		</nav>
		<div class="links">
			<a href="#" class="link">Перейти на сайт</a>
			<a href="#" class="link dropdown"><i class="icon account"></i>Аккаунт</a>
		</div>
		<ul class="dropdown">
			<li><a href="#">Первая</a></li>
			<li><a href="#">Вторая</a></li>
			<li><a href="#">Третья</a></li>
		</ul>
	</div>
</header>