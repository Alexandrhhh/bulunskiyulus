@extends('layouts.admin')

@section('title')
    Добавление новости - АдминПанель - Булунский Портал
@endsection

@section('content')
    <div class="container my-2">
        <div class="col-12">
            <form action="{{route('admin.administration.measure.create')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label>Заголовок</label>
                    <input type="text" name="title" class="form-control" placeholder="Введите заголовок" required>
                </div>
                <div class="form-group">
                    <label>Короткое описание</label>
                    <textarea name="short" class="form-control" required></textarea>
                </div>
                <div class="form-group">
                    <label>Полное описание</label>
                    <textarea name="content" id="content" required></textarea>
                </div>
                <div class="form-group">
                    <label>Изображение</label>
                    <input type="file" name="image" class="form-control-file" required>
                </div>
                <div class="form-group">
                    <label>Файлы <span class="badge badge-secondary">Выберите все необходимые файлы</span></label>
                    <input type="file" name="docs[]" class="form-control-file" required multiple>
                </div>
                <button type="submit" class="btn btn-primary">Добавить</button>
            </form>
            <script src="{{ asset('js/ckeditor/ckeditor.js') }}"></script>
            <script>
                CKEDITOR.replace( 'content', {
                    filebrowserUploadUrl: "{{route('upload.image', ['_token' => csrf_token() ])}}",
                    filebrowserUploadMethod: 'form'
                });
            </script>
        </div>
    </div>
@endsection
