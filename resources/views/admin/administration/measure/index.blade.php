@extends('layouts.admin')

@section('title')
    Меры поддержки - АдминПанель - Булунский Портал
@endsection

@section('content')
    <div class="table-responsive">
        <table class="table table-sm">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Название</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                @if($measures->count() != 0)
                    @foreach ($measures as $key => $measure)
                        <tr>
                            <td scope="col" class="align-middle">{{$measure->id}}</td>
                            <td class="align-middle">{{$measure->title}}</td>
                            <td class="align-middle text-right">
                                <div class="d-flex justify-content-end">
                                    <a href="{{route('admin.administration.measure.edit', ['id' => $measure->id])}}" role="button" class="btn btn-sm btn-success"><span class="oi oi-pencil"></span></a>
                                    <form action="{{route('admin.administration.measure.delete', ['id' => $measure->id])}}" method="post">
                                        @csrf
                                        @method('delete')
                                        <button type="sumbit" class="btn btn-danger btn-sm ml-2" onclick="return confirm('Удалить {{$measure->title}}?');">
                                            <span class="oi oi-trash"></span>
                                        </button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="3">Мер нет</td>
                    </tr>
                @endif
            </tbody>
        </table>
    </div>
    <a href="{{ route('admin.administration.measure.add') }}" class="btn btn-sm btn-success ml-2">Добавить</a>
@endsection
