@extends('layouts.admin')

@section('title')
    Добавление новости - АдминПанель - Булунский Портал
@endsection

@section('content')
    <div class="container my-2">
        <div class="col-12">
            <form action="{{route('admin.administration.measure.update', ['id' => $measure->id])}}" method="post" enctype="multipart/form-data">
                @csrf
                @method('put')
                <div class="form-group">
                    <label>Заголовок</label>
                    <input type="text" name="title" class="form-control" value="{{ $measure->title }}" placeholder="Введите заголовок" required>
                </div>
                <div class="form-group">
                    <label>Короткое описание</label>
                    <textarea name="short" class="form-control" required>{{ $measure->short }}</textarea>
                </div>
                <div class="form-group">
                    <label>Полное описание</label>
                    <textarea name="content" id="content" required>{!!  html_entity_decode($measure->content) !!}</textarea>
                </div>
                <div class="form-group">
                    @if($measure->image)<img src="{{ Storage::url($measure->image) }}" style="max-width: 200px;" alt=""><br>@endif
                    <label>Изображение <span class="badge badge-secondary">выберите чтобы изменить текущее</span></label>
                    <input type="file" name="image" class="form-control-file">
                </div>
                <div class="form-group">
                    <label>Файлы <span class="badge badge-secondary">Можно выбрать несколько (предыдущие будут удалены)</span></label>
                    <input type="file" name="docs[]" class="form-control-file" multiple>
                    @if($measure->files)
                        <div class="d-block"><b>Сейчас прикреплены</b></div>
                        <ul class="list-group">
                            @foreach ($measure->files as $key => $file)
                                <li class="list-group-item"><img src="/images/filetype/{{ $file->extension }}.svg" style="max-width: 30px;margin-right: 10px;" alt="">{{ $file->title }}</li>
                            @endforeach
                        </ul>
                    @endif
                </div>
                <button type="submit" class="btn btn-primary">Обновить</button>
            </form>
            <script src="{{ asset('js/ckeditor/ckeditor.js') }}"></script>
            <script>
                CKEDITOR.replace( 'content', {
                    filebrowserUploadUrl: "{{route('upload.image', ['_token' => csrf_token() ])}}",
                    filebrowserUploadMethod: 'form'
                });
            </script>
        </div>
    </div>
@endsection
