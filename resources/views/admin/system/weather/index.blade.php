@extends('layouts.admin')

@section('title')
    Цвета - АдминПанель - Булунский портал
@endsection

@section('content')
    <div class="table-responsive">
        <table class="table table-sm">
            <thead class="thead-dark">
                <th>#</th>
                <th class="w-100">Направление ветра</th>
            </thead>
            <tbody>
                @foreach ($winds as $key => $wind)
                    <tr>
                        <td class="align-middle">{{$wind->id}}</td>
                        <td class="align-middle">{{$wind->title}}</td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="4" class="p-2">
                        <form action="{{route('admin.system.weather.wind.create')}}" method="post">
                            @csrf
                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-1 col-form-label">Новый</label>
                                <div class="col-sm-2">
                                    <input type="text" name="title" class="form-control" placeholder="Название">
                                </div>
                                <div class="col-sm-2">
                                    <button type="submit" name="button" class="btn btn-primary">Создать</button>
                                </div>
                            </div>
                        </form>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="table-responsive">
        <table class="table">
            <thead>
            <th></th>
            @foreach($alarmTerms->groupBy('wind') as $wind => $alarms)
                <th>{{ str_replace(';' , ' - ', $wind) }}</th>
            @endforeach
            </thead>
            <tbody>
            @foreach($alarmTerms->groupBy('temperature') as $temperature => $alarms)
                <tr>
                    <td>{{ str_replace(';', ' ', $temperature) }}</td>
                    @foreach($alarms as $alarm)
                        <td>{{ $alarm->alert }}</td>
                    @endforeach
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
