@extends('layouts.admin')

@section('title')
    Цвета - АдминПанель - Булунский портал
@endsection

@section('content')
    <div class="table-responsive">
        <table class="table table-sm">
            <thead class="thead-dark">
                <th class="w-100">Название</th>
                <th></th>
            </thead>
            <tbody>
                @foreach ($colors as $key => $color)
                    <tr>
                        <td class="align-middle"><span class="category {{$color->eng}}">{{$color->title}}</span></td>
                        <td class="align-middle text-right">
                            <form action="{{route('admin.system.color.delete', ['id' => $color->id])}}" method="post">
                                @csrf
                                @method('delete')
                                <button type="sumbit" class="btn btn-danger btn-sm" onclick="return confirm('Удалить категорию {{$color->title}}?');">
                                    <span class="oi oi-trash"></span>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="4" class="p-2">
                        <form action="{{route('admin.system.color.create')}}" method="post">
                            @csrf
                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-1 col-form-label">Новый</label>
                                <div class="col-sm-2">
                                    <input type="text" name="title" class="form-control" placeholder="Название">
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="eng" class="form-control" placeholder="Eng SCSS">
                                </div>
                                <div class="col-sm-2">
                                    <button type="submit" name="button" class="btn btn-primary">Создать</button>
                                </div>
                            </div>
                        </form>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
@endsection
