@extends('layouts.admin')

@section('title')
    Редактирование города - АдминПанель - Булунский Портал
@endsection

@section('content')
    <div class="container my-2">
        <div class="col-12">
            <form action="{{route('admin.system.cities.update', ['id' => $city->id])}}" method="post">
                @csrf
                @method('put')
                <div class="form-group">
                    <label>Название</label>
                    <input type="text" name="title" class="form-control" placeholder="Введите название" required value="{{ $city->title }}">
                </div>
                <div class="form-group">
                    <label>Широта</label>
                    <input type="text" name="latitude" class="form-control" placeholder="Введите широту" required value="{{ $city->latitude }}">
                </div>
                <div class="form-group">
                    <label>Долгота</label>
                    <input type="text" name="longitude" class="form-control" placeholder="Введите долготу" required value="{{ $city->longitude }}">
                </div>
                <button type="submit" class="btn btn-primary" name="create">Обновить</button>
            </form>
        </div>
    </div>
@endsection
