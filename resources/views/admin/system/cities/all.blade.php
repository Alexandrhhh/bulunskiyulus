@extends('layouts.admin')

@section('title')
    Города - АдминПанель - Булунский портал
@endsection

@section('content')
    <div class="table-responsive">
        <table class="table table-sm">
            <thead class="thead-dark">
                <th class="w-100">Название</th>
                <th></th>
            </thead>
            <tbody>
                @foreach ($cities as $key => $city)
                    <tr>
                        <td class="align-middle">{{$city->title}}</td>
                        <td class="align-middle text-right d-flex justify-content-end">
                            <a href="{{ route('admin.system.cities.edit', ['id' => $city->id]) }}" class="btn btn-sm btn-success"><span class="oi oi-pencil"></span></a>
                        </td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="4" class="p-2">
                        <form action="{{route('admin.system.cities.create')}}" method="post">
                            @csrf
                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-1 col-form-label">Новый</label>
                                <div class="col-sm-2">
                                    <input type="text" name="title" class="form-control" placeholder="Название" required value="{{ old('title') }}">
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="latitude" class="form-control" placeholder="Ширина" required value="{{ old('latitude') }}">
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="longitude" class="form-control" placeholder="Долгота" required value="{{ old('longitude') }}">
                                </div>
                                <div class="col-sm-2">
                                    <button type="submit" name="button" class="btn btn-primary">Создать</button>
                                </div>
                            </div>
                        </form>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
@endsection
