@extends('layouts.admin')

@section('title')
    Добавление объявление - АдминПанель - Булунский Портал
@endsection

@section('content')
    <div class="container my-2">
        <div class="col-12">
            <form action="{{route('ads.create')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label>Заголовок</label>
                    <input type="text" name="title" class="form-control" placeholder="Введите заголовок" required>
                </div>
                <div class="form-group">
                    <label>Город</label>
                    <select name="city_id" class="form-control dropdown" id="city" required>
                        @foreach ($cities as $key => $city)
                            <option value="{{$city->id}}">{{$city->title}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Категория</label>
                    <select name="category_id" class="form-control dropdown" required>
                        @foreach ($categories as $key => $category)
                            <option value="{{$category->id}}">{{$category->title}}</option>
                        @endforeach
                    </select>
                </div>
                <label>Изображения</label>
                <div class="custom-file mb-3 form-group">
                    <input type="file" name="images[]" class="custom-file-input" id="customFileLangHTML" multiple required>
                    <label class="custom-file-label" for="customFileLangHTML" data-browse="Выбрать">Изображения</label>
                </div>
                <div class="form-group">
                    <label>Описание</label>
                    <textarea name="text" class="form-control" placeholder="Описание товара" required></textarea>
                </div>
                <div class="form-group">
                    <label>Цена</label>
                    <input type="number" name="price" min-value="0" step="1" class="form-control" placeholder="500" required>
                </div>
                <div class="form-group">
                    <label>Адрес</label>
                    <input type="text" name="addres" class="form-control" placeholder="ул. Шевченко, д.15" required>
                </div>
                <div class="form-group">
                    <label>Телефон</label>
                    <input type="text" name="phone" class="form-control phone" required>
                </div>
                <button type="submit" class="btn btn-primary">Добавить</button>
            </form>
        </div>
    </div>
@endsection
