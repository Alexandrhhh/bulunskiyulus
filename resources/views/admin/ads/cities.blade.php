@extends('layouts.admin')

@section('title')
    Города - Объявления - АндминПанель - Булунский портал
@endsection

@section('content')
    <div class="table-responsive">
        <table class="table table-sm">
            <thead class="thead-dark">
                <th>#</th>
                <th>Название</th>
                <th>Объявлений в городе</th>
                <th></th>
            </thead>
            <tbody>
                @foreach ($cities as $key => $city)
                    <tr>
                        <td class="align-middle">{{$city->id}}</td>
                        <td class="align-middle">{{$city->title}}</td>
                        <td class="align-middle">{{$city->ads->count()}}</td>
                        <td class="align-middle text-right">
                            <form action="{{route('admin.ads.cities.delete', ['id' => $city->id])}}" method="post">
                                @csrf
                                @method('delete')
                                <button type="sumbit" class="btn btn-danger btn-sm" onclick="return confirm('Удалить город {{$city->title}}? Это удалит и {{$city->ads->count()}} объявлений.');">
                                    <span class="oi oi-trash"></span>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="4" class="p-2">
                        <form action="{{route('admin.ads.cities.create')}}" method="post">
                            @csrf
                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-1 col-form-label">Название</label>
                                <div class="col-sm-2">
                                    <input type="text" name="title" class="form-control">
                                </div>
                                <div class="col-sm-2">
                                    <button type="submit" name="button" class="btn btn-primary">Создать</button>
                                </div>
                            </div>
                        </form>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
@endsection
