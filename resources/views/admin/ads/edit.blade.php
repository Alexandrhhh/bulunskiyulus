@extends('layouts.admin')

@section('title')
    Редактирование объявления - АдминПанель - Булунский Портал
@endsection

@section('content')
    <div class="container my-2">
        <div class="col-12">
            <form action="{{route('ads.update', ['id' => $ad->id])}}" method="post" enctype="multipart/form-data">
                @csrf
                @method('put')
                <div class="form-group">
                    <label>Заголовок</label>
                    <input type="text" name="title" class="form-control" placeholder="Введите заголовок" value="{{ $ad->title }}" required>
                </div>
                <div class="form-group">
                    <label>Город</label>
                    <select name="city_id" class="form-control dropdown" required>
                        @foreach ($cities as $key => $city)
                            <option value="{{$city->id}}" @if($ad->city_id == $city->id) selected @endif>{{$city->title}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Категория</label>
                    <select name="category_id" class="form-control dropdown" required>
                        @foreach ($categories as $key => $category)
                            <option value="{{$category->id}}" @if($ad->category_id == $category->id) selected @endif>{{$category->title}}</option>
                        @endforeach
                    </select>
                </div>
                <label>Изображения</label>
                @foreach ($ad->photos as $key => $photo)
                    <img src="{{ Storage::url($photo->url) }}" style="max-width: 200px;" alt="">
                @endforeach
                <div class="custom-file mb-3 form-group">
                    <input type="file" name="images[]" class="custom-file-input" id="customFileLangHTML" multiple>
                    <label class="custom-file-label" for="customFileLangHTML" data-browse="Выбрать">Изображения</label>
                </div>
                <div class="form-group">
                    <label>Описание</label>
                    <textarea name="text" class="form-control" required>{{ $ad->text }}</textarea>
                </div>
                <div class="form-group">
                    <label>Цена</label>
                    <input type="number" name="price" min-value="0" step="1" class="form-control" value="{{ $ad->price }}" required>
                </div>
                <div class="form-group">
                    <label>Адрес</label>
                    <input type="text" name="addres" class="form-control" placeholder="ул. Шевченко, д.15" required value="{{ $ad->addres }}">
                </div>
                <div class="form-group">
                    <label>Телефон</label>
                    <input type="text" name="phone" class="form-control phone" required value="{{ $ad->phone }}">
                </div>
                <button type="submit" class="btn btn-primary">Обновить</button>
            </form>
        </div>
    </div>
@endsection
