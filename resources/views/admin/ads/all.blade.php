@extends('layouts.admin')

@section('title')
    Все объявления - АдминПанель - Булунский портал
@endsection

@section('content')
    <div class="table-responsive">
        <table class="table table-sm">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Город</th>
                    <th scope="col">Товар</th>
                    <th scope="col">Категория</th>
                    <th scope="col">Цена</th>
                    <th scope="col">Описание</th>
                    <th scope="col">Дата публикации</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                @if($ads->count() != 0)
                    @foreach ($ads as $key => $ad)
                        <tr>
                            <td scope="col" class="align-middle">{{$ad->id}}</td>
                            <td class="align-middle">{{$ad->city->title}}</td>
                            <td class="align-middle">{{$ad->title}}</td>
                            <td class="align-middle">{{$ad->category->title}}</td>
                            <td class="align-middle">{{$ad->price}}</td>
                            <td class="align-middle">{{Str::limit($ad->text, 300, '...')}}</td>
                            <td class="align-middle">{{$ad->created_at->format('d.m.Y в H:i')}}</td>
                            <td class="align-middle text-right">
                                <div class="d-flex justify-content-end">
                                    <a href="{{route('admin.ads.edit', ['id' => $ad->id])}}" role="button" class="btn btn-sm btn-success"><span class="oi oi-pencil"></span></a>
                                    <form action="{{route('admin.ads.delete', ['id' => $ad->id])}}" method="post">
                                        @csrf
                                        @method('delete')
                                        <button type="sumbit" class="btn btn-danger btn-sm ml-2" onclick="return confirm('Удалить новости {{$ad->title}}?');">
                                            <span class="oi oi-trash"></span>
                                        </button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="8">Объявлений нет</td>
                    </tr>
                @endif
            </tbody>
        </table>
        {{$ads->links()}}
    </div>
@endsection
