@extends('layouts.admin')

@section('title')
    Категории - Объявления - АндминПанель - Булунский портал
@endsection

@section('content')
    <div class="table-responsive">
        <table class="table table-sm">
            <thead class="thead-dark">
                <th>#</th>
                <th>Название</th>
                <th>Объявлений в категории</th>
                <th></th>
            </thead>
            <tbody>
                @foreach ($categories as $key => $category)
                    <tr>
                        <td class="align-middle">{{$category->id}}</td>
                        <td class="align-middle">{{$category->title}}</td>
                        <td class="align-middle">{{$category->ads->count()}}</td>
                        <td class="align-middle text-right">
                            <form action="{{route('admin.ads.categories.delete', ['id' => $category->id])}}" method="post">
                                @csrf
                                @method('delete')
                                <button type="sumbit" class="btn btn-danger btn-sm" onclick="return confirm('Удалить категорию {{$category->title}}? Это удалит и {{$category->ads->count()}} новостей.');">
                                    <span class="oi oi-trash"></span>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="4" class="p-2">
                        <form action="{{route('admin.ads.categories.create')}}" method="post">
                            @csrf
                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-1 col-form-label">Название</label>
                                <div class="col-sm-2">
                                    <input type="text" name="title" class="form-control">
                                </div>
                                <div class="col-sm-2">
                                    <button type="submit" name="button" class="btn btn-primary">Создать</button>
                                </div>
                            </div>
                        </form>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
@endsection
