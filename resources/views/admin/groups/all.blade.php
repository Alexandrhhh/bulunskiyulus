@extends('layouts.admin')

@section('title')
    Все группы - АдминПанель - Булунский портал
@endsection

@section('content')
    <div class="table-responsive">
        <table class="table table-sm">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Название</th>
                    <th scope="col">Описание</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($groups as $key => $group)
                    <tr>
                        <td class="align-middle">{{ $group->id }}</td>
                        <td class="align-middle">{{ $group->title }}</td>
                        <td class="align-middle">{{ $group->description }}</td>
                        <td class="align-middle">
                            <div class="d-flex justify-content-end">
                                <a href="{{route('admin.groups.edit', ['id' => $group->id])}}" role="button" class="btn btn-sm btn-success"><span class="oi oi-pencil"></span></a>
                                <form action="{{route('admin.groups.delete', ['id' => $group->id])}}" method="post">
                                    @csrf
                                    @method('delete')
                                    <button type="sumbit" class="btn btn-danger btn-sm" onclick="return confirm('Удалить группу {{ $group->title }}?');">
                                        <span class="oi oi-trash"></span>
                                    </button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="4" class="p-2">
                        <form action="{{route('admin.groups.create')}}" method="post">
                            @csrf
                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-1 col-form-label">Название</label>
                                <div class="col-sm-2">
                                    <input type="text" name="title" class="form-control" id="inputPassword">
                                </div>
                                <div class="col-sm-2">
                                    <button type="submit" name="button" class="btn btn-primary">Создать</button>
                                </div>
                            </div>
                        </form>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
@endsection
