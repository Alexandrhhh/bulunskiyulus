@extends('layouts.admin')

@section('title')
    Редактирование группы - АдминПанель - Булунский Портал
@endsection

@section('content')
    <div class="container my-2">
        <div class="col-12">
            <form action="{{route('admin.groups.update', ['id' => $group->id])}}" method="post" enctype="multipart/form-data">
                @csrf
                @method('put')
                <div class="form-group">
                    <label>Название</label>
                    <input type="text" name="title" class="form-control" placeholder="Введите заголовок" value="{{ $group->title }}">
                </div>
                <div class="form-group">
                    <label>Короткое описание</label>
                    <textarea class="form-control" name="description">{{ $group->description }}</textarea>
                </div>
                <div class="table-responsive">
                    <table class="table table-sm">
                        <thead>
                            <tr>
                                <th>Раздел</th>
                                <th class="text-center">Лайки</th>
                                <th class="text-center">Комментарии</th>
                                <th class="text-center">Добавление</th>
                                <th class="text-center">Редактировании</th>
                                <th class="text-center">Удаление</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Новости</td>
                                <td class="text-center"><input type="checkbox" name="permissions[news][like]" @if($group->hasPermission('news.like')) checked @endif></td>
                                <td class="text-center"><input type="checkbox" name="permissions[news][comment]" @if($group->hasPermission('news.comment')) checked @endif></td>
                                <td class="text-center"><input type="checkbox" name="permissions[news][add]" @if($group->hasPermission('news.add')) checked @endif></td>
                                <td class="text-center"><input type="checkbox" name="permissions[news][edit]" @if($group->hasPermission('news.edit')) checked @endif></td>
                                <td class="text-center"><input type="checkbox" name="permissions[news][delete]" @if($group->hasPermission('news.delete')) checked @endif></td>
                            </tr>
                            <tr>
                                <td>Объявления</td>
                                <td class="text-center"><input type="checkbox" name="permissions[ads][like]" @if($group->hasPermission('ads.like')) checked @endif></td>
                                <td class="text-center"><input type="checkbox" name="permissions[ads][comment]" @if($group->hasPermission('ads.comment')) checked @endif></td>
                                <td class="text-center"><input type="checkbox" name="permissions[ads][add]" @if($group->hasPermission('ads.add')) checked @endif></td>
                                <td class="text-center"><input type="checkbox" name="permissions[ads][edit]" @if($group->hasPermission('ads.edit')) checked @endif></td>
                                <td class="text-center"><input type="checkbox" name="permissions[ads][delete]" @if($group->hasPermission('ads.delete')) checked @endif></td>
                            </tr>
                            <tr>
                                <td>Мероприятия</td>
                                <td class="text-center"><input type="checkbox" name="permissions[events][like]" @if($group->hasPermission('events.like')) checked @endif></td>
                                <td class="text-center"><input type="checkbox" name="permissions[events][comment]" @if($group->hasPermission('events.comment')) checked @endif></td>
                                <td class="text-center"><input type="checkbox" name="permissions[events][add]" @if($group->hasPermission('events.add')) checked @endif></td>
                                <td class="text-center"><input type="checkbox" name="permissions[events][edit]" @if($group->hasPermission('events.edit')) checked @endif></td>
                                <td class="text-center"><input type="checkbox" name="permissions[events][delete]" @if($group->hasPermission('events.delete')) checked @endif></td>
                            </tr>
                            <tr>
                                <td class="table-secondary" colspan="6">Дополнительные</td>
                            </tr>
                            <tr>
                                <td>Доступ к админке</td>
                                <td class="text-center"><input type="checkbox" name="permissions[admin][admin]" @if($group->hasPermission('admin.admin')) checked @endif></td>
                                <td colspan="4"></td>
                            </tr>
                            <tr>
                                <td>Новости</td>
                                <td class="text-center"><input type="checkbox" name="permissions[admin][news]" @if($group->hasPermission('admin.news')) checked @endif></td>
                                <td colspan="4"></td>
                            </tr>
                            <tr>
                                <td>Объявления</td>
                                <td class="text-center"><input type="checkbox" name="permissions[admin][ads]" @if($group->hasPermission('admin.ads')) checked @endif></td>
                                <td colspan="4"></td>
                            </tr>
                            <tr>
                                <td>Мероприятия</td>
                                <td class="text-center"><input type="checkbox" name="permissions[admin][events]" @if($group->hasPermission('admin.events')) checked @endif></td>
                                <td colspan="4"></td>
                            </tr>
                            <tr>
                                <td>Пользователи</td>
                                <td class="text-center"><input type="checkbox" name="permissions[admin][users]" @if($group->hasPermission('admin.users')) checked @endif></td>
                                <td colspan="4"></td>
                            </tr>
                            <tr>
                                <td>Группы</td>
                                <td class="text-center"><input type="checkbox" name="permissions[admin][groups]" @if($group->hasPermission('admin.groups')) checked @endif></td>
                                <td colspan="4"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <button type="submit" class="btn btn-primary">Обновить</button>
            </form>
        </div>
    </div>
@endsection
