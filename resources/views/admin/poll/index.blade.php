@extends('layouts.admin')

@section('title')
    Все опросы - АдминПанель - Булунский Портал
@endsection

@section('content')
    <div class="table-responsive">
        <table class="table table-sm">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Название</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($polls as $key => $poll)
                    <tr>
                        <td class="align-middle">{{ $poll->id }}</td>
                        <td class="align-middle">{{ $poll->title }}</td>
                        <td class="align-middle">
                            <div class="d-flex justify-content-end">
                                <a href="{{route('admin.poll.edit', ['id' => $poll->id])}}" role="button" class="btn btn-sm btn-success"><span class="oi oi-pencil"></span></a>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
