@extends('layouts.admin')

@section('title')
    Добавление новости - АдминПанель - Булунский Портал
@endsection

@section('content')
    <div class="container my-2">
        <div class="col-12">
            <form action="{{route('admin.poll.create')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label>Заголовок</label>
                    <input type="text" name="title" class="form-control" placeholder="Введите заголовок" required>
                </div>
                <div class="form-group">
                    <label>Подзаголовок</label>
                    <input type="text" name="subtitle" class="form-control" placeholder="Необязательно">
                </div>
                <div class="form-group">
                    <label>Дата окончания</label>
                    <input type="text" name="end_date" class="datepicker-here form-control" data-date-format="yyyy-mm-dd" data-time-format="hh:ii:00" data-timepicker="true"/>
                </div>
                <div class="form-group">
                    <label>Тип</label>
                    <select name="type" class="form-control dropdown" required>
                        <option value="0">Один вариант ответа</option>
                        <option value="1">Несколько вариантов ответа</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Ответы</label>
                    <input type="text" class="form-control mb-2" name="answer[]">
                    <input type="text" class="form-control mb-2 d-none" name="answer[]">
                    <a href="#copy" class="btn btn-success">+</a>
                </div>
                <button type="submit" class="btn btn-primary" name="create">Добавить</button>
            </form>
        </div>
    </div>
@endsection
