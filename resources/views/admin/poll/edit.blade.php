@extends('layouts.admin')

@section('title')
    Добавление новости - АдминПанель - Булунский Портал
@endsection

@section('content')
    <div class="container my-2">
        <div class="col-12">
            <form action="{{route('admin.poll.update', ['id' => $poll->id])}}" method="post" enctype="multipart/form-data">
                @csrf
                @method('put')
                <div class="form-group">
                    <label>Заголовок</label>
                    <input type="text" name="title" class="form-control" placeholder="Введите заголовок" required value="{{ $poll->title }}">
                </div>
                <div class="form-group">
                    <label>Подзаголовок</label>
                    <input type="text" name="subtitle" class="form-control" placeholder="Необязательно">
                </div>
                <div class="form-group">
                    <label>Дата окончания</label>
                    <input type="text" name="end_date" class="datepicker-here form-control" data-date-format="yyyy-mm-dd" data-time-format="hh:ii:00" data-timepicker="true" value="{{ $poll->end_date }}"/>
                </div>
                <div class="form-group">
                    <label>Тип</label>
                    <select name="type" class="form-control dropdown" required>
                        <option value="0" @if($poll->type == 0) selected @endif>Один вариант ответа</option>
                        <option value="1" @if($poll->type == 1) selected @endif>Несколько вариантов ответа</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Ответы</label>
                    @foreach ($poll->answers as $key => $answer)
                        <div class="form-row">
                            <div class="col-10">
                                <input type="text" class="form-control mb-2" name="answers[{{ $answer->id }}][title]" value="{{ $answer->title }}">
                            </div>
                            <div class="col-2">
                                <input type="number" class="form-control mb-2" name="answers[{{ $answer->id }}][count]" value="{{ $answer->count }}">
                            </div>
                        </div>
                    @endforeach
                    <input type="text" class="form-control mb-2 d-none" name="answer[]">
                    <a href="#copy" class="btn btn-success">+</a>
                </div>
                <button type="submit" class="btn btn-primary" name="create">Обновить</button>
            </form>
        </div>
    </div>
@endsection
