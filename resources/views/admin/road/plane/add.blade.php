@extends('layouts.admin')

@section('title')
    Добавление  - АдминПанель - Булунский Портал
@endsection

@section('content')
    <div class="container my-2">
        <div class="col-12">
            <form action="{{route('admin.road.plane.create')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label>Вылет из</label>
                    <input type="text" name="departure" class="form-control" placeholder="Пример: Тикси" required>
                </div>
                <div class="form-group">
                    <label>Прилет в</label>
                    <input type="text" name="arrival" class="form-control" placeholder="Пример: Якутск" required>
                </div>
                <div class="form-group">
                    <label>Время вылета</label>
                    <input type="text" name="departure_at" class="datepicker-here form-control" data-timepicker="true" placeholder="Пример: 12.10.2020 15:30" required>
                </div>
                <div class="form-group">
                    <label>Время прилета</label>
                    <input type="text" name="arrival_at" class="datepicker-here form-control" data-timepicker="true" placeholder="Пример: 12.10.2020 15:30" required>
                </div>
                <div class="form-group">
                    <label>Статус</label>
                    <select name="status_id" class="form-control dropdown" data-placeholder="Статус рейса" required>
                        @foreach ($statuses as $key => $status)
                            <option value="{{ $status->id }}">{{ $status->title }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Авиакомпания</label>
                    <select name="company_id" class="form-control dropdown" data-placeholder="Компания" required>
                        @foreach ($companies as $key => $company)
                            <option value="{{ $company->id }}">{{ $company->title }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Рейс</label>
                    <input type="text" name="flight" class="form-control" placeholder="Пример: ЯК431" required>
                </div>
                <div class="form-group">
                    <label>Кол-во минут после статуса</label>
                    <input type="number" name="time_at" class="form-control" placeholder="Пример: 60">
                    <small>Задержка рейса на XX минут, вылет отложен на XX минут</small>
                </div>
                <div class="form-check">
                    <input class="form-check-input" name="visible" type="checkbox" value="1" id="visible">
                    <label class="form-check-label" for="visible">
                        Отображать
                    </label>
                </div>
                <div class="form-group">
                    <label>Описание</label>
                    <textarea name="description" class="form-control autosize" placeholder="Любой текст"></textarea>
                    <small>Выводится на странице рейса</small>
                </div>
                <button type="submit" class="btn btn-primary" name="create">Добавить</button>
            </form>
        </div>
    </div>
@endsection
