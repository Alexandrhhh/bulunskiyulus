@extends('layouts.admin')

@section('title')
    Статусы рейсов - АдминПанель - Булунский портал
@endsection

@section('content')
    <div class="table-responsive">
        <table class="table table-sm">
            <thead class="thead-dark">
                <th>Изображение</th>
                <th>Название</th>
                <th></th>
            </thead>
            <tbody>
                @foreach ($companies as $key => $company)
                    <tr>
                        <td class="align-middle"><img src="{{ Storage::url($company->logotype) }}" alt="" style="max-width: 100px"></td>
                        <td class="align-middle">{{$company->title}}</td>
                        <td class="align-middle text-right d-flex justify-content-end">
                            <a href="{{ route('admin.road.plane.company.edit', ['id' => $company->id]) }}" class="btn btn-sm btn-success"><span class="oi oi-pencil"></span></a>
                        </td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="4" class="p-2">
                        <form action="{{route('admin.road.plane.company.create')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-1 col-form-label">Название</label>
                                <div class="col-sm-2">
                                    <input type="text" name="title" class="form-control" placeholder="Пример: Якутские авиалинии">
                                </div>
                                <div class="col-sm-2">
                                    <input type="file" name="logotype" class="form-control">
                                </div>
                                <div class="col-sm-2">
                                    <button type="submit" name="button" class="btn btn-primary">Создать</button>
                                </div>
                            </div>
                        </form>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
@endsection
