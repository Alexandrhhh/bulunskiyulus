@extends('layouts.admin')

@section('title')
    Добавление  - АдминПанель - Булунский Портал
@endsection

@section('content')
    <div class="container my-2">
        <div class="col-12">
            <form action="{{route('admin.road.plane.company.update', ['id' => $company->id])}}" method="post" enctype="multipart/form-data">
                @csrf
                @method('put')
                <div class="form-group">
                    <label>Название</label>
                    <input type="text" name="title" class="form-control" placeholder="Пример: Якутские авиалинии" value="{{ $company->title }}" required>
                </div>
                <div class="form-group">
                    <label>Логотип</label>
                    @if($company->logotype)<img src="{{ Storage::url($company->logotype) }}" style="max-width: 200px" alt=""> @endif
                    <input type="file" name="logotype" class="form-control" required>
                </div>
                <button type="submit" class="btn btn-primary" name="create">Обновить</button>
            </form>
        </div>
    </div>
@endsection
