@extends('layouts.admin')

@section('title')
    Статусы рейсов - АдминПанель - Булунский портал
@endsection

@section('content')
    <div class="table-responsive">
        <table class="table table-sm">
            <thead class="thead-dark">
                <th>Название</th>
                <th>Цвет</th>
                <th></th>
            </thead>
            <tbody>
                @foreach ($statuses as $key => $status)
                    <tr>
                        <td class="align-middle">{{$status->title}}</td>
                        <td class="align-middle"><span style="color:{{$status->color}}">{{$status->color}}</span></td>
                        <td class="align-middle text-right d-flex justify-content-end">
                            <a href="{{ route('admin.road.plane.status.edit', ['id' => $status->id]) }}" class="btn btn-sm btn-success"><span class="oi oi-pencil"></span></a>
                        </td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="4" class="p-2">
                        <form action="{{route('admin.road.plane.status.create')}}" method="post">
                            @csrf
                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-1 col-form-label">Название</label>
                                <div class="col-sm-2">
                                    <input type="text" name="title" class="form-control" placeholder="Пример: По рассписанию">
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="color" class="form-control" placeholder="Цвет: #ff0000">
                                </div>
                                <div class="col-sm-2">
                                    <button type="submit" name="button" class="btn btn-primary">Создать</button>
                                </div>
                            </div>
                        </form>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
@endsection
