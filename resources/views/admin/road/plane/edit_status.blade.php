@extends('layouts.admin')

@section('title')
    Добавление  - АдминПанель - Булунский Портал
@endsection

@section('content')
    <div class="container my-2">
        <div class="col-12">
            <form action="{{route('admin.road.plane.status.update', ['id' => $status->id])}}" method="post" enctype="multipart/form-data">
                @csrf
                @method('put')
                <div class="form-group">
                    <label>Название</label>
                    <input type="text" name="title" class="form-control" placeholder="Пример: По расписанию" value="{{ $status->title }}" required>
                </div>
                <div class="form-group">
                    <label>Цвет</label>
                    <input type="text" name="arrival" class="form-control" placeholder="Пример: #ff0000" value="{{ $status->color }}" required>
                </div>
                <button type="submit" class="btn btn-primary" name="create">Обновить</button>
            </form>
        </div>
    </div>
@endsection
