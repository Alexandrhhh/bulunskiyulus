@extends('layouts.admin')

@section('title')
    Все рейсы - АдминПанель - Булунский Портал
@endsection

@section('content')
    <div class="table-responsive">
        <table class="table table-sm">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Вылет из</th>
                    <th scope="col">Прилет в</th>
                    <th scope="col">Время вылета</th>
                    <th scope="col">Время прилета</th>
                    <th scope="col">Текущий статус</th>
                    <th scope="col">Добавленное</th>
                    <th scope="col">Описание</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                @if($planes->count() != 0)
                    @foreach ($planes as $key => $plane)
                        <tr>
                            <td class="align-middle">{{$plane->id}}</td>
                            <td class="align-middle">{{$plane->departure}}</td>
                            <td class="align-middle">{{$plane->arrival}}</td>
                            <td class="align-middle">{{$plane->departure_at->format('d.m.Y H:i')}}</td>
                            <td class="align-middle">{{$plane->arrival_at->format('d.m.Y H:i')}}</td>
                            <td class="align-middle">{{$plane->status->title}}</td>
                            <td class="align-middle">{{$plane->time_at}} минут</td>
                            <td class="align-middle">{{$plane->description}}</td>
                            <td class="align-middle text-right">
                                <div class="d-flex justify-content-end">
                                    <a href="{{route('admin.road.plane.edit', ['id' => $plane->id])}}" role="button" class="btn btn-sm btn-success"><span class="oi oi-pencil"></span></a>
                                    <form action="{{route('admin.road.plane.delete', ['id' => $plane->id])}}" method="post">
                                        @csrf
                                        @method('delete')
                                        <button type="sumbit" class="btn btn-danger btn-sm ml-2" onclick="return confirm('Удалить рейс {{$plane->title}}?');">
                                            <span class="oi oi-trash"></span>
                                        </button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="9">Рейсов нет</td>
                    </tr>
                @endif
            </tbody>
        </table>
        <a href="{{ route('admin.road.plane.add') }}" class="btn btn-sm btn-success ml-2">Создать рейс</a>
        <a href="{{ route('admin.road.plane.status.index') }}" class="btn btn-sm btn-info">Статусы</a>
        <a href="{{ route('admin.road.plane.company.index') }}" class="btn btn-sm btn-info">Компании</a>
    </div>
@endsection
