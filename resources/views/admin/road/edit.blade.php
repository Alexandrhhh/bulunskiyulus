@extends('layouts.admin')

@section('title')
    Добавление новости - АдминПанель - Булунский Портал
@endsection

@section('content')
    <div class="container my-2">
        <div class="col-12">
            <form action="{{route('admin.road.update', ['id' => $road->id])}}" method="post" enctype="multipart/form-data">
                @csrf
                @method('put')
                <div class="form-group">
                    <label>Дорога</label>
                    <input type="text" name="title" class="form-control" placeholder="Введите название" required value="{{ $road->title }}">
                </div>
                <div class="form-group">
                    <label>Цвет</label>
                    <select name="color" class="form-control dropdown">
                        <option value="0" @if($road->color == 0) selected @endif>Зеленый</option>
                        <option value="1" @if($road->color == 1) selected @endif>Красный</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Открытие автозимника</label>
                    <input type="text" name="open_date" class="datepicker-here form-control" data-date-format="yyyy-mm-dd" value="@if($road->open_date){{ $road->open_date->format('Y-m-d') }}@endif"/>
                </div>
                <div class="form-group">
                    <label>Закрытие автозимника</label>
                    <input type="text" name="close_date" class="datepicker-here form-control" data-date-format="yyyy-mm-dd" value="@if($road->close_date){{ $road->close_date->format('Y-m-d') }}@endif"/>
                </div>
                <div class="form-group">
                    <label>Максимальный тоннаж</label>
                    <input type="text" name="max_tonns" class="form-control" placeholder="30 тонн" value={{ $road->max_tonns }}>
                </div>
                <div class="form-group">
                    <label>Дорожная техника</label>
                    <input type="text" name="technics" class="form-control" placeholder="На дороге" value="{{ $road->technics }}">
                </div>
                <div class="form-group">
                    <label>Следующая чистка</label>
                    <input type="text" name="next_clean" class="datepicker-here form-control" data-date-format="yyyy-mm-dd" value="@if($road->next_clean){{ $road->next_clean->format('Y-m-d') }}@endif"/>
                </div>
                <div class="form-group">
                    <label>Особености</label>
                    <textarea name="about" class="form-control">{{ $road->about }}</textarea>
                </div>
                <div class="form-group">
                    <label>Треки</label>
                    <input type="text" name="tracks" class="form-control" placeholder="-" value="{{ $road->tracks }}">
                </div>
                <button type="submit" class="btn btn-primary" name="create">Обновить</button>
            </form>
        </div>
    </div>
@endsection
