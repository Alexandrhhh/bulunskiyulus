@extends('layouts.admin')

@section('title')
    Дороги - АдминПанель - Булунский Портал
@endsection

@section('content')
    <div class="table-responsive">
        <table class="table table-sm">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Название</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($roads as $key => $road)
                    <tr>
                        <td class="align-middle">{{ $road->id }}</td>
                        <td class="align-middle">{{ $road->title }}</td>
                        <td class="align-middle">
                            <div class="d-flex justify-content-end">
                                <a href="{{route('admin.road.edit', ['id' => $road->id])}}" role="button" class="btn btn-sm btn-success"><span class="oi oi-pencil"></span></a>
                                <form action="{{route('admin.road.delete', ['id' => $road->id])}}" method="post">
                                    @csrf
                                    @method('delete')
                                    <button type="sumbit" class="btn btn-danger btn-sm ml-2" onclick="return confirm('Удалить новости {{$road->title}}?');">
                                        <span class="oi oi-trash"></span>
                                    </button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <a href="{{ route('admin.road.add') }}" class="btn btn-sm btn-success ml-2">Создать новую</a>
    </div>
@endsection
