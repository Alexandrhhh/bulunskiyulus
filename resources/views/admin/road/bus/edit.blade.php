@extends('layouts.admin')

@section('title')
    Добавление новости - АдминПанель - Булунский Портал
@endsection

@section('content')
    <div class="container my-2">
        <div class="col-12">
            <form action="{{route('admin.road.buses.update', ['id' => $bus->id])}}" method="post">
                @csrf
                @method('put')
                <div class="form-group">
                    <label>Название автобуса</label>
                    <input type="text" name="title" class="form-control" placeholder="Введите название" required value="{{ $bus->title }}">
                </div>
                <div class="form-group">
                    <label>Остановки</label>
                    @foreach ($bus->stops as $key => $stop)
                        <div class="row align-items-center mb-2">
                            <div class="col-4">
                                <select class="form-control dropdown" name="stop[title][]" data-placeholder="Название">
                                    <option value=""></option>
                                    @foreach ($stopnames as $key => $stopname)
                                        <option value="{{ $stopname->id }}" @if($stop->stop_name_id == $stopname->id) selected @endif>{{ $stopname->title }} {{ $stopname->description }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-4">
                                <select class="form-control dropdown" name="stop[days][]" data-placeholder="День недели">
                                    <option value=""></option>
                                    @foreach ($stopdays as $key => $stopday)
                                        <option value="{{ $stopday->id }}" @if($stop->stop_day_id == $stopday->id) selected @endif>{{ $stopday->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-2">
                                <input type="text" class="form-control only-time" name="stop[time][]" placeholder="Время" value="{{ $stop->time->format('H:i') }}"/>
                            </div>
                            <div class="col-2">
                                <div class="form-check">
                                    <input hidden name="stop[weekend][]" value="{{ $stop->weekend }}">
                                    <input class="form-check-input" type="checkbox" @if($stop->weekend == 1) checked @endif>
                                    <label class="form-check-label">
                                        Выходной
                                    </label>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <div class="row align-items-center mb-2">
                        <div class="col-4">
                            <select class="form-control dropdown" name="stop[title][]" data-placeholder="Название">
                                <option value=""></option>
                                @foreach ($stopnames as $key => $stopname)
                                    <option value="{{ $stopname->id }}">{{ $stopname->title }} {{ $stopname->description }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-4">
                            <select class="form-control dropdown" name="stop[days][]" data-placeholder="День недели">
                                <option value=""></option>
                                @foreach ($stopdays as $key => $stopday)
                                    <option value="{{ $stopday->id }}">{{ $stopday->title }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-2">
                            <input type="text" class="form-control only-time" name="stop[time][]" placeholder="Время"/>
                        </div>
                        <div class="col-2">
                            <div class="form-check">
                                <input hidden name="stop[weekend][]" value="0">
                                <input class="form-check-input" type="checkbox">
                                <label class="form-check-label">
                                    Выходной
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row align-items-center d-none mb-2">
                        <div class="col-4">
                            <select class="form-control dropdown" name="stop[title][]" data-placeholder="Название">
                                <option value=""></option>
                                @foreach ($stopnames as $key => $stopname)
                                    <option value="{{ $stopname->id }}">{{ $stopname->title }} {{ $stopname->description }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-4">
                            <select class="form-control dropdown" name="stop[days][]" data-placeholder="День недели">
                                <option value=""></option>
                                @foreach ($stopdays as $key => $stopday)
                                    <option value="{{ $stopday->id }}">{{ $stopday->title }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-2">
                            <input type="text" class="form-control only-time" name="stop[time][]" placeholder="Время"/>
                        </div>
                        <div class="col-2">
                            <div class="form-check">
                                <input hidden name="stop[weekend][]" value="0">
                                <input class="form-check-input" type="checkbox">
                                <label class="form-check-label">
                                    Выходной
                                </label>
                            </div>
                        </div>
                    </div>
                    <a href="#copy" class="btn btn-success btn-sm">
                        <span class="oi oi-plus"></span>
                    </a>
                </div>
                <button type="submit" class="btn btn-primary" name="create">Обновить</button>
            </form>
        </div>
    </div>
@endsection
