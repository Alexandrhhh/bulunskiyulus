@extends('layouts.admin')

@section('title')
    Автобусы - АдминПанель - Булунский Портал
@endsection

@section('content')
    <div class="table-responsive">
        <table class="table table-sm">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Название</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($buses as $key => $bus)
                    <tr>
                        <td class="align-middle">{{ $bus->id }}</td>
                        <td class="align-middle">{{ $bus->title }}</td>
                        <td class="align-middle">
                            <div class="d-flex justify-content-end">
                                <a href="{{route('admin.road.buses.edit', ['id' => $bus->id])}}" role="button" class="btn btn-sm btn-success"><span class="oi oi-pencil"></span></a>
                            </div>
                        </td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="3" class="p-2">
                        <form action="{{route('admin.road.buses.create')}}" method="post">
                            @csrf
                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-1 col-form-label">Автобус</label>
                                <div class="col-sm-2">
                                    <input type="text" name="title" class="form-control">
                                </div>
                                <div class="col-sm-2">
                                    <button type="submit" name="button" class="btn btn-primary">Создать</button>
                                </div>
                            </div>
                        </form>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" class="p-2">
                        <form action="{{route('admin.road.buses.stops.names.create')}}" method="post">
                            @csrf
                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-1 col-form-label">Название остановки</label>
                                <div class="col-sm-2">
                                    <input type="text" name="title" class="form-control" placeholder="Название">
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="description" class="form-control" placeholder="Подробнее">
                                </div>
                                <div class="col-sm-2">
                                    <button type="submit" name="button" class="btn btn-primary">Создать</button>
                                </div>
                            </div>
                        </form>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" class="p-2">
                        <form action="{{route('admin.road.buses.stops.days.create')}}" method="post">
                            @csrf
                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-1 col-form-label">День</label>
                                <div class="col-sm-2">
                                    <input type="text" name="title" class="form-control">
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="eng" class="form-control">
                                </div>
                                <div class="col-sm-2">
                                    <button type="submit" name="button" class="btn btn-primary">Создать</button>
                                </div>
                            </div>
                        </form>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
@endsection
