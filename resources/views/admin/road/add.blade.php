@extends('layouts.admin')

@section('title')
    Добавление новости - АдминПанель - Булунский Портал
@endsection

@section('content')
    <div class="container my-2">
        <div class="col-12">
            <form action="{{route('admin.road.create')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label>Дорога</label>
                    <input type="text" name="title" class="form-control" placeholder="Введите название" required>
                </div>
                <div class="form-group">
                    <label>Цвет</label>
                    <select name="color" class="form-control dropdown">
                        <option value="0">Зеленый</option>
                        <option value="1">Красный</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Открытие автозимника</label>
                    <input type="text" name="open_date" class="datepicker-here form-control" data-date-format="yyyy-mm-dd"/>
                </div>
                <div class="form-group">
                    <label>Закрытие автозимника</label>
                    <input type="text" name="close_date" class="datepicker-here form-control" data-date-format="yyyy-mm-dd"/>
                </div>
                <div class="form-group">
                    <label>Максимальный тоннаж</label>
                    <input type="text" name="max_tonns" class="form-control" placeholder="30 тонн">
                </div>
                <div class="form-group">
                    <label>Дорожная техника</label>
                    <input type="text" name="technics" class="form-control" placeholder="На дороге">
                </div>
                <div class="form-group">
                    <label>Следующая чистка</label>
                    <input type="text" name="next_clean" class="datepicker-here form-control" data-date-format="yyyy-mm-dd"/>
                </div>
                <div class="form-group">
                    <label>Особености</label>
                    <textarea name="about" class="form-control"></textarea>
                </div>
                <div class="form-group">
                    <label>Треки</label>
                    <input type="text" name="tracks" class="form-control" placeholder="-">
                </div>
                <button type="submit" class="btn btn-primary" name="create">Добавить</button>
            </form>
        </div>
    </div>
@endsection
