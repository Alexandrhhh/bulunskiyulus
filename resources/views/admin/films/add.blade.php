@extends('layouts.admin')

@section('title')
    Добавление фильмы - АдминПанель - Булунский Портал
@endsection

@section('content')
    <div class="container my-2">
        <div class="col-12">
            <form action="{{route('admin.film.create')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label>Обложка</label>
                    <input type="file" name="image" class="form-control-file" required>
                </div>
                <div class="form-group">
                    <label>Название</label>
                    <input type="text" name="title" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Английское название</label>
                    <input type="text" name="title_eng" class="form-control">
                    <small>Если есть</small>
                </div>
                <div class="form-group">
                    <label>Продолжительность</label>
                    <input type="number" name="duration" placeholder="В минутах" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Полное описание</label>
                    <textarea name="description" class="form-control" required></textarea>
                </div>
                <div class="form-group">
                    <label>Жанры</label>
                    <select name="genres[]" class="form-control" id="tags" multiple="multiple">
                        @foreach ($genres as $key => $genre)
                            <option value="{{$genre->title}}">{{$genre->title}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Сеансы</label>
                    <input type="text" name="sessions[]" class="form-control datepicker-here" data-timepicker="true">
                </div>
                <div class="form-group d-none">
                    <input type="text" name="sessions[]" class="form-control datepicker-here" data-timepicker="true">
                </div>
                <a href="#copy" class="btn btn-success btn-sm mb-2">
                    <span class="oi oi-plus"></span>
                </a>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Добавить</button>
                </div>
            </form>
        </div>
    </div>
@endsection
