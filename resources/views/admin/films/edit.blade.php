@extends('layouts.admin')

@section('title')
    Добавление фильмы - АдминПанель - Булунский Портал
@endsection

@section('content')
    <div class="container my-2">
        <div class="col-12">
            <form action="{{route('admin.film.update', ['id' => $film->id])}}" method="post" enctype="multipart/form-data">
                @csrf
                @method('put')
                <div class="form-group">
                    <img src="{{ Storage::url($film->image) }}" style="max-width: 100px" alt="">
                    <label>Обложка</label>
                    <input type="file" name="image" class="form-control-file">
                </div>
                <div class="form-group">
                    <label>Название</label>
                    <input type="text" name="title" class="form-control" value="{{ $film->title }}" required>
                </div>
                <div class="form-group">
                    <label>Английское название</label>
                    <input type="text" name="title_eng" value="{{ $film->title_eng }}" class="form-control">
                    <small>Если есть</small>
                </div>
                <div class="form-group">
                    <label>Продолжительность</label>
                    <input type="number" name="duration" placeholder="В минутах" value="{{ $film->duration }}" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Полное описание</label>
                    <textarea name="description" class="form-control" required>{{ $film->description }}</textarea>
                </div>
                <div class="form-group">
                    <label>Жанры</label>
                    <select name="genres[]" class="form-control" id="tags" multiple="multiple">
                        @foreach ($genres as $key => $genre)
                            <option value="{{$genre->title}}" @if($film->genres) @foreach ($film->genres as $key => $value) @if($genre->id == $value->id) selected @endif @endforeach @endif>{{$genre->title}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Сеансы</label>
                </div>
                @if($film->sessions)
                    @foreach ($film->sessions()->orderBy('display_at')->get() as $key => $session)
                        <div class="form-group">
                            <input type="text" name="sessions[]" class="form-control datepicker-here" data-timepicker="true" value="{{ $session->display_at->format('d.m.Y H:i') }}">
                        </div>
                    @endforeach
                @endif
                <div class="form-group d-none">
                    <input type="text" name="sessions[]" class="form-control datepicker-here" data-timepicker="true">
                </div>
                <a href="#copy" class="btn btn-success btn-sm mb-2">
                    <span class="oi oi-plus"></span>
                </a>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Обновить</button>
                </div>
            </form>
        </div>
    </div>
@endsection
