@extends('layouts.admin')

@section('title')
    Все фильмы - АдминПанель - Булунский Портал
@endsection

@section('content')
    <div class="table-responsive">
        <table class="table table-sm">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Обложка</th>
                    <th scope="col">Название</th>
                    <th scope="col">Жанры</th>
                    <th scope="col">Описание</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                @if($films->count() != 0)
                    @foreach ($films as $key => $film)
                        <tr>
                            <td scope="col" class="align-middle">{{ $film->id }}</td>
                            <td class="align-middle"><img src="{{ Storage::url($film->image) }}" style="max-width: 100px;" alt=""></td>
                            <td class="align-middle">{{ $film->title }}</td>
                            <td class="align-middle">{{ $film->genres->implode('title', ', ') }}</td>
                            <td class="align-middle">{{ Str::limit($film->description, 100, '...') }}</td>
                            <td class="align-middle text-right">
                                <div class="d-flex justify-content-end">
                                    <a href="{{route('admin.film.edit', ['id' => $film->id])}}" role="button" class="btn btn-sm btn-success"><span class="oi oi-pencil"></span></a>
                                    <form action="{{route('admin.film.delete', ['id' => $film->id])}}" method="post">
                                        @csrf
                                        @method('delete')
                                        <button type="sumbit" class="btn btn-danger btn-sm ml-2" onclick="return confirm('Удалить фильм {{$film->title}}?');">
                                            <span class="oi oi-trash"></span>
                                        </button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="6">Фильмов нет</td>
                    </tr>
                @endif
            </tbody>
        </table>
    </div>
    <a href="{{ route('admin.film.add') }}" class="btn btn-sm btn-success ml-2">Добавить фильм</a>
@endsection
