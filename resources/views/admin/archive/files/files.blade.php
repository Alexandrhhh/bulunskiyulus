@extends('layouts.admin')

@section('content')
    <div class="table-responsive">
        <table class="table table-sm">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">Тип</th>
                    <th scope="col">Изображение</th>
                    <th scope="col">Название</th>
                    <th scope="col">Описание</th>
					<th scope="col">Дата съемки</th>
					<th>Добавил</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                @if($files->count() != 0)
                    @foreach ($files as $key => $file)
                        <tr>
                            <td scope="col" class="align-middle">@if($file->type) Видео@else Фото @endif</td>
							<td class="align-middle"><img src="@if($file->type){{ Storage::url($file->preview) }}@else{{ $file->file }}@endif" style="max-width: 200px"></td>
							<td class="align-middle">{{ $file->title }}</td>
							<td class="align-middle">{{ $file->description }}</td>
							<td class="align-middle">@if($file->create_day){{ $file->create_day->format('d.m.Y') }}@endif</td>
							<td class="align-middle">@if($file->user) {{ $file->user->first_name }} {{ $file->user->last_name }} @endif</td>
                            <td class="align-middle text-right">
                                <div class="d-flex justify-content-end">
									<a href="{{ route('archive.section', ['id' => $file->section->id]) }}" target="_blank" role="button" class="btn btn-sm btn-primary"><span class="oi oi-link-intact"></span></a>
									<a href="{{route('admin.archive.sections.file.show', ['id' => $file->id])}}" role="button" class="btn btn-sm btn-success ml-2"><span class="oi oi-pencil"></span></a>
                                    <form action="{{route('admin.archive.sections.file.delete', ['id' => $file->id])}}" method="post">
                                        @csrf
                                        @method('delete')
                                        <button type="sumbit" class="btn btn-danger btn-sm ml-2" onclick="return confirm('Удалить файл {{$file->title}}?');">
                                            <span class="oi oi-trash"></span>
                                        </button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="6">Газет нет</td>
                    </tr>
                @endif
            </tbody>
        </table>
        <a class="btn btn-sm btn-success ml-2" href="{{ route('admin.archive.mayak.add') }}">Добавить выпуск</a>
        <a class="btn btn-sm btn-info" href="{{ route('admin.archive.sections.index') }}">Разделы</a>
    </div>
@endsection
