@extends('layouts.admin')

@section('title')
    Категории новостей - АдминПанель - Булунский портал
@endsection

@section('content')
    <div class="table-responsive">
        <table class="table table-sm">
            <thead class="thead-dark">
                <th>#</th>
                <th>Название</th>
                <th>Материалов в разделе</th>
                <th></th>
            </thead>
            <tbody>
                @foreach ($sections as $key => $section)
                    <tr>
                        <td class="align-middle">{{$section->id}}</td>
                        <td class="align-middle">{{$section->title}}</td>
                        <td class="align-middle">{{$section->files->count()}}</td>
                        <td class="align-middle text-right d-flex justify-content-end">
							<a href="{{ route('admin.archive.sections.show', ['id' => $section->id]) }}" class="btn btn-sm btn-primary"><span class="oi oi-box"></span></a>
                            <a href="{{ route('admin.archive.sections.edit', ['id' => $section->id]) }}" class="btn btn-sm btn-success ml-2"><span class="oi oi-pencil"></span></a>
                            <form action="{{route('admin.archive.sections.delete', ['id' => $section->id])}}" method="post">
                                @csrf
                                @method('delete')
                                <button type="sumbit" class="btn btn-danger btn-sm ml-2" onclick="return confirm('Удалить раздел {{$section->title}}? Это удалит и {{$section->files->count()}} файлов.');">
                                    <span class="oi oi-trash"></span>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="4" class="p-2">
                        <form action="{{route('admin.archive.sections.create')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-1 col-form-label">Название</label>
                                <div class="col-sm-2">
                                    <input type="text" name="title" class="form-control">
                                </div>
                                <label for="inputPassword" class="col-sm-1 col-form-label">Обложка</label>
                                <div class="col-sm-3">
                                    <input type="file" name="image" class="form-control">
                                </div>
                                <div class="col-sm-2">
                                    <button type="submit" name="button" class="btn btn-primary">Создать</button>
                                </div>
                            </div>
                        </form>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
@endsection
