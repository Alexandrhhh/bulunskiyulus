@extends('layouts.admin')

@section('content')
<div class="container my-2">
        <div class="col-12">
            <form action="{{route('admin.archive.sections.file.update', ['id' => $file->id])}}" method="post" enctype="multipart/form-data">
                @csrf
                @method('put')
                <div class="form-group">
                    <label>Заголовок</label>
                    <input type="text" name="title" class="form-control" placeholder="Введите заголовок" value="{{ $file->title }}" required>
				</div>
				<div class="form-group">
                    <label>Заголовок</label>
                    <textarea type="text" name="description" class="form-control" placeholder="Введите описание">{{ $file->description }}</textarea>
				</div>
				<div class="form-group">
                    <label>Дата съемки</label>
                    <input type="text" name="create_day" class="datepicker-here form-control" placeholder="Дата съемки" value="@if($file->create_day){{ $file->create_day->format('d.m.Y') }}@endif">
                </div>
                <button type="submit" class="btn btn-primary" name="create">Обновить</button>
            </form>
        </div>
    </div>
@endsection
