@extends('layouts.admin')

@section('title')
    Редактирование раздела - АдминПанель - Булунский Портал
@endsection

@section('content')
    <div class="container my-2">
        <div class="col-12">
            <form action="{{route('admin.archive.sections.update', ['id' => $section->id])}}" method="post" enctype="multipart/form-data">
                @csrf
                @method('put')
                <div class="form-group">
                    <label>Название</label>
                    <input type="text" name="title" class="form-control" placeholder="Введите заголовок" value="{{ $section->title }}" required>
                </div>
                <div class="form-group">
                    <label>Обложка</label>
                    @if($section->image)<img src="{{ Storage::url($section->image) }}" alt="" style="max-width: 200px;">@endif
                    <input type="file" name="image" class="form-control-file">
                </div>
                </div>

                <button type="submit" class="btn btn-primary" name="create">Обновить</button>
            </form>
        </div>
    </div>
@endsection
