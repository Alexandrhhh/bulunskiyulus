@extends('layouts.admin')

@section('title')
    Добавление выпуска - АдминПанель - Булунский Портал
@endsection

@section('content')
    <div class="container my-2">
        <div class="col-12">
            <form action="{{route('admin.archive.mayak.update', ['id' => $mayak->id])}}" method="post" enctype="multipart/form-data">
                @csrf
                @method('put')
                <div class="form-group">
                    <label>Заголовок</label>
                    <input type="text" name="title" class="form-control" placeholder="Введите заголовок" value="{{ $mayak->title }}" required>
                </div>
                <div class="form-group">
                    <label>Дата выпуска</label>
                    <input type="text" name="publish_at" class="datepicker-here form-control" placeholder="Дата выпуска" value="{{ $mayak->publish_at->format('d.m.Y') }}" required>
                </div>
                <div class="form-group">
                    <label>Обложка</label>
                    <img src="{{ Storage::url($mayak->image) }}" alt="" style="max-width: 200px;">
                    <input type="file" name="image" class="form-control-file">
                </div>
                <div class="form-group">
                    <label>Выпуск</label>
                    @if($mayak->file) <small>Выпуск прикреплён, чтобы заменить прикрепите другой</small> @endif
                    <input type="file" name="file" class="form-control-file">
                </div>
                <div class="form-group">
                    <label>Тэги</label>
                    <select name="tags[]" class="form-control" id="tags" multiple="multiple">
                        @foreach ($tags as $key => $tag)
                            <option value="{{$tag->title}}" @foreach($mayak->tags as $newtag) @if($newtag->id == $tag->id) selected @endif @endforeach>{{$tag->title}}</option>
                        @endforeach
                    </select>
                </div>

                <button type="submit" class="btn btn-primary" name="create">Обновить</button>
            </form>
        </div>
    </div>
@endsection
