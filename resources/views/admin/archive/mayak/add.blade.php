@extends('layouts.admin')

@section('title')
    Добавление выпуска - АдминПанель - Булунский Портал
@endsection

@section('content')
    <div class="container my-2">
        <div class="col-12">
            <form action="{{route('admin.archive.mayak.create')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label>Заголовок</label>
                    <input type="text" name="title" class="form-control" placeholder="Введите заголовок" required>
                </div>
                <div class="form-group">
                    <label>Дата выпуска</label>
                    <input type="text" name="publish_at" class="datepicker-here form-control" placeholder="Дата выпуска" required>
                </div>
                <div class="form-group">
                    <label>Обложка</label>
                    <input type="file" name="image" class="form-control-file" required>
                </div>
                <div class="form-group">
                    <label>Выпуск</label>
                    <input type="file" name="file" class="form-control-file" required>
                </div>
                <div class="form-group">
                    <label>Тэги</label>
                    <select name="tags[]" class="form-control" id="tags" multiple="multiple">
                        @foreach ($tags as $key => $tag)
                            <option value="{{$tag->title}}">{{$tag->title}}</option>
                        @endforeach
                    </select>
                </div>

                <button type="submit" class="btn btn-primary" name="create">Добавить</button>
            </form>
        </div>
    </div>
@endsection
