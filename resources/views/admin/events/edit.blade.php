@extends('layouts.admin')

@section('title')
    Добавление объявление - АдминПанель - Булунский Портал
@endsection

@section('content')
    <div class="container my-2">
        <div class="col-12">
            <form action="{{route('admin.events.update', ['id' => $event->id])}}" method="post" enctype="multipart/form-data">
                @csrf
                @method('put')
                <div class="form-group">
                    <label>Заголовок</label>
                    <input type="text" name="title" class="form-control" placeholder="Введите заголовок" value="{{ $event->title }}" required>
                </div>
                <div class="form-group">
                    <label>Категория</label>
                    <select name="category_id" class="form-control dropdown" required>
                        @foreach ($categories as $key => $category)
                            <option value="{{$category->id}}" @if($event->category_id == $category->id) selected @endif>{{$category->title}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Раздел</label>
                    <select name="section_id" class="form-control dropdown" required>
                        @foreach ($sections as $key => $section)
                            <option value="{{$section->id}}" @if($event->section_id == $section->id) selected @endif>{{$section->title}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Размер</label>
                    <select name="style" class="form-control dropdown" required>
                        <option value="0" @if($event->style == 0) selected @endif>Маленький</option>
                        <option value="1" @if($event->style == 1) selected @endif>Большой</option>
                    </select>
                </div>
                <label>Изображениe</label>
                <div class="custom-file mb-3 form-group">
                    @if($event->image) <img src="{{ Storage::url($event->image) }}" alt="" style="max-width: 200px;"> @endif
                    <input type="file" name="image" class="custom-file-input" id="customFileLangHTML" required>
                    <label class="custom-file-label" for="customFileLangHTML" data-browse="Выбрать">Изображения</label>
                </div>
                <div class="form-group">
                    <label>Короткое описание</label>
                    <textarea name="short" class="form-control" required>{{ $event->short }}</textarea>
                </div>
                <div class="form-group">
                    <label>Полное описание</label>
                    <textarea name="long" id="long" required>{!! html_entity_decode($event->long) !!}</textarea>
                </div>
                <div class="form-group">
                    <label>Дата и время проведения</label>
                    <input type="text" name="date_event" class="datepicker-here form-control" data-date-format="yyyy-mm-dd" data-time-format="hh:ii:00" data-timepicker="true" @if($event->date_event) value="{{ $event->date_event->format('Y-m-d H:i:s') }}" @endif required/>
                </div>
                <div class="form-group">
                    <label>Тэги</label>
                    <select name="tags[]" class="form-control" id="tags" multiple="multiple">
                        @foreach($tags as $key => $tag)
                            <option value="{{$tag->title}}" @foreach($event->tags as $evtag) @if($evtag->id == $tag->id) selected @endif @endforeach>{{$tag->title}}</option>
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">Добавить</button>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/ckeditor/ckeditor.js') }}"></script>
    <script>
        CKEDITOR.replace( 'long', {
            filebrowserUploadUrl: "{{route('upload.image', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
    </script>
@endsection
