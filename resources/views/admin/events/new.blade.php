@extends('layouts.admin')

@section('title')
    Добавление объявление - АдминПанель - Булунский Портал
@endsection

@section('content')
    <div class="container my-2">
        <div class="col-12">
            <form action="{{route('admin.events.create')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label>Заголовок</label>
                    <input type="text" name="title" class="form-control" placeholder="Введите заголовок" required>
                </div>
                <div class="form-group">
                    <label>Категория</label>
                    <select name="category_id" class="form-control dropdown" required>
                        @foreach ($categories as $key => $category)
                            <option value="{{$category->id}}">{{$category->title}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Раздел</label>
                    <select name="section_id" class="form-control dropdown" required>
                        @foreach ($sections as $key => $section)
                            <option value="{{$section->id}}">{{$section->title}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Размер</label>
                    <select name="style" class="form-control dropdown" required>
                        <option value="0">Маленький</option>
                        <option value="1">Большой</option>
                    </select>
                </div>
                <label>Изображениe</label>
                <div class="custom-file mb-3 form-group">
                    <input type="file" name="image" class="custom-file-input" id="customFileLangHTML" required>
                    <label class="custom-file-label" for="customFileLangHTML" data-browse="Выбрать">Изображения</label>
                </div>
                <div class="form-group">
                    <label>Короткое описание</label>
                    <textarea name="short" class="form-control" required></textarea>
                </div>
                <div class="form-group">
                    <label>Полное описание</label>
                    <textarea name="long" id="long" required></textarea>
                </div>
                <div class="form-group">
                    <label>Дата и время проведения</label>
                    <input type="text" name="date_event" class="datepicker-here form-control" data-date-format="yyyy-mm-dd" data-time-format="hh:ii:00" data-timepicker="true" required/>
                </div>
                <div class="form-group">
                    <label>Тэги</label>
                    <select name="tags[]" class="form-control" id="tags" multiple="multiple">
                        @foreach ($tags as $key => $tag)
                            <option value="{{$tag->title}}">{{$tag->title}}</option>
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">Добавить</button>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/ckeditor/ckeditor.js') }}"></script>
    <script>
        CKEDITOR.replace( 'long', {
            filebrowserUploadUrl: "{{route('upload.image', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
    </script>
@endsection
