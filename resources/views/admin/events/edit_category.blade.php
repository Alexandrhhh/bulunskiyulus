@extends('layouts.admin')

@section('title')
    Добавление новости - АдминПанель - Булунский Портал
@endsection

@section('content')
    <div class="container my-2">
        <div class="col-12">
            <form action="{{route('admin.events.categories.update', ['id' => $category->id])}}" method="post">
                @csrf
                @method('put')
                <div class="form-group">
                    <label>Заголовок</label>
                    <input type="text" name="title" class="form-control" placeholder="Введите заголовок" value="{{$category->title}}">
                </div>
                <div class="form-group">
                    <label>Цвет</label>
                    <select type="text" name="color_id" class="form-control dropdown" placeholder="Выберите цвет">
                        @foreach ($colors as $key => $color)
                            <option value="{{ $color->id }}" @if($category->color_id == $color->id) selected @endif>{{ $color->title }}</option>
                        @endforeach
                    </select>
                </div>

                <button type="submit" class="btn btn-primary">Обновить</button>
            </form>
        </div>
    </div>
@endsection
