@extends('layouts.admin')

@section('title')
    Все мероприятия - АдминПанель - Булунский Портал
@endsection

@section('content')
    <div class="table-responsive">
        <table class="table table-sm">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Название</th>
                    <th scope="col" style="width: 25%;">Краткое содержание</th>
                    <th scope="col">Категория</th>
                    <th scope="col">Дата мероприятия</th>
                    <th scope="col">Дата публикации</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                @if($events->count() != 0)
                    @foreach ($events as $key => $event)
                        <tr>
                            <td scope="col" class="align-middle">{{$event->id}}</td>
                            <td class="align-middle">{{$event->title}}</td>
                            <td class="align-middle">{!! html_entity_decode($event->short) !!}</td>
                            <td class="align-middle">@if($event->category){{$event->category->title}}@else Категории нет@endif</td>
                            <td class="align-middle">{{$event->date_event->format('d.m.Y в H:i')}}</td>
                            <td class="align-middle">{{$event->created_at->format('d.m.Y в H:i')}}</td>
                            <td class="align-middle text-right">
                                <div class="d-flex justify-content-end">
                                    <a href="{{route('admin.events.edit', ['id' => $event->id])}}" role="button" class="btn btn-sm btn-success"><span class="oi oi-pencil"></span></a>
                                    <form action="{{route('admin.events.delete', ['id' => $event->id])}}" method="post">
                                        @csrf
                                        @method('delete')
                                        <button type="sumbit" class="btn btn-danger btn-sm ml-2" onclick="return confirm('Удалить мероприятие {{$event->title}}?');">
                                            <span class="oi oi-trash"></span>
                                        </button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="7">Мероприятий нет</td>
                    </tr>
                @endif
            </tbody>
        </table>
    </div>
@endsection
