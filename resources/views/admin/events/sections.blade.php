@extends('layouts.admin')

@section('title')
    Разделы мероприятий - АдминПанель - Булунский портал
@endsection

@section('content')
    <div class="table-responsive">
        <table class="table table-sm">
            <thead class="thead-dark">
                <th>#</th>
                <th>Название</th>
                <th>Мероприятий в разделе</th>
                <th></th>
            </thead>
            <tbody>
                @foreach ($sections as $key => $section)
                    <tr>
                        <td class="align-middle">{{$section->id}}</td>
                        <td class="align-middle">{{$section->title}}</td>
                        <td class="align-middle">{{$section->events->count()}}</td>
                        <td class="align-middle text-right">
                            <form action="{{route('admin.events.sections.delete', ['id' => $section->id])}}" method="post">
                                @csrf
                                @method('delete')
                                <button type="sumbit" class="btn btn-danger btn-sm" onclick="return confirm('Удалить раздел {{$section->title}}? Это удалит и {{$section->events->count()}} мероприятий.');">
                                    <span class="oi oi-trash"></span>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="4" class="p-2">
                        <form action="{{route('admin.events.sections.create')}}" method="post">
                            @csrf
                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-1 col-form-label">Название</label>
                                <div class="col-sm-2">
                                    <input type="text" name="title" class="form-control">
                                </div>
                                <div class="col-sm-2">
                                    <button type="submit" name="button" class="btn btn-primary">Создать</button>
                                </div>
                            </div>
                        </form>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
@endsection
