@extends('layouts.admin')

@section('title')
    Добавление новости - АдминПанель - Булунский Портал
@endsection

@section('content')
    <div class="container my-2">
        <div class="col-12">
            <form action="{{route('news.update', ['id' => $news->id])}}" method="post" enctype="multipart/form-data">
                @csrf
                @method('put')
                <div class="form-group">
                    <label>Заголовок</label>
                    <input type="text" name="title" class="form-control" placeholder="Введите заголовок" value="{{$news->title}}">
                </div>
                <div class="form-group">
                    <label>Категория</label>
                    <select name="category_id" class="form-control" id="dropdown">
                        @foreach ($categories as $key => $category)
                            <option value="{{$category->id}}" @if($news->category_id == $category->id) selected @endif>{{$category->title}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Короткое описание</label>
                    <textarea name="short" class="form-control">{!! html_entity_decode($news->short) !!}</textarea>
                </div>
                <div class="form-group">
                    <label>Полное описание</label>
                    <textarea name="content" id="content">{!! html_entity_decode($news->content) !!}</textarea>
                </div>
                <div class="form-group">
                    <label>Изображение</label>
                    @if($news->image_url)
                        <img src="{{Storage::url($news->image_url)}}" style="max-width:300px" alt="">
                    @endif
                    <input type="file" name="image" class="form-control-file">
                </div>
                <div class="form-group">
                    <label>Тэги</label>
                    <select name="tags[]" class="form-control" id="tags" multiple="multiple">
                        @foreach ($tags as $key => $tag)
                            <option value="{{$tag->title}}" @foreach($news->tags as $newtag) @if($newtag->id == $tag->id) selected @endif @endforeach>{{$tag->title}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Опрос<span class="badge badge-pill badge-secondary mx-2">Необязательно</span></label>
                    <select name="poll_id" class="form-control dropdown">
                        <option value=""></option>
                        @foreach ($polls as $key => $poll)
                            <option value="{{$poll->id}}" @if($news->poll_id == $poll->id) selected @endif>{{$poll->title}}</option>
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">Обновить</button>
            </form>
            <script src="{{ asset('js/ckeditor/ckeditor.js') }}"></script>
            <script>
                CKEDITOR.replace( 'content', {
                    filebrowserUploadUrl: "{{route('upload.image', ['_token' => csrf_token() ])}}",
                    filebrowserUploadMethod: 'form'
                });
            </script>
        </div>
    </div>
@endsection
