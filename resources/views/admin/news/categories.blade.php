@extends('layouts.admin')

@section('title')
    Категории новостей - АдминПанель - Булунский портал
@endsection

@section('content')
    <div class="table-responsive">
        <table class="table table-sm">
            <thead class="thead-dark">
                <th>#</th>
                <th>Название</th>
                <th>Новостей в категории</th>
                <th></th>
            </thead>
            <tbody>
                @foreach ($categories as $key => $category)
                    <tr>
                        <td class="align-middle">{{$category->id}}</td>
                        <td class="align-middle"><span class="category {{ $category->color->eng }}">{{$category->title}}</span></td>
                        <td class="align-middle">{{$category->newses->count()}}</td>
                        <td class="align-middle text-right d-flex justify-content-end">
                            <a href="{{ route('admin.news.category.edit', ['id' => $category->id]) }}" class="btn btn-sm btn-success"><span class="oi oi-pencil"></span></a>
                            <form action="{{route('admin.news.category.delete', ['id' => $category->id])}}" method="post">
                                @csrf
                                @method('delete')
                                <button type="sumbit" class="btn btn-danger btn-sm ml-2" onclick="return confirm('Удалить категорию {{$category->title}}? Это удалит и {{$category->newses->count()}} новостей.');">
                                    <span class="oi oi-trash"></span>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="4" class="p-2">
                        <form action="{{route('admin.news.category.create')}}" method="post">
                            @csrf
                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-1 col-form-label">Название</label>
                                <div class="col-sm-2">
                                    <input type="text" name="title" class="form-control">
                                </div>
                                <div class="col-sm-2">

                                    <select type="text" name="color_id" class="form-control dropdown" placeholder="Выберите цвет">
                                        @foreach ($colors as $key => $color)
                                            <option value="{{ $color->id }}">{{ $color->title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-2">
                                    <button type="submit" name="button" class="btn btn-primary">Создать</button>
                                </div>
                            </div>
                        </form>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
@endsection
