@extends('layouts.admin')

@section('title')
    Все новости - АдминПанель - Булунский Портал
@endsection

@section('content')
    <div class="table-responsive">
        <table class="table table-sm">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Название</th>
                    <th scope="col" style="width: 25%;">Краткое содержание</th>
                    <th scope="col">Категория</th>
                    <th scope="col">Теги</th>
                    <th scope="col">Дата публикации</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                @if($news->count() != 0)
                    @foreach ($news as $key => $new)
                        <tr>
                            <td scope="col" class="align-middle">{{$new->id}}</td>
                            <td class="align-middle">{{$new->title}}</td>
                            <td class="align-middle">{!! html_entity_decode($new->short) !!}</td>
                            <td class="align-middle">{{$new->category->title}}</td>
                            <td class="align-middle">{{$new->tags->implode('title', ', ')}}</td>
                            <td class="align-middle">{{$new->created_at->format('d.m.Y в H:i')}}</td>
                            <td class="align-middle text-right">
                                <div class="d-flex justify-content-end">
                                    <a href="{{route('admin.news.edit', ['id' => $new->id])}}" role="button" class="btn btn-sm btn-success"><span class="oi oi-pencil"></span></a>
                                    <form action="{{route('admin.news.delete', ['id' => $new->id])}}" method="post">
                                        @csrf
                                        @method('delete')
                                        <button type="sumbit" class="btn btn-danger btn-sm ml-2" onclick="return confirm('Удалить новости {{$new->title}}?');">
                                            <span class="oi oi-trash"></span>
                                        </button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="7">Новостей нет</td>
                    </tr>
                @endif
            </tbody>
        </table>
    </div>
@endsection
