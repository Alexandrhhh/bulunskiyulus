@extends('layouts.admin')

@section('title')
    Теги новостей - АдминПанель - Булунский портал
@endsection

@section('content')
    <div class="table-responsive">
        <table class="table table-sm">
            <thead class="thead-dark">
                <th>#</th>
                <th style="width: 95%">Название</th>
            </thead>
            <tbody>
                @foreach ($tags as $key => $tag)
                    <tr>
                        <td class="align-middle">{{$tag->id}}</td>
                        <td class="align-middle">{{$tag->title}}</td>
                        </tr>
                @endforeach
                <tr>
                    <td colspan="3" class="p-2">
                        <form action="{{route('admin.news.tags.create')}}" method="post">
                            @csrf
                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-1 col-form-label">Название</label>
                                <div class="col-sm-2">
                                    <input type="text" name="title" class="form-control" id="inputPassword">
                                </div>
                                <div class="col-sm-2">
                                    <button type="submit" name="button" class="btn btn-primary">Создать</button>
                                </div>
                            </div>
                        </form>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
@endsection
