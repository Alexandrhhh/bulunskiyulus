@extends('layouts.admin')

@section('title')
    Добавление новости - АдминПанель - Булунский Портал
@endsection

@section('content')
    <div class="container my-2">
        <div class="col-12">
            <form action="{{route('news.create')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label>Заголовок</label>
                    <input type="text" name="title" class="form-control" placeholder="Введите заголовок" required>
                </div>
                <div class="form-group">
                    <label>Раздел</label>
                    <select name="section_id" class="form-control dropdown" required>
                        @foreach ($sections as $key => $section)
                            <option value="{{$section->id}}">{{$section->title}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Категория</label>
                    <select name="category_id" class="form-control dropdown" required>
                        @foreach ($categories as $key => $category)
                            <option value="{{$category->id}}">{{$category->title}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Короткое описание</label>
                    <textarea name="short" class="form-control" required></textarea>
                </div>
                <div class="form-group">
                    <label>Полное описание</label>
                    <textarea name="content" id="content" required></textarea>
                </div>
                <div class="form-group">
                    <label>Изображение</label>
                    <input type="file" name="image" class="form-control-file" required>
                </div>
                <div class="form-group">
                    <label>Тэги</label>
                    <select name="tags[]" class="form-control" id="tags" multiple="multiple">
                        @foreach ($tags as $key => $tag)
                            <option value="{{$tag->title}}">{{$tag->title}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Опрос<span class="badge badge-pill badge-secondary mx-2">Необязательно</span></label>
                    <select name="poll_id" class="form-control dropdown">
                        <option value=""></option>
                        @foreach ($polls as $key => $poll)
                            <option value="{{$poll->id}}">{{$poll->title}}</option>
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-primary" name="create">Добавить</button>
                <button type="submit" class="btn btn-primary" name="preload">Предпросмотр</button>
            </form>
            <script src="{{ asset('js/ckeditor/ckeditor.js') }}"></script>
            <script>
                CKEDITOR.replace( 'content', {
                    filebrowserUploadUrl: "{{route('upload.image', ['_token' => csrf_token() ])}}",
                    filebrowserUploadMethod: 'form'
                });
            </script>
        </div>
    </div>
@endsection
