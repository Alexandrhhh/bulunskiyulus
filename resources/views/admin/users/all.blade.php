@extends('layouts.admin')

@section('title')
    Все пользователи - АдминПанель - Булунский портал
@endsection

@section('content')
    <div class="table-responsive">
        <table class="table table-sm datatable">
            <thead class="thead-dark">
                <tr>
                    <th scope="col"><input type="text" id="search" class="form-control form-control-sm" placeholder="Поиск"></th>
                    <th scope="col" class="align-middle">Имя</th>
                    <th scope="col" class="align-middle">Тариф</th>
                    <th scope="col" class="align-middle">Баланс</th>
                    <th scope="col" class="align-middle">Группа</th>
                    <th scope="col" class="align-middle">#</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($users as $key => $user)
                    <tr>
                        <td>{{ $user->user_name }} | {{ $user->user_pswd }}</td>
                        <td>@if($user->last_name){{ $user->last_name }} {{ $user->first_name }} @else <small class="text-warning">{{ $user->client_name2 }}</small> @endif</td>
                        <td>{{ $user->tarif }}</td>
                        <td>{{ $user->ballance }}</td>
                        <td>{{ $user->email }}</td>
                        <td><small><a href="#" class="text-success">Ред.</a></small></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
