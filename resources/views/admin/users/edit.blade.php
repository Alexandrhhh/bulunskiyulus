@extends('layouts.admin')

@section('title')
    Редактирование пользователя - АдминПанель - Булунский Портал
@endsection

@section('content')
    <div class="container my-2">
        <div class="col-12">
            <form action="{{route('admin.users.update', ['id' => $user->id])}}" method="post" enctype="multipart/form-data">
                @csrf
                @method('put')
                <div class="form-group">
                    <label>Имя</label>
                    <input type="text" class="form-control" value="{{ $user->name }}" disabled>
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input type="email" name="email" class="form-control" value="{{ $user->email }}" required>
                </div>
                <div class="form-group">
                    <label>Группа</label>
                    <select name="group_id" class="form-control" id="dropdown">
                        @foreach ($groups as $key => $group)
                            <option value="{{$group->id}}" @if($user->group_id == $group->id) selected @endif>{{$group->title}}</option>
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">Обновить</button>
            </form>
        </div>
    </div>
@endsection
