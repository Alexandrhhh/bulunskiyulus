@extends('layouts.app')

@section('title')
    Киноафиша
@endsection

@section('content')
    <div class="d-flex align-items-center justify-content-between mb-5">
        <h2>Киноафиша</h2>
    </div>
    <div class="row afisha mb-4">
        @foreach ($films as $key => $film)
            @if($film->actual_sessions()->count())
                <div class="col-12 mb-9">
                    <div class="white">
                        <div class="d-flex top-content">
                            <div class="col-3 image">
                                <div class="d-block w-100 img">
                                    <div class="i" style="background-image: url('{{ Storage::url($film->image) }}')"></div>
                                </div>
                            </div>
                            <div class="about">
                                <div class="title">
                                    <h2>{{ $film->title }}</h2>
                                    @if($film->title_eng)<h3>{{ $film->title_eng }}</h3>@endif
                                </div>
                                <p class="description">
                                    {{ $film->description }}
                                </p>
                                {{-- <a href="#openDescription" class="btn btn-primary btn-sm mt-2 mb-3">Показать полностью</a> --}}
                                <div class="d-flex genre">
                                    <span>@if($film->duration){{ Carbon\CarbonInterval::minutes($film->duration)->cascade()->forHumans() }}@endif</span>
                                    <span>@if($film->genres)Жанр: {{ $film->genres->implode('title', ', ') }}@endif</span>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex middle-content">
                            <ul class="nav days" id="pills-tab" role="tablist">
                                @foreach ($film->actual_sessions() as $day => $sessions)
                                    <li class="nav-item" role="presentation">
                                        <a
                                        class="nav-link @if($loop->first)active @endif"
                                        id="day{{ now()->setDateFrom($day)->format('d') }}-tab"
                                        data-toggle="pill" href="#film{{ $film->id }}day{{ now()->setDateFrom($day)->format('d') }}"
                                        role="tab"
                                        aria-controls="day{{ now()->setDateFrom($day)->format('d') }}"
                                        aria-selected="true"
                                        >
                                            <span class="day">{{ mb_convert_case(now()->setDateFrom($day)->translatedFormat('l'), MB_CASE_TITLE, "UTF-8")  }}</span>
                                            <span class="number">{{ now()->setDateFrom($day)->translatedFormat('j')  }} {{now()->setDateFrom($day)->getTranslatedMonthName('Do MMMM')}}</span>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="d-flex bottom-content">
                            <div class="tab-content" id="pills-tabContent">
                                @foreach ($film->actual_sessions() as $day => $sessions)
                                    @php
                                        $active = false;
                                    @endphp
                                    <div class="tab-pane fade @if($loop->first)show active @endif" id="film{{ $film->id }}day{{ now()->setDateFrom($day)->format('d') }}" role="tabpanel" aria-labelledby="day{{ now()->setDateFrom($day)->format('d') }}-tab">
                                        <ul>
                                            @foreach ($sessions as $key => $session)
                                                @if($loop->parent->first)
                                                    <li class="@if($session->display_at < now()) disabled @elseif(!$active) @php $active = true; @endphp active @endif" >{{ $session->display_at->format('H:i') }}</li>
                                                @else
                                                    <li>{{ $session->display_at->format('H:i') }}</li>
                                                @endif
                                            @endforeach
                                        </ul>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        @endforeach
    </div>
@endsection
