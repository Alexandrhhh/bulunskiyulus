@extends('layouts.cabinet')

@section('title')
	Авторизация
@endsection

@section('content')
<div class="d-flex align-items-center justify-content-center flex-grow-1">
	<div class="white auth">
	<h1>Авторизация</h1>
	<input type="text" class="auth" placeholder="Логин">
	<input type="password" class="auth" placeholder="Пароль">
	<div class="row">
		<div class="col-7">
			<input type="checkbox" id="remember">
			<label for="remember">Запомнить меня</label>
		</div>
		<div class="col-5 text-right">
			<a href="#" class="small">Забыли пароль?</a>
		</div>
	</div>
	<a href="#" class="button primary">Войти</a>
</div>
</div>
@endsection