@extends('layouts.cabinet')

@section('title')
	Добавление трафика
@endsection

@section('content')
<div class="container-fluid">
	@include('include._navbar_cabinet', [
		'links' => [
			0 => [
				'url' => route("cabinet.home"),
				'title' => 'Личный кабинет'
			],
			1 => [
				'title' => "Добавить трафик"
			]
		]
	])
	<h1>Добавление дополнительного трафика</h1>
	<div class="row my-2">
		<div class="col-8">
			<div class="white traffic">
				<ul class="nav-list">
					<li>
						<div class="gray">
							<b>1</b>Гбайт
						</div>
						<span class="price">
							<b>50 </b>₽
						</span>
						<span class="description">
							Дополнительный 1 ГБ
						</span>
						<a href="#" class="button primary-outline">Выбрать</a>
					</li>
					<li>
						<div class="gray">
							<b>3</b>Гбайт
						</div>
						<span class="price">
							<b>150 </b>₽
						</span>
						<span class="description">
							Дополнительные 3 ГБ
						</span>
						<a href="#" class="button primary-outline">Выбрать</a>
					</li>
					<li>
						<div class="gray">
							<b>5</b>Гбайт
						</div>
						<span class="price">
							<b>250 </b>₽
						</span>
						<span class="description">
							Дополнительные 5 ГБ
						</span>
						<a href="#" class="button primary-outline">Выбрать</a>
					</li>
					<li>
						<div class="gray">
							<b>8</b>Гбайт
						</div>
						<span class="price">
							<b>400 </b>₽
						</span>
						<span class="description">
							Дополнительные 8 ГБ
						</span>
						<a href="#" class="button primary-outline">Выбрать</a>
					</li>
					<li>
						<div class="gray">
							<b>10</b>Гбайт
						</div>
						<span class="price">
							<b>500 </b>₽
						</span>
						<span class="description">
							Дополнительные 10 ГБ
						</span>
						<a href="#" class="button primary-outline">Выбрать</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="col-4">
			<div class="white buy">
				<div class="add">
					<div class="mb-2">
						<h3>Способ оплаты</h3>
						<small>Выберите предпочитаемый способ оплаты</small>
					</div>
					<select name="traffic" class="form-control">
						<option value="1">С баланса аккаунта</option>
						<option value="2">Онлайн оплата</option>
					</select>
					<hr class="my-2">
					<div class="d-flex align-items-center justify-content-between mb-2">
						<span>Итого к оплате:</span>
						<span class="order"><b>400</b> ₽</span>
					</div>
					<a href="#" class="button primary">Добавить трафик</a>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection