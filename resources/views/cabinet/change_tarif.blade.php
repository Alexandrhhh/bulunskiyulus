@extends('layouts.cabinet')

@section('title')
	Добавление трафика
@endsection

@section('content')
<div class="container-fluid">
	@include('include._navbar_cabinet', [
		'links' => [
			0 => [
				'url' => route("cabinet.home"),
				'title' => 'Личный кабинет'
			],
			1 => [
				'title' => "Смена тарифа"
			]
		]
	])
	<h1>Смена тарифа</h1>
	<div class="row my-2">
		<div class="col-8">
			<div class="white payment mb-3">
				<h3>Текущий тарифный план</h3>
				<hr>
				<div class="row-tarif row">
					<div class="col-6">
						<div class="description">
							<h4>Тариф {{ Auth::user()->tarif->tarif_name }}</h4>
							<p>
								{{ Auth::user()->tarif->tarifdescr }}
							</p>
						</div>
					</div>
					<div class="col-6">
						<div class="block row align-items-stretch">
							<div class="col-6 p-0">
								<div class="tarif d-flex align-items-center justify-content-center">
									<span class="count">{{ round(str_replace(' ', '', explode(' Мб', explode(' мб', Auth::user()->tarif->tarifdescr)[0])[0])/1000) }}</span>
									<div class="rows d-flex flex-column">
										<span>Гбайт</span>
										<span>Месяц</span>
									</div>
								</div>
							</div>
							<div class="col-6 p-0">
								<div class="price">
									<span><b>{{ str_replace(' Руб.', '', Auth::user()->tarif->user_pay)  }}</b> ₽</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="white payment">
				<h3>Выберите новый тарифный план</h3>
				<hr>
				@foreach(Auth::user()->changeTariffs as $tariff)
				<div class="radio">
					<div class="row-tarif row">
						<div class="col-6">
							<div class="description">
								<h4>{{ $tariff->tarif_name }}</h4>
								<p>
									{{ $tariff->tarifdescr }}
								</p>
							</div>
						</div>
						<div class="col-6">
							<div class="block no-shadow row align-items-stretch">
								<div class="col-6 p-0">
									<div class="tarif d-flex align-items-center justify-content-center">
										<span class="count">{{ round(str_replace(' ', '', explode(' Мб', explode(' мб', $tariff->tarifdescr)[0])[0])/1000) }}</span>
										<div class="rows d-flex flex-column">
											<span>Гбайт</span>
											<span>Месяц</span>
										</div>
									</div>
								</div>
								<div class="col-6 p-0">
									<div class="price">
										<span><b>{{ str_replace(' Руб.', '', $tariff->user_pay) }}</b> ₽</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
                @endforeach
				<hr>
				<a href="#" class="button primary inline">Сменить</a>
			</div>
		</div>
		<div class="col-4">
		</div>
	</div>
</div>
@endsection
