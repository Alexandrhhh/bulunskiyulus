@extends('layouts.cabinet')

@section('title')
	Добавление трафика
@endsection

@section('content')
<div class="container-fluid">
	@include('include._navbar_cabinet', [
		'links' => [
			0 => [
				'url' => route("cabinet.home"),
				'title' => 'Личный кабинет'
			],
			1 => [
				'title' => "Пополнение лицевого счёта"
			]
		]
	])
	<h1>Пополнение лицевого счёта</h1>
	<div class="row my-2">
		<div class="col-8">
			<div class="white payment mb-3">
				<h3>Способ оплаты</h3>
				<select name="method" id="" class="form-control mt-2">
					<option value="">Банковской картой</option>
				</select>
			</div>
			<div class="white payment">
				<h3>Действующий тарифный план</h3>
				<hr>
				<div class="row-tarif row">
					<div class="col-6">
						<div class="description">
							<h4>Тариф «Начало»</h4>
							<p>
								Значимость этих проблем настолько свойств очевидна, что сложившаяся структура ранга, имеет важнешее составляющее
							</p>
						</div>
					</div>
					<div class="col-6">
						<div class="block row align-items-stretch">
							<div class="col-6 p-0">
								<div class="tarif d-flex align-items-center justify-content-center">
									<span class="count">12</span>
									<div class="rows d-flex flex-column">
										<span>Гбайт</span>
										<span>Месяц</span>
									</div>
								</div>
							</div>
							<div class="col-6 p-0">
								<div class="price">
									<span><b>540</b> ₽</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<hr>
				<a href="#" class="button gray inline">Сменить тариф</a>
			</div>
		</div>
		<div class="col-4">
			<div class="white buy">
				<div class="add">
					<div class="mb-2">
						<h3>Способ доставки чека</h3>
						<small class="no-wrap">После оплаты вам будет направлен кассовый чек</small>
					</div>
					<input type="radio" name="delivery" id="first_delivery">
					<label for="first_delivery">На электронную почту</label>
					<input type="radio" name="delivery" id="second_delivery">
					<label for="second_delivery">На номер телефона</label>
					<hr class="my-2">
					<div class="d-flex align-items-center justify-content-between mb-2">
						<span>К оплате</span>
						<div class="icon-rub">
							<input type="number" class="rub">
						</div>
					</div>
					<a href="#" class="button primary">Пополнить</a>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection