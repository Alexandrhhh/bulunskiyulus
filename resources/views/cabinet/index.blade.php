@extends('layouts.cabinet')

@section('title')
	Главная страница
@endsection

@section('content')
<div class="container-fluid flex-grow-1 d-flex">
	<div class="row flex-grow-1">
		<aside class="col-4 flex-grow-1">
			<h3>Баланс аккаунта</h3>
			<div class="d-flex balance my-2 align-items-center">
				<div class="ico">
					<i class="icon rub"></i>
				</div>
				<div class="d-flex flex-column">
					<small>Доступные средства:</small>
					<span class="current">{{str_replace(',','.',str_replace(' Руб.', '', Auth::user()->ballance))}} <i>₽</i></span>
				</div>
			</div>
			<a href="{{ route('cabinet.payment') }}" class="button primary">Пополнить счёт</a>
			<hr class="mb-2">
			<div class="d-flex justify-content-between">
				<h3>Остаток трафика</h3>
				<span>
					Гб Мб
				</span>
			</div>
			<div class="stat my-2"></div>
			<a href="{{ route('cabinet.add_traffic') }}" class="button primary-outline">Добавить трафик</a>
			<hr>
			<h3>Тарифный план</h3>
			<div class="d-flex plan my-2 align-items-center">
				<div class="ico">
					<i class="icon globus"></i>
				</div>
				<div class="d-flex flex-column">
					<div class="current">Тариф {{ Auth::user()->tarif->tarif_name }}</div>
					<small>{{ Auth::user()->tarif->tarifdescr }}</small>
				</div>
			</div>
			<a href="{{ route('cabinet.change_tarif') }}" class="button primary-outline">Сменить тариф</a>
		</aside>
		<div class="col-8 pt-2">
			<div class="col-12 pr-0">
				<div class="white my-2 traffic">
					<h3>Статистика расхода трафика</h3>
					<hr class="my-2">
					<div class="row align-items-center mb-2">
						<div class="col-6">
							<div class="row count">
								<div class="col-4 d-flex flex-column">
									<small>Всего</small>
									<span><b>1.95</b> <small>ГИГ</small></span>
								</div>
								<div class="col-4 d-flex flex-column">
									<small>Принято</small>
									<span><i class="icon up"></i>1.32 <small>ГИГ</small></span>
								</div>
								<div class="col-4 d-flex flex-column">
									<small>Передано</small>
									<span><i class="icon down"></i>0.74 <small>ГИГ</small></span>
								</div>
							</div>
						</div>
						<div class="col-6 text-right">
							<ul class="nav nav-pills nav-days" id="pills-tab" role="tablist">
								<li class="nav-item" role="presentation">
									<a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">День</a>
								</li>
								<li class="nav-item" role="presentation">
									<a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Неделя</a>
								</li>
								<li class="nav-item" role="presentation">
									<a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Месяц</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							<div class="tab-content" id="pills-tabContent">
  								<div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
									<img src="{{ asset('images/Table.jpg') }}" alt="" class="fluid">
								</div>
								<div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
									<img src="{{ asset('images/Table.jpg') }}" alt="" class="fluid">
								</div>
								<div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
									<img src="{{ asset('images/Table.jpg') }}" alt="" class="fluid">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="white devices mb-3">
					<div class="d-flex justify-content-between align-items-center">
						<h3>Подключенные устройства</h3>
						<a href="#" class="button primary-outline inline small">Показать все устройства</a>
					</div>
					<hr class="mt-1 mb-2">
					<div class="table-responsive">
						<table class="table table-devices">
							<thead>
								<th>Устройство</th>
								<th>Расход трафика</th>
								<th>IP Адрес</th>
								<th>Статус</th>
							</thead>
							<tbody>
                                @foreach(Auth::user()->devices->take(5) as $device)
								<tr>
									<td class="align-middle">
										<div class="d-flex align-items-center">
											<div class="ico">
												<i class="icon @if($device['type']) tablet @else phone @endif"></i>
											</div>
											<span>{{$device['title']}}</span>
										</div>
									</td>
									<td class="align-middle"><i class="icon network"></i> <b>{{$device['traffic']}}</b> <small>ГИГ</small></td>
									<td class="align-middle">{{$device['ip']}}</td>
									<td class="align-middle"><span class="status active">Активен</span></td>
								</tr>
                                @endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
