@extends('layouts.app')

@section('title')
    Расписание Автобуса
@endsection

@section('content')
    <div class="d-flex align-items-center justify-content-between">
        <h2 class="thin">Расписание движения маршрутного такси</h2>
    </div>
    <div class="row road mb-4">
        <div class="col-12">
            <div class="white block">
                @foreach ($bus->stops()->with('name')->with('day')->get()->sortBy('day.id')->groupBy('day.id')->sortBy('time') as $key => $group)
                    @foreach ($group as $key => $value)
                        @php $top = false; @endphp
                        @if(in_array(now()->format('D'), explode(', ', $value->day->eng)))
                            @foreach ($group->sortBy('time') as $key => $value)
                                @if(strtotime(date('Y-m-d') . " " . now()->format('H:i:s')) > strtotime(date('Y-m-d') . " " . $value->time->format('H:i:s')))
                                @else
                                    @php
                                        $top = true;
                                        $current_id = $value->id;
                                    @endphp
                                    @break
                                @endif
                                @if($loop->last)
                                    @if(strtotime(date('Y-m-d') . " " . now()->format('H:i:s')) > strtotime(date('Y-m-d') . " " . $value->time->format('H:i:s')))
                                        @foreach ($group->sortBy('time') as $key => $value)
                                            @if(in_array(now()->format('D'), explode(', ', $value->day->eng)))
                                                @php
                                                    $top = true;
                                                    $current_id = $value->id;
                                                @endphp
                                                @break
                                            @endif
                                        @endforeach
                                    @endif
                                @endif
                            @endforeach
                        @else
                            @if($loop->parent->last)
                                @if(!in_array(now()->format('D'), explode(', ', $value->day->eng)) && $top)
                                    @foreach ($bus->stops()->with('name')->with('day')->get()->sortBy('day.id')->groupBy('day.id')->sortBy('time') as $key => $group)
                                        @php
                                            $current_id = null;
                                        @endphp
                                        @break
                                    @endforeach
                                @endif
                            @endif
                        @endif
                        @break
                    @endforeach
                @endforeach
                @foreach ($bus->stops()->with('name')->with('day')->get()->sortBy('day.id')->groupBy('day.id')->sortBy('time') as $key => $group)
                    <div class="header text-center big">
                        {{ $group[0]->day->title }}
                    </div>
                    <div class="row schedule-bus">
                        @foreach ($group->sortBy('name.id')->groupBy('name.id') as $key => $stop_name)
                            <div class="col text-center d-flex flex-column justify-content-between @if(!$loop->parent->first) pt-0 @endif">
                                @if($loop->parent->first)
                                    <div class="div">
                                        <span class="name">{{ $stop_name[0]->name->title }}</span>
                                        @if($stop_name[0]->name->description)<span class="description">{{ $stop_name[0]->name->description }}</span>@endif
                                    </div>
                                @endif
                                <ul class="schedule">
                                    @foreach ($stop_name->sortBy('time') as $key => $stop)
                                        <li @if($current_id == $stop->id) class="active" @endif><span>{{ $stop->time->format('H:i') }}</span></li>
                                    @endforeach
                                </ul>
                            </div>
                        @endforeach
                    </div>
                @endforeach
                <div class="header orange text-center">
                    ВОСКРЕСЕНЬЕ ВЫХОДНОЙ
                </div>
            </div>
        </div>
    </div>
@endsection
