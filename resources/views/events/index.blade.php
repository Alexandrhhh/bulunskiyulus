@extends('layouts.app')

@section('title')
    Новости
@endsection

@section('content')

    <div class="row mb-4">
        <div class="col-12 mb-3">
            <div class="d-flex justify-content-between align-items-center">
                <h2>Киноафиша</h2>
                <a href="{{ route('afisha') }}">Все фильмы</a>
            </div>
        </div>
        <div class="col-12">
            <div class="row afisha">
                @foreach ($films as $key => $film)
                    <div class="col film">
                        <div class="image" style="background-image: url('{{ Storage::url($film->image) }}')"></div>
                        <h4 class="title">{{ $film->title }}</h4>
                        <span class="time">{{ Carbon\CarbonInterval::minutes($film->duration)->cascade()->forHumans() }}</span>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="d-flex align-items-center justify-content-between">
        <h2>События</h2>
        <div class="d-flex align-items-center change-region">
            <span class="mr-3">Фильтр по региону: </span>
            <div class="btn-group">
                @foreach ($sections as $key => $section)
                    @if(request()->is("events/section/{$section->id}*"))
                        <a href="#" class="dropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{ $section->title }}
                        </a>
                    @endif
                @endforeach
                <div class="dropdown-menu">
                    @foreach ($sections as $key => $section)
                        <a class="dropdown-item @if(request()->is("events/section/{$section->id}*")) active @endif" href="{{ route('events.section', ['section_id' => $section->id]) }}">{{ $section->title }}</a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="row events">
        <div class="col-12">
            <div class="row">
                @if($events->count() == 0)
                    <p>Мероприятий нет</p>
                @else
                    @foreach ($events as $key => $event)
                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="block white @if($event->style == 1) big @endif">
                                <div class="top" style="background-image:url('{{ Storage::url($event->image) }}')">
                                    <span class="category {{ $event->category->color->eng }}">{{ $event->category->title }}</span>
                                </div>
                                <div class="bottom">
                                    <div class="content">
                                        <span class="date">{{ $event->created_at->format('d.m.Y') }}</span>
                                        <div class="title mb-1">{{ $event->title }}</div>
                                        @if($event->short)<span class="date mb-2">{{ $event->short }}</span>@endif
                                        <a href="{{ route('events.event', ['id' => $event->id]) }}" class="button extra @if($event->style == 0) btn-white @else btn-transparent @endif">Подробнее</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
@endsection
