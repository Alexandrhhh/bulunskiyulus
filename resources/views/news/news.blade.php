@extends('layouts.app')

@section('title')
    Новость
@endsection

@section('content')
    @php
        $i = $news->comments->count();
        foreach ($news->comments as $key => $comment) {
            $i += $comment->comments->count();
        }
    @endphp
    <div class="fullnews white @if($news->poll) mb-3 @else mb-5 @endif">
        <div class="header" style="background-image: url('{{ Storage::url($news->image_url) }}')">
            <div class="absolute">
                <div class="info d-flex align-items-center">
                    <span class="category {{ $news->category->color->eng }}">{{ $news->category->title }}</span>
                    <span class="date mx-3">{{ $news->created_at->format('d.m.Y') }}</span>
                    <span class="time">{{ $news->created_at->format('H:i') }}</span>
                </div>
                <h1>
                    {{ $news->title }}
                </h1>
                <hr>
                <div class="stats d-flex align-items-center">
                    <span class="mr-3 d-flex align-items-center"><i class="icon read min"></i>{{ $news->reads }}</span>
                    <span class="mr-3 d-flex align-items-center"><i class="icon comments min"></i>{{ $i }}</span>
                    <span class="d-flex align-items-center"><i class="icon profile min"></i>{{ $news->user->first_name }} {{ $news->user->last_name }}</span>
                </div>
            </div>
            <a href="#"class="share"></a>
        </div>
        <div class="content">
            {!! html_entity_decode($news->content) !!}
            <div class="info d-flex align-items-center justify-content-between">
                <div class="profile">
                    <div class="img">
                        <img src="@if($news->user && $news->user->photo){{ Storage::url($news->user->photo) }}@else{{ asset('images/noavatar.jpg') }}@endif" alt="">
                    </div>
                    <span class="name">{{ $news->user->first_name }} {{ $news->user->last_name }}</span>
                </div>
                <div class="d-flex align-items-center">
                    <i class="icon like @auth click @if($like) active @endif @endauth" @auth data-class="{{ get_class($news) }}" data-id="{{ $news->id }}" data-user="{{ Auth::user()->id }}" @endauth></i><span>{{ $news->likes->count() }}</span>
                    <i class="icon read min gray ml-4"></i>{{ $news->reads }}
                    <i class="icon comments min gray ml-4"></i>{{ $i }}
                </div>
            </div>
            <div class="tags">
                @foreach ($news->tags as $key => $tag)
                    <span>#{{ $tag->title }}</span>
                @endforeach
            </div>
        </div>
    </div>
    @if($news->poll)
        <div class="white mb-4 poll">
            <div class="header">
                <div class="title">
                    {{ $news->poll->title }}
                </div>
                <span class="subtitle">
                    {{ $news->poll->subtitle }}
                </span>
            </div>
            <div class="content">
                @include('include.poll')
            </div>
        </div>
    @endif
    @auth
        <form action="{{ route('comment.create', ['id' => $news->id]) }}" method="post">
            @csrf
            <div class="white new mb-4">
                <div class="header">Ваш комментарий</div>
                <div class="content">
                    <div class="img">
                        <img src="@if(Auth::user() && Auth::user()->photo){{ Storage::url(Auth::user()->photo) }}@else{{ asset('images/noavatar.jpg') }}@endif" alt="">
                    </div>
                    <input hidden name="class" value="{{ get_class($news) }}">
                    <input hidden name="id" value="{{ $news->id }}">
                    <textarea class="autosize" name="text" rows="1" cols="80" placeholder="Напишите ваш комментарий"></textarea>
                    <button type="submit" class="send mt-auto"></button>
                </div>
            </div>
        </form>
    @endauth
    @foreach ($news->comments as $key => $comment)
        <div class="white topic mb-4">
            <div class="img">
                <img src="@if($comment->user && $comment->user->photo){{ Storage::url($comment->user->photo) }}@else{{ asset('images/noavatar.jpg') }}@endif" alt="">
            </div>
            <div class="content">
                <div class="header mb-2">
                    <div class="name">@if($comment->user) {{ $comment->user->first_name }} {{ $comment->user->last_name }} @endif</div>
                    <div class="date">{{ $comment->created_at->diffForHumans() }}</div>
                </div>
                <p>{{ $comment->text }}</p>
                @foreach ($comment->comments as $key => $subcomment)
                    <div class="topic p-0">
                        <div class="img">
                            <img src="@if($comment->user && $comment->user->photo){{ Storage::url($comment->user->photo) }}@else{{ asset('images/noavatar.jpg') }}@endif" alt="">
                        </div>
                        <div class="content">
                            <div class="header mb-1">
                                <div class="name">@if($comment->user) {{ $comment->user->first_name }} {{ $comment->user->last_name }} @endif</div>
                                <div class="date">{{ $subcomment->created_at->diffForHumans() }}</div>
                            </div>
                            <p>{{ $subcomment->text }}</p>
                        </div>
                    </div>
                @endforeach
                @auth
                    <form action="{{ route('comment.create', ['id' => $comment->id]) }}" method="post">
                        @csrf
                        <div class="new p-0">
                            <div class="content p-0 pt-2">
                                <div class="img">
                                    <img src="@if($comment->user && $comment->user->photo){{ Storage::url($comment->user->photo) }}@else{{ asset('images/noavatar.jpg') }}@endif" alt="">
                                </div>
                                <input hidden name="class" value="{{ get_class($comment) }}">
                                <input hidden name="id" value="{{ $comment->id }}">
                                <textarea class="autosize" name="text" rows="1" cols="80" placeholder="Напишите ваш комментарий"></textarea>
                                <button type="submit" class="send mt-auto"></button>
                            </div>
                        </div>
                    </form>
                @endauth
            </div>
        </div>
    @endforeach
@endsection
