@extends('layouts.app')

@section('title')
    Новости
@endsection

@section('content')
    <div class="d-flex align-items-center justify-content-between">
        <h2>Все новости</h2>
        <div class="d-flex align-items-center change-region">
            <span class="mr-3">Фильтр по региону: </span>
            <div class="btn-group">
                @foreach ($sections as $key => $section)
                    @if(request()->is("news/section/{$section->id}*"))
                        <a href="#" class="dropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{ $section->title }}
                        </a>
                    @endif
                @endforeach
                <div class="dropdown-menu">
                    @foreach ($sections as $key => $section)
                        <a class="dropdown-item @if(request()->is("news/section/{$section->id}*")) active @endif" href="{{ route('news.section', ['id' => $section->id]) }}">{{ $section->title }}</a>
                    @endforeach
                </div>

            </div>
        </div>
    </div>
    <div class="row news">
        <div class="col-12">
            <div class="row">
                @if($newses->count() == 0)
                    <p>Новостей нет</p>
                @else
                    @foreach ($newses as $key => $news)
                        @php
                            $i = $news->comments->count();
                            foreach ($news->comments as $key => $comment) {
                                $i += $comment->comments->count();
                            }
                        @endphp
                        <div class="block flex-sm-row flex-column d-flex white">
                            <div class="image p-3" style="background-image: url('{{ Storage::url($news->image_url) }}')">
                                <span class="category {{ $news->category->color->eng }}">{{ $news->category->title }}</span>
                            </div>
                            <div class="content">
                                <div class="title">
                                    <a href="{{ route('news.news', ['id' => $news->id]) }}">{{ $news->title }}</a>
                                </div>
                                <div class="short">
                                    {{ $news->short }}
                                </div>
                                <div class="info d-flex justify-content-between align-items-center py-3">
                                    <span class="time">{{ $news->created_at->format('d.m.Y в H:i') }}</span>
                                    <div class="d-flex stats align-items-center">
                                        <i class="icon read"></i>{{ $news->reads }}<i class="icon comments ml-4"></i>{{ $i }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
@endsection
