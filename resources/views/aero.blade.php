@extends('layouts.app')

@section('title')
    Авиарейсы
@endsection

@section('content')
    <div class="d-flex align-items-center justify-content-between">
        <h2>Расписание авиарейсов</h2>
    </div>
    <div class="row aero mb-4">
        <div class="col-12">
            <div class="block white mb-4">
                <div class="header">Аэропорт Тикси (IKS)</div>
                <div class="row contacts">
                    <div class="col-sm col-12 text-center">
                        <i class="icon phone"></i>
                        <a href="tel:+74116728337">8 (411) 672-83-37</a>
                    </div>
                    <div class="col-sm col-12 text-center">
                        <i class="icon map"></i>
                        <span>ул. Полярной Авиации, 4, Тикси, Респ. Саха (Якутия), 678400</span>
                    </div>
                    <div class="col-sm col-12 text-center">
                        <i class="icon mail"></i>
                        <a href="mailto:pal@tiksi.sakha.ru">pal@tiksi.sakha.ru</a>
                    </div>
                </div>
            </div>
        </div>
        @foreach ($planes as $key => $plane)
            <div class="col-12 mb-3">
                <div class="white plane">
                    <div class="row m-0 align-items-center flex-column flex-md-row">
                        <div class="col logo h-100">
                            <div class="d-flex text-md-left text-center align-items-center justify-content-center justify-content-md-start">
                                <div class="image"><img src="{{ Storage::url($plane->company->logotype) }}" class="img-fluid" alt=""></div>
                                <div class="info text-left">
                                    <div class="flight">{{ $plane->flight }}</div>
                                    <div class="title">{{ $plane->company->title }}</div>
                                </div>
                            </div>
                        </div>
                        <div class="col mb-3 mb-md-0 text-center text-md-center">
                            <div class="departure">{{ $plane->departure }}</div>
                            <span class="date">@if(now()->format('d.m.Y') == $plane->departure_at->format('d.m.Y')) Сегодня, {{ $plane->departure_at->format('H:i') }} @else {{ $plane->departure_at->isoFormat('Do MMMM, HH:mm') }}@endif</span>
                        </div>
                        <div class="col mb-3 mb-md-0 d-flex align-items-center justify-content-center lines">
                            <div class="air">
                                @include('include.plane', ['color' => $plane->status->color])
                            </div>
                            <span class="time">
                                {{ $plane->arrival_at->diffInHours($plane->departure_at) }}:@if($plane->arrival_at->subHours($plane->arrival_at->diffInHours($plane->departure_at))->diffInMinutes($plane->departure_at) == 0)00 @else{{ $plane->arrival_at->subHours($plane->arrival_at->diffInHours($plane->departure_at))->diffInMinutes($plane->departure_at) }} @endif</span>
                        </div>
                        <div class="col mb-2 mb-md-0 text-center">
                            <div class="arrival">{{ $plane->arrival }}</div>
                            <span class="date">@if(now()->format('d.m.Y') == $plane->arrival_at->format('d.m.Y')) Сегодня, {{ $plane->arrival_at->format('H:i') }} @else {{ $plane->arrival_at->isoFormat('Do MMMM, HH:mm') }}@endif</span>
                        </div>
                        <div class="col-md-2 col-12 mb-3 mb-md-0 text-md-right text-center"><span class="status" style="color: {{ $plane->status->color }}">{{ $plane->status->title }}</span></div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection
