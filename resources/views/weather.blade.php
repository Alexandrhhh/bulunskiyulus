@extends('layouts.app')

@section('title')
    Прогноз погоды
@endsection

@section('content')
    <div class="d-flex align-items-center justify-content-between">
        <h2 class="thin">Прогноз погоды</h2>
    </div>
    <div class="row weather mb-4">
        {{-- {{ $weather->setting }} --}}
        <div class="col-12 d-md-block d-none">
            <ul class="nav nav-pills" id="pills-tab" role="tablist">
                @foreach ($weather as $key => $day)
                    <li class="nav-item flex-grow-1" role="presentation">
                        <a
                            class="nav-link @if($loop->first) active @endif"
                            id="{{ str_replace(".", '', $key) }}-tab"
                            data-toggle="pill"
                            href="#tab{{ str_replace(".", '', $key) }}"
                            role="tab"
                            aria-controls="{{ str_replace(".", '', $key) }}"
                            aria-selected="true">
                            <span class="d-flex align-items-center">
                                <span class="day">
                                    {{ now()->setDateFrom($key)->translatedFormat('j')  }}
                                </span>
                                <span class="d-flex flex-column">
                                    <span class="mounth">
                                        {{ mb_convert_case(now()->setDateFrom($key)->getTranslatedMonthName('Do MMMM'), MB_CASE_TITLE, "UTF-8")  }}
                                    </span>
                                    <span class="week">
                                        {{ mb_convert_case(now()->setDateFrom($key)->translatedFormat('l'), MB_CASE_TITLE, "UTF-8")  }}
                                    </span>
                                </span>
                            </span>


                        </a>
                    </li>
                @endforeach
            </ul>
            <div class="tab-content white" id="pills-tabContent">
                @foreach ($weather as $key => $day)
                    <div class="tab-pane fade @if($loop->first) show active @endif" id="tab{{ str_replace(".", '', $key) }}" role="tabpanel" aria-labelledby="{{ str_replace(".", '', $key) }}-tab">
                        <ul>
                            @foreach ($day as $key2 => $data)
                                <li class="d-flex align-items-center row">
                                    <span class="temperature d-flex flex-column col">
                                        <span class="title">
                                            @switch($loop->index)
                                                @case(0)
                                                    Ночью
                                                    @break
                                                @case(1)
                                                    Утром
                                                    @break
                                                @case(2)
                                                    Днем
                                                    @break
                                                @case(3)
                                                    Вечером
                                                    @break
                                            @endswitch
                                        </span>
                                        <span>@if($data->temperature->air->C > 0) +@elseif($data->temperature->air->C < 0) @endif{{ round($data->temperature->air->C) }}°</span>
                                    </span>
                                    <span class="icon d-flex align-items-center col">
                                        <img src="/images/gm/{{ $data->icon }}.svg" style="max-width: 40px" alt="">
                                        <span>{{ $data->description->full }}</span>
                                    </span>
                                    <span class="pressure d-flex flex-column col">
                                        <span class="title">Давление, мм рт.</span>
                                        <span>{{ $data->pressure->mm_hg_atm }}</span>
                                    </span>
                                    <span class="wind_speed d-flex flex-column col">
                                        <span class="title">Скорость ветра</span>
                                        <span>{{ $data->wind->speed->m_s }} м/с</span>
                                    </span>
                                    <span class="humidity d-flex flex-column col">
                                        <span class="title">Влажность</span>
                                        <span>{{ $data->humidity->percent }}%</span>
                                    </span>

                                </li>
                                {{-- {{ dd($data) }} --}}
                            @endforeach
                        </ul>
                    </div>
                @endforeach
            </div>
		</div>
		<div class="col-12 d-block d-md-none">
			@foreach($weather as $key => $day)
				<div class="white day-weather mb-4">
					<h3>{{ mb_convert_case(now()->setDateFrom($key)->translatedFormat('l'), MB_CASE_TITLE, "UTF-8")  }}, <i>{{ now()->setDateFrom($key)->translatedFormat('j')  }}</i> <b>{{ mb_convert_case(now()->setDateFrom($key)->getTranslatedMonthName('Do MMMM'), MB_CASE_TITLE, "UTF-8")  }}</b></h3>
					@foreach($day as $data)
						<div class="d-flex justify-content-between gray-row">
							<div>
								<img src="/images/gm/{{ $data->icon }}.svg" style="max-width: 40px" alt="">
								<span>
									@if($data->temperature->air->C > 0)
										+
									@elseif($data->temperature->air->C < 0)
										-
									@endif
									{{ round($data->temperature->air->C) }}°,
								</span>
								<span>
									{{ $data->description->full }}
								</span>
							</div>
							<span>
								@switch($loop->index)
									@case(0)
										Ночью
										@break
									@case(1)
										Утром
										@break
									@case(2)
										Днем
										@break
									@case(3)
										Вечером
										@break
								@endswitch
							</span>
						</div>
					@endforeach
				</div>
			@endforeach
		</div>
    </div>
@endsection
