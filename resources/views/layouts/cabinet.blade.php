<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ mix('/css/cabinet.css') }}">
    <meta name="_token" content="{!! csrf_token() !!}" />
    <title>@yield('title')</title>
</head>

<body class="d-flex flex-column">
	@include('include._header_cabinet')
	@yield('content')
    <script src="{{ mix('/js/app.js') }}"></script>
    @yield('scripts')
</body>

</html>
