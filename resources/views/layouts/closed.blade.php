<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
    <meta name="_token" content="{!! csrf_token() !!}" />
    <title>@yield('title')</title>
</head>
<body style="min-height: 100vh; display: flex; flex-direction: column">
    <header>
        <div class="top d-flex justify-content-center align-items-center">
            <a href="/" class="logotype col-auto">Булунский Портал</a>
        </div>
    </header>
    <div class="container d-flex justify-content-center align-items-center h-100 closed">
        <div class="row">
            <div class="col-12">
                @yield('content')
            </div>
        </div>
    </div>
    <script src="{{ mix('/js/app.js') }}"></script>
</body>
</html>
