<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
    <meta name="_token" content="{!! csrf_token() !!}" />
    <title>@yield('title')</title>
</head>
<body>
    <header>
        <div class="top d-flex justify-content-between align-items-center">
            <div class="arrow col-auto" data-toggle="button" aria-pressed="false" autocomplete="off" role="button">
                @if(Auth::check())
                    <span>{{ Auth::user()->city->title }}</span>
                    <div class="dropdown white">
                        @foreach ($cities as $key => $city)
                            <a href="#" onclick="location.href='{{ route('change_city', ['id' => $city->id]) }}';" class="change_city">{{ $city->title }}</a>
                        @endforeach
                    </div>
                @else
                    <span>Тикси</span>
                    <div class="dropdown white">
                        <small>Необходимо выполнить <a href="#loginForm" data-toggle="modal" data-target="#loginForm">вход</a></small>
                    </div>
                @endif
            </div>
            <a href="/" class="logotype d-none d-lg-block col-auto">Булунский Портал</a>
            <div class="col-auto">
                @if(Auth::check())
                    <a href="{{ route('lk.index') }}" class="login-form mr-3" >Профиль</a>
                    <a href="{{ route('logout') }}" class="login-form" >Выйти</a>
                @else
                    <a href="#loginForm" class="login-form" data-toggle="modal" data-target="#loginForm">Войти</a>
                @endif
            </div>
        </div>
        <div class="bottom d-flex align-items-center justify-content-between">
            <div class="menu">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
            <nav class="navbar navbar-expand-lg navbar-light bg-white">
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item @if(Route::is('index')) active @endif"><a href="{{ route('index') }}" class="nav-link">Главная</a></li>
                            <li class="nav-item @if(Route::is('news.*')) active @endif"><a href="{{ route('news.index') }}" class="nav-link">Новости</a></li>
                                <li class="nav-item @if(Route::is('ads.*')) active @endif"><a href="{{ route('ads.index') }}" class="nav-link">Объявления</a></li>
                                <li class="nav-item @if(Route::is('events.*')) active @endif"><a href="{{ route('events.index') }}" class="nav-link">Афиша</a></li>
                                <li class="nav-item @if(Route::is('road')) active @endif"><a href="{{ route('road') }}" class="nav-link">Транспорт</a></li>
                                <li class="nav-item @if(Route::is('forum.*')) active @endif"><a href="{{ route('forum.index') }}" class="nav-link">Форум</a></li>
                                <li class="nav-item @if(Route::is('administration.*')) active @endif"><a href="{{ route('administration.index') }}" class="nav-link">Администрация</a></li>
                                <li class="nav-item @if(Route::is('archive.*')) active @endif"><a href="{{ route('archive.index') }}" class="nav-link">Архив</a></li>
                            </ul>
                        </div>
                        <a href="/" class="logotype d-lg-none d-md-block">Булунский Портал</a>
                    </nav>
                    <div class="search">
                        <div class="icon"></div>
                        <form action="" method="get">
                            <div class="input_search">
                                <input type="text" class="white" placeholder="Введите фразу для поиска">
                            </div>
                        </form>
                    </div>
                </div>
            </header>
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-9">
                        @yield('content')
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="block white mb-4 weather text-center">
                            <div class="header">Погода</div>
                            @if(Auth::check())
                                <div class="d-flex align-items-center justify-content-center temperature mb-3">
                                    <img src="/images/gm/{{Auth::user()->city->weather->icon_name}}.svg" alt="">
                                    <span><b>@if(Auth::user()->city->weather->temperature != 0 && Auth::user()->city->weather->temperature > 0)+ @elseif(Auth::user()->city->weather->temperature != 0 && Auth::user()->city->weather->temperature > 0)- @endif{{ round(Auth::user()->city->weather->temperature) }}</b> °С</span>
                                </div>
                                <div class="about text-center mb-3">
                                    {{ Auth::user()->city->weather->about }}
                                </div>
                                <div class="wind text-center mb-3">
                                    Ветер {{ Str::lower(Auth::user()->city->weather->wind->title) }} <b>{{ Auth::user()->city->weather->wind_speed }} м/с</b>
                                </div>
                            @else
                                @foreach ($cities as $key => $city)
                                    @if($loop->first)
                                        <div class="d-flex align-items-center justify-content-center temperature mb-3">
                                            <img src="/images/gm/{{$city->weather->icon_name}}.svg" alt="">
                                            <span><b>@if($city->weather->temperature != 0 && $city->weather->temperature > 0)+ @elseif($city->weather->temperature != 0 && $city->weather->temperature > 0)- @endif{{ round($city->weather->temperature) }}</b> °С</span>
                                        </div>
                                        <div class="about text-center mb-3">
                                            {{ $city->weather->about }}
                                        </div>
                                        <div class="wind text-center mb-3">
                                            Ветер {{ Str::lower($city->weather->wind->title) }} <b>{{ $city->weather->wind_speed }} м/с</b>
                                        </div>
                                    @endif
                                @endforeach
                            @endif
                            <a href="{{ route('weather') }}" class="button btn-outline-blue small mb-4">ПРОГНОЗ ПОГОДЫ</a>
                            @if(Cache::get('alert'))
                                <hr style="my-0 mb-2">
                                <div class="d-flex flex-column mb-3">
                                    <span style="font-family: 'Golos';font-style: normal;font-weight: 600;font-size: 11.6677px;color: #D86E28;">ОТМЕНА ЗАНЯТИЙ</span>
                                    <span style="font-family: 'Golos';font-style: normal;font-weight: 600;font-size: 11.6677px;text-transform: uppercase;color: #333333;">{{ Cache::get('alert') }}</span>
                                </div>
                            @endif

                        </div>
                        @if($mayak_vipusk)
                            <div class="block white mb-4 text-center">
                                <div class="header">Маяк Арктики</div>
                                <a href="{{ route('archive.show_mayak', ['id' => $mayak_vipusk->id]) }}"><img src="{{ Storage::url($mayak_vipusk->image) }}" class="img-fluid" alt=""></a>
                            </div>
                        @endif
                        <div class="block white mb-4 text-center">
                            <div class="header">Дороги</div>
                            <a href="{{ route('road') }}"><img src="{{ asset('images/road.jpg') }}" class="img-fluid" alt=""></a>
                        </div>
                        <div class="block white mb-4 text-center">
                            <div class="header">Автобус</div>
                            <div class="bus">
                                @if(!empty($bus))
                                    @foreach ($bus->stops()->with('name')->with('day')->get()->sortBy('day.id')->groupBy('day.id')->sortBy('time') as $key => $group)
                                        @php $top = false; @endphp
                                        @foreach ($group as $key => $value)
                                            @if(in_array(now()->format('D'), explode(', ', $value->day->eng)))
                                                @foreach ($group->sortBy('time') as $key => $value)
                                                    @if(strtotime(date('Y-m-d') . " " . now()->format('H:i:s')) > strtotime(date('Y-m-d') . " " . $value->time->format('H:i:s')))
                                                    @else
                                                        @php $top = true; @endphp
                                                        @if($loop->first)<span>Сегодня</span>@else<span>Текущая остановка:</span>@endif
                                                        <span class="current-stop">{{ $value->name->title }}</span>
                                                        @if($value->name->description) <span class="gray">{{ $value->name->description }}</span>@endif
                                                        <span class="time">{{ $value->time->format('H:i') }}</span>
                                                        @break
                                                    @endif
                                                    @if($loop->last)
                                                        @if(strtotime(date('Y-m-d') . " " . now()->format('H:i:s')) > strtotime(date('Y-m-d') . " " . $value->time->format('H:i:s')))
                                                            @foreach ($group as $key => $value)
                                                                @if(in_array(now()->format('D'), explode(', ', $value->day->eng)))
                                                                    @php $top = true; @endphp
                                                                    <span>Завтра</span>
                                                                    <span class="current-stop">{{ $value->name->title }}</span>
                                                                    @if($value->name->description) <span class="gray">{{ $value->name->description }}</span>@endif
                                                                    <span class="time">{{ $value->time->format('H:i') }}</span>
                                                                    @break
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    @endif
                                                @endforeach
                                            @else
                                                @if($loop->parent->last)
                                                    @if(!in_array(now()->format('D'), explode(', ', $value->day->eng)) && $top)
                                                        @foreach ($bus->stops()->with('name')->with('day')->get()->sortBy('day.id')->groupBy('day.id')->sortBy('time') as $key => $group)
                                                            @foreach ($group->sortBy('time') as $key => $value)
                                                                <span>В понедельник</span>
                                                                <span class="current-stop">{{ $value->name->title }}</span>
                                                                @if($value->name->description) <span class="gray">{{ $value->name->description }}</span>@endif
                                                                <span class="time">{{ $value->time->format('H:i') }}</span>
                                                                @break
                                                            @endforeach
                                                            @break
                                                        @endforeach
                                                    @endif
                                                @endif
                                            @endif
                                            @break
                                        @endforeach
                                    @endforeach
                                @endif
                            </div>
                            <div class="text-center">
                                <a href="{{ route('bus') }}" class="button btn-outline-blue small mb-4">ПОДРОБНЕЕ</a>
                            </div>
                        </div>
                        <div class="block white mb-4 text-center avia">
                            <div class="header">Авиарейсы</div>
                            <ul>
                                @foreach ($planes_block as $key => $plane)
                                    <li>
                                        <div class="row mb-2">
                                            <div class="col-12 d-flex">
                                                <span class="col text-left departure">{{ $plane->departure }}</span>
                                                <span class="col d-flex justify-content-center"><span class="d-block air">@include('include.plane', ['color' => $plane->status->color])</span></span>
                                                <span class="col text-right arrival">{{ $plane->arrival }}</span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12 d-flex">
                                                <div class="col text-left pr-0 time">@if(now()->format('d.m.Y') == $plane->departure_at->format('d.m.Y')) Сегодня, {{ $plane->departure_at->format('H:i') }} @else {{ $plane->departure_at->isoFormat('Do MMMM, HH:mm') }}@endif</div>
                                                <div class="col text-right pl-0 time">@if(now()->format('d.m.Y') == $plane->arrival_at->format('d.m.Y')) Сегодня, {{ $plane->arrival_at->format('H:i') }} @else {{ $plane->arrival_at->isoFormat('Do MMMM, HH:mm') }}@endif</div>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                            <div class="all"><a href="{{ route('aero') }}">ПОЛНОЕ РАСПИСАНИЕ</a></div>
                        </div>
                    </div>
                </div>
            </div>
            <footer>
                <nav class="footer-nav">
                    <a href="#" class="logotype">Булунский Портал</a>
                    <ul class="navbar-nav d-none d-md-flex">
                        <li class="nav-item @if(Route::is('index'))active @endif"><a href="{{ route('index') }}" class="nav-link">Главная</a></li>
                        <li class="nav-item @if(Route::is('news.*'))active @endif"><a href="{{ route('news.index') }}" class="nav-link">Новости</a></li>
                        <li class="nav-item @if(Route::is('ads.*'))active @endif"><a href="{{ route('ads.index') }}" class="nav-link">Объявления</a></li>
                        <li class="nav-item @if(Route::is('events.*'))active @endif"><a href="{{ route('events.index') }}" class="nav-link">Афиша</a></li>
                        <li class="nav-item @if(Route::is('road'))active @endif"><a href="{{ route('road') }}" class="nav-link">Транспорт</a></li>
                        <li class="nav-item @if(Route::is('forum.*')) active @endif"><a href="{{ route('forum.index') }}" class="nav-link">Форум</a></li>
                        <li class="nav-item @if(Route::is('administration.*')) active @endif"><a href="{{ route('administration.index') }}" class="nav-link">Администрация</a></li>
                        <li class="nav-item @if(Route::is('archive.*')) active @endif"><a href="{{ route('archive.index') }}" class="nav-link">Архив</a></li>
                    </ul>
                </nav>
            </footer>
            <div class="modal fade" id="loginForm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Форма входа</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form method='post' action="{{ route('login') }}">
                                @csrf
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Логин:</label>
                                    <input type="text" class="form-control" name="login">
                                </div>
                                <div class="form-group">
                                    <label for="message-text" class="col-form-label">Пароль:</label>
                                    <input type="password" class="form-control" name="password">
                                </div>
                                <div class="form-group text-right">
                                    <button type="submit" name="button" class="btn btn-primary">Войти</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="fixed-bottom alert-bottom">
                @if(session()->has('success'))
                    <div class="alert alert-success alert-dismissible fade show m-4" role="alert">
                        {!! session()->get('success')!!}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                @if(session()->has('error'))
                    <div class="alert alert-danger alert-dismissible fade show m-4" role="alert">
                        {!! session()->get('error')!!}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                @if ($errors->any())
                <div class="alert alert-warning fade show m-4" role="alert">
                        @foreach ($errors->all() as $error)
                            {{ $error }}
                        @endforeach
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                </div>
                @endif
            </div>
            <script src="{{ mix('/js/app.js') }}"></script>
            @yield('scripts')
        </body>
        </html>
