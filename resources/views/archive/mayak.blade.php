@extends('layouts.app')

@section('title')
    Новости
@endsection

@section('content')
    <div class="d-flex align-items-center justify-content-between">
        <h2>Газета «Маяк Арктики»</h2>
        <div class="d-flex align-items-center change-region">
            <span class="mr-3">Фильтр: </span>
            <div class="btn-group">
                <a href="#" class="dropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    @if(Request::get('asc'))
                        Сначала старые
                    @else
                        Сначала новые
                    @endif
                </a>
                <div class="dropdown-menu">
                    <a class="dropdown-item @if(!Request::get('asc')) active @endif" href="{{ route('archive.mayak') }}">Новые</a>
                    <a class="dropdown-item @if(Request::get('asc')) active @endif" href="{{ route('archive.mayak', ['asc' => 1]) }}">Старые</a>
                </div>

            </div>
        </div>
    </div>
    <div class="row archive">
        @foreach ($mayaks as $key => $mayak)
            <div class="col-md-6 col-12 mb-4">
                <a href="{{ route('archive.show_mayak', ['id' => $mayak->id]) }}">
                    <div class="block white pdf d-flex">
                        <div class="image" style="background-image: url('{{ Storage::url($mayak->image) }}')"></div>
                        <div class="description w-100 pr-3">
                            <div class="title">{{ $mayak->title }}</div>
                            <div class="attr">
                                <span class="time">{{ $mayak->publish_at->format('d.m.Y') }}</span>
                                <span class="tags"><b>Теги:</b> {{ $mayak->tags->implode('title', ', ') }}</span>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        @endforeach
    </div>
@endsection
