@extends('layouts.app')

@section('title')
    Новости
@endsection

@section('content')
    <div class="d-flex align-items-center justify-content-between">
        <h2>{{ $mayak->title }}</h2>
    </div>
    <div class="row">
        <div class="col-12 mb-4">
            <div id="pdf">
                <object data="{{ Storage::url($mayak->file) }}" type="application/pdf" width="100%" height="100%" style="height: 90vh">
                    <p>Ваш браузер не поддерживает PDF, <a href="{{ Storage::url($mayak->file) }}">Скачать выпуск</a>.</p>
                </object>
            </div>
        </div>
    </div>
@endsection
