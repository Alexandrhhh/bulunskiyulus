@extends('layouts.app')

@section('title')
    Новости
@endsection

@section('content')
    <div class="d-flex align-items-center justify-content-between">
        <h2>{{ $section->title }}</h2>
    </div>
    <div id="lightgallery" class="row archive">
			@foreach ($files as $file)
				<a class="col-12 col-md-4 col-sm-6 mb-4" @if($file->type) data-poster="{{ Storage::url($file->preview) }}" data-html="#video{{ $file->id }}" @else href="{{ $file->file }}@endif" data-sub-html="<h4>{{ $file->title }}</h4><p>{{ $file->description }} @if($file->create_day)@if($file->type) Видео@else Снимок@endif от: {{ $file->create_day->format('d.m.Y') }} @endif</p>">
					<div class="file">
						<div class="image" style="background-image:
							@if($file->type)
							url('{{ Storage::url($file->preview) }}')
							@else
							url('{{ $file->file }}')
							@endif
						"></div>
						@if($file->type)
							<img src="{{ Storage::url($file->preview) }}">
							<div class="play-button"></div>
							<span class="duration">{{ floor($file->duration/60) }}:{{ $file->duration%60 }}</span>
						@else
							<img src="{{ $file->file }}">
						@endif
						<span class="title">{{ $file->title }}</span>
					</div>
				</a>
			@endforeach
		</div>
		@foreach ($files as $file)
			@if($file->type)
				<div style="display:none;" id="video{{ $file->id }}">
					<video class="lg-video-object lg-html5" controls="true" preload="none">
						<source src="{{ $file->file }}" type="video/mp4">
						Your browser does not support HTML5 video.
					</video>
				</div>
			@endif
		@endforeach
	{{ $files->links() }}

@endsection

{{-- @section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $("#lightgallery").lightGallery();
    });
</script>
@endsection --}}