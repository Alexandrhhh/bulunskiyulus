@extends('layouts.app')

@section('title')
    Новости
@endsection

@section('content')
    <style>
    .photoLabel .imgWrap {
        display: block;
        position: relative;
        text-align:center;
    }
    .photoLabel .imgWrap .friendDialog {
        display: none;
    }
    .photoLabel .img-overlay {
        background: none repeat scroll 0 0 black;
        cursor: crosshair;
    }
    .photoLabel .img-area {
        cursor: crosshair;
        font-size: 0;
        overflow: hidden;
    }
    .photoLabel .img-area img {
        max-width: 700px;
    }
    .photoLabel .img-cont .rel {
        position: relative;
    }
    .photoLabel .img-taglabel {
        border: medium none;
    }
    .photoLabel .img-taglabel span {
        background: rgba(0,0,0,.5);
        border-radius: 5px;
        border:1px dashed white;
        color: white;
        display: none;
        padding: 5px;
        position: absolute;
        text-align: center;
        width: auto;
        font-size: 12px;
        font-family: 'Golos Text', sans-serif;
        font-weight: 400;
    }
    .photoLabel .img-area .ui-resizable-handle {
        background: none repeat scroll 0 0 white;
        border: medium none;
        box-shadow: 0 0 3px #969696;
        height: 7px;
        opacity: 0.7;
        position: absolute;
        width: 7px;
        z-index: 25;
    }
    .photoLabel .img-area .ui-resizable-handle:hover {
        opacity: 0.9;
    }
    .photoLabel .img-area .ui-resizable-nw {
        left: 0;
        top: 0;
    }
    .photoLabel .img-area .ui-resizable-n {
        left: 50%;
        margin-left: -3px;
        top: 0;
    }
    .photoLabel .img-area .ui-resizable-ne {
        left: 100%;
        margin-left: -7px;
        top: 0;
    }
    .photoLabel .img-area .ui-resizable-w {
        left: 0;
        margin-top: -3px;
        top: 50%;
    }
    .photoLabel .img-area .ui-resizable-e {
        left: 100%;
        margin-left: -7px;
        margin-top: -3px;
        top: 50%;
    }
    .photoLabel .img-area .ui-resizable-sw {
        bottom: 0;
        left: 0;
    }
    .photoLabel .img-area .ui-resizable-s {
        bottom: 0;
        left: 50%;
        margin-left: -3px;
    }
    .photoLabel .img-area .ui-resizable-se {
        bottom: 0;
        left: 100%;
        margin-left: -7px;
    }

    #labelsInfo {
        background: none repeat scroll 0 0 #3F3F3F;
        display: none;
        padding: 10px 0;
        text-align:center;
        color: white;
    }

    #labelsInfo a {
        font-weight:bold;
        color: white;
    }

    .photoLabel .labelBox {
        padding-top: 10px;
        text-align: left;
    }
    .photoLabel .labelList {
        display: none;
    }

    .photoLabel .labels {
        display: inline;
        list-style: none outside none;
        margin: 0;
        padding: 0;
    }
    .photoLabel .labels li {
        display: inline;
    }
    .photoLabel .labels .tag {
        cursor: pointer;
    }
    .photoLabel .labels i {
        font-style: normal;
    }
    .photoLabel .labels .del {
        background: url("../img/close.png") no-repeat scroll center center transparent;
        font-size: 0;
        height: 0;
        padding-left: 15px;
        padding-top: 10px;
        width: 0;
    }
    .photoLabel .recoverTags {
        display: none;
        padding: 7px 0;
    }

    .photoLabel .tag_view {
        border: 2px solid white;
        display: none;
        overflow: hidden;
    }

    .photoLabel .img-view-area {
        position: absolute;
    }


    .friendDialog .findbox {
        background: none repeat scroll 0 0 #eee;
        border-bottom: 1px solid #ACACAC;
        border-top: 1px solid #ACACAC;
        padding: 10px;
    }
    .friendDialog .findbox input {
        width: 97%;
    }
    .friendDialog .userbox {
        list-style: none outside none;
        margin: 0;
        max-height: 200px;
        overflow: auto;
        padding: 0px;
    }
    .friendDialog .userbox li {
    }
    .friendDialog .userbox li a {
        display: block;
        line-height: 18px;
        padding: 3px 10px;
        text-decoration: none;
    }
    .friendDialog .userbox li a:hover {
        background: none repeat scroll 0 0 #E0E9F1;
        color: black;
    }

    .friendDialog .userbox .empty {
        padding:5px 10px;
        text-align:center;
        color: gray;
    }

    .ui-dialog, .ui-dialog .ui-dialog-content {
        overflow: visible;
    }

    .tag_view img {
        max-width: 700px;
    }
    .img-taglabel {
        display: flex;
        align-items: center;
        justify-content: center;
    }

    .img-taglabel:hover {
        border: 2px dashed white;
        border-radius: 2px;
    }
    .ui-dialog-buttonset {
        text-align: center;
    }
    .ui-dialog:focus {
        outline: none;
    }
    .ui-dialog-titlebar {
        cursor: move;
    }


    </style>
    <div class="d-flex align-items-center justify-content-between">
        <h2>Добавление файла</h2>
    </div>

    </video>
    <div class="row add-file">
        <div class="col-12 mb-3 text-center" id="media">
            <form action="" method="post">
                @csrf
                <input type="file" id="file" accept="application/x-troff-msvideo,video/avi,video/msvideo,video/x-msvideo,video/mp4,image/png,image/jpeg,image/pjpeg">
                <label for="file">
                    <i class="icon plus"></i>
                    <span class="name">Добавить фото или видео</span>
                    <span class="format">Формат файла:</span>
                    <span class="formats">AVI, MP4, JPG, PNG</span>
                </label>
            </form>
        </div>
        <div id="block-1" class="photoLabel mx-auto d-none">
            <div class="imgWrap" style="width:700px;"> <!-- Плагин навешивается на этот элемент. Он должен иметь фиксированную ширину и внутреннее выравнивание по центру -->
                <img src="" class="img-fluid">
            </div>

            <!-- Блок со списком отметок -->
            <div class="labelBox">
                <div class="recoverTags"><!-- здесь будет ссылка на восстановление тега --></div>
                <div class="labelList"> <!-- этот блок будет скрываться, если отметок нет -->
                    На этой фотографии:
                    <ul class="labels">
                        <!-- здесь будут перечилены отметки на фото -->
                    </ul>
                </div>
            </div>
            <!-- Конец блока со списком отметок -->
        </div>
        <div class="col-12 mb-4 d-none" id="form">
            <div class="white form">
                <form action="" id="form_data" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <input type="text" class="input" placeholder="Название" name="title">
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col"><input type="text" class="datepicker-here input" name="create_day" placeholder="Дата"></div>
                            <div class="col">
                                <select class="input tags" placeholder="Теги" name="tags[]" multiple="multiple" data-placeholder="Теги">
                                    @foreach ($tags as $key => $tag)
                                        <option value="{{ $tag->title }}">{{ $tag->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col d-none" id="form-preview">
                                <input type="file" name="preview" id="preview" accept="image/png,image/jpeg,image/pjpeg">
                                <label for="preview" class="preview">Превью (обязательно)</label>
                            </div>
                            <div class="col" id="lab">
                                <a href="JavaScript:;" onclick="$('.imgWrap').photoLabel('start')" class="turnOn button btn-outline-blue small w-100 h-100"><i class="icon taggs"></i>Отметить человека</a>
                                <a href="JavaScript:;" onclick="$('.imgWrap').photoLabel('stop')" class="turnOff button btn-outline-blue small w-100 h-100" style="display:none"><i class="icon taggs"></i>Готово</a>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <textarea class="textarea autosize" name="description" placeholder="Описание"></textarea>
                    </div>
                    <div class="form-group">
                        <select class="input dropdown" name="section_id" placeholder="Выберите раздел">
                            @foreach ($sections as $key => $section)
                                <option value="{{ $section->id }}">{{ $section->title }}</option>
                            @endforeach
                        </select>
                    </div>
                    <button class="button small">Отправить</button>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    {{-- <script src="{{ asset('js/photolabel/jquery-1.7.2.min.js') }}"></script> --}}
    {{-- <script src="{{ asset('js/photolabel/jquery-ui-1.8.22.custom.min.js') }}"></script> --}}
    <script src="{{ asset('js/photolabel/jquery.photolabel.js') }}"></script>
<script>
    $(function() {

        $('body').on('change', '#file', function() {
            var file_data = $('#file').prop('files')[0];
            var form_data = new FormData();
            form_data.append('file', file_data);
            var video = ['avi', 'mp4'];
            var image = ['jpeg', 'jpg', 'png'];
        	//отправляем через ajax
            $('#media').html('<div class="spinner-border text-primary d-block mx-auto" role="status">\
                <span class="sr-only">Loading...</span>\
            </div>');
        	setTimeout(function() {
                $.ajax({
            		url: "/api/upload_file",
            		type: "POST",
                    dataType: 'text',
                    cache: false,
                    contentType: false,
                    processData: false,
            		data: form_data, //указываем что отправляем
            		success: function(response){
                        var response = $.parseJSON(response)
                        console.log(response);
                        var format = response.url.split('.').pop();
                        if(video.indexOf(format) !== -1) {
                            $('#media').html('<video src="' + response.url + '" controls width="100%">');
                            $('#form_data').append('<input hidden name="file" type="text" value="' + response.url + '"><input hidden name="type" type="number" value="1"><input hidden name="duration" value="' + response.duration + '">');
                            $('#form').toggleClass('d-none');
                            $('#lab').toggleClass('d-none');
                            $('#form-preview').toggleClass('d-none');
                            $('#preview').prop('required',true);
                        } else if(image.indexOf(format) !== -1) {
                            $('#media').html('');
                            $('#form_data').append('<input hidden name="file" type="text" value="' + response.url + '"><input hidden name="type" type="number" value="0">');
                            $('#form').toggleClass('d-none');
                            $('.imgWrap .img-fluid').attr('src', response.url);
                            $('.photoLabel').toggleClass('d-none');
                            $('.imgWrap').photoLabel({
                                onStart: function() { //Обработчик на событие - "start" (начало процедуры отметок на фото)
                                    $('.turnOn').hide();
                                    $('.turnOff').show();
                                    $('.info').show();
                                },
                                onStop: function() { //Обработчик на событие - "stop" (завершение процедуры отметок на фото)
                                    $('.turnOn').show();
                                    $('.turnOff').hide();
                                    $('.info').hide();
                                },
                                recoverContainer: '.recoverTags', //Контейнер для ссылки "восстановить тег"
                                labelListContainer: '.labelList', //Общий контейнер, для списка отметок.
                                labelContainer: '.labels', //UL контейнер для списка отметок

                                addTagUrl: "{{ route('add_tag') }}", //Адрес скрипта, который будет вызван при добавлении метки
                                removeTagUrl: "{{ route('remove_tag') }}", //Адрес скрипта, который будет вызван при удалении метки
                                recoverTagUrl: "php/tags.php?Act=recoverTag", //Адрес скрипта, который будет вызван при восстановлении метки

                                friends: {
                                    @foreach ($humans as $key => $human)
                                        "{{ $key }}": {id: {{ $human->id }}, fullname:"{{ $human->title }}", url: null},
                                    @endforeach
                                },
                                isAdmin: 1, //1 - если фото просматривает модератор, 0 - если обычный пользователь
                                viewerId: -1, //id текущего пользователя в вашей системе
                                areas: //Список отмеченных областей на изображении
                                []
                            });
                        } else {
                            console.log('error format');
                            return false;
                        }
                        $('#form').show(300);
            		},
                    fail: function(xhr, textStatus, errorThrown){
                       alert('Ошибка при загрузке, гляди консоль!');
                       $('#media').html('<form action="" method="post">\
                           @csrf\
                           <input type="file" id="file" accept="application/x-troff-msvideo,video/avi,video/msvideo,video/x-msvideo,video/mp4,image/png,image/jpeg,image/pjpeg">\
                           <label for="file">\
                               <i class="icon plus"></i>\
                               <span class="name">Добавить фото или видео</span>\
                               <span class="format">Формат файла:</span>\
                               <span class="formats">AVI, MP4, JPG, PNG</span>\
                           </label>\
                       </form>');
                    }
            	});
            }, 500);

        	return false;
        });

        $('body').on('change', '#preview', function() {
            $(this).next('label').addClass('active');
            $(this).next('label').html(document.getElementById('preview').files[0].name);
        })

    });
</script>
@endsection
