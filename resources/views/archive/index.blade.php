@extends('layouts.app')

@section('title')
    Новости
@endsection

@section('content')
    <div class="d-flex align-items-center justify-content-between">
        <h2>Архив</h2>
    </div>
    <div class="row archive">
        @foreach ($sections as $key => $section)
            <div class="col-6 mb-4">
                <a href="{{ route('archive.section', ['id' => $section->id]) }}">
                    <div class="block white big-img">
                        <div class="image" style="background-image: url('{{ Storage::url($section->image) }}')">
                        </div>
                        <span class="title">
                            {{ $section->title }}
                        </span>
                    </div>
                </a>
            </div>
        @endforeach
        <div class="col-12 text-center">
            <a href="{{ route('archive.add') }}" class="button small">Добавить</a>
        </div>
        <div class="col-12">
            <hr class="my-5">
        </div>
        <div class="col-12 mb-4">
            <div class="block mayak d-flex justify-content-center justify-content-md-start">
                <div class="d-flex flex-column text-center text-md-left">
                    <span class="title">Архив газеты</span>
                    <div class="mayak-logo"></div>
                    <a href="{{ route('archive.mayak') }}" class="button btn-outline-blue">СМОТРЕТЬ АРХИВ</a>
                </div>
            </div>
        </div>
    </div>
@endsection
