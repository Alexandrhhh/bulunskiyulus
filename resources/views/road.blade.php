\@extends('layouts.app')

@section('title')
    Новости
@endsection

@section('content')
    <div class="d-flex align-items-center justify-content-between">
        <h2>Состояние дорог</h2>
    </div>
    <div class="row road mb-4">
        <div class="col-12">
            <div class="table-responsive">
                <table class="table table-road white">
                    <thead>
                        <tr>
                            <th>Дорога</th>
                            <th>Открытие автозимника</th>
                            <th>Закрытие автозимника</th>
                            <th>Максимальный тоннаж</th>
                            <th>Дорожная техника</th>
                            <th>Следующая чистка</th>
                            <th>Особености</th>
                            <th>Треки</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($roads as $key => $road)
                            <tr>
                                <td @if($road->color == 0) class="green" @else class="red" @endif>{{ $road->title }}</td>
                                <td>@if($road->open_date) {{ $road->open_date->format('d.m.y') }} @else - @endif</td>
                                <td>@if($road->close_date) {{ $road->close_date->format('d.m.y') }} @else - @endif</td>
                                <td>@if($road->max_tonns){{ $road->max_tonns }} @else - @endif</td>
                                <td>@if($road->technics){{ $road->technics }} @else - @endif</td>
                                <td>@if($road->next_clean) {{ $road->next_clean->format('d.m.y') }} @else - @endif</td>
                                <td>@if($road->about){{ $road->about }} @else - @endif</td>
                                <td>@if($road->tracks){{ $road->tracks }} @else - @endif</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
