@extends('layouts.app')

@section('title')
    Баланс - Личный кабинет - Булунский Портал
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="white p-4">
                @include('include._lk_navigate')
                <div class="d-flex justify-content-between align-items-center mb-4">
                    <div>
                        <span class="current_balance mb-1">Текущий баланс</span>
                        <span class="current_balance_sum"><b>{{ str_replace(' Руб.', '', Auth::user()->ballance) }}</b> руб.</span>
                    </div>
                    <div>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                          Пополнить
                        </button>

                        <!-- Modal -->
                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Пополнение счёта</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <form action="{{ route('lk.yandex_pay') }}" method="post">
                                @csrf
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label>Сумма пополнения</label>
                                        <input type="number" name="sum" min="1" step="1" class="form-control" placeholder="Укажите сумму пополнения">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary">Пополнить</button>
                                </div>
                               </form>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
                <p>График платежей</p>
                <div class="row">
                    <div class="col-8">
                        <table class="table table-sm table-striped table-grey">
                            <thead>
                                <tr>
                                    <th>Дата</th>
                                    <th>Сумма</th>
                                    <th>Описание</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach (Auth::user()->moneys()->sortByDesc('moneytime') as $key => $money)
                                    <tr>
                                        <td>{{ date('d.m.Y', $money->moneytime)}}</td>
                                        <td @if($money->cash > 0) class="green" @elseif($money->cash < 0) class="red" @endif>{{$money->cash}}</td>
                                        <td>{{ $money->value1 }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
