@extends('layouts.app')

@section('title')
    Тариф - Личный кабинет - Булунский Портал
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div>
                @include('include._lk_navigate')
                <div class="d-flex justify-content-between align-items-center mb-4">
                    <div>
                        <span class="current_balance mb-1">Ваш текущий тариф</span>
                        <span class="current_balance_sum"><b>{{Auth::user()->tarif}}</b></span>
                    </div>
                </div>
                @if(!empty($tariffs))
                    <p>Тарифы доступные для подключения</p>
                    <div class="row">
                        @foreach ($tariffs as $key => $tarif)
                            <div class="col-4">
                                <form action="{{ route('lk.change_tariff') }}" method="post">
                                    @csrf
                                    <div class="white p-4">
                                        <input type="hidden" value="{{ $tarif->tarif_name }}" name="tarif">
                                        <p class="text-center">{{ $tarif->tarif_name }}</p>
                                        <small class="mb-3 text-center d-block">{{ $tarif->tarifdescr }}</small>
                                        <button type="submit" class="mx-auto d-block btn btn-sm btn-primary"  onclick="return confirm('Вы действительно хотите изменить тариф на {{ $tarif->tarif_name }}?')">Подключить</button>
                                    </div>
                                </form>
                            </div>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
    var ctx = document.getElementById('myChart').getContext('2d');
    var gradientStroke = ctx.createLinearGradient(0, 0, 0, 500);
        gradientStroke.addColorStop(0, "#c3eaff");
        gradientStroke.addColorStop(1, "#93d8ff");
    var chart = new Chart(ctx, {
        // The type of chart we want to create
         type: 'bar',
        axisY: {
            title: "Views on YouTube",
        },
        // The data for our dataset
        data: {
            labels: ['1.01', '2.01', '3.01', '4.01', '5.01', '6.01', '7.01', '8.01', '9.01', '10.01'],
        datasets: [{
            label: 'ГБ',
            data: [12, 19, 3, 5, 2, 3, 33, 14, 13, 30],
            backgroundColor: gradientStroke,
            borderColor: gradientStroke,
            borderWidth: 1
        }]
        },

        // Configuration options go here
        options: {
            scales: {
                yAxes: [{
                    label: "Views on YouTube",
                    ticks: {
                        beginAtZero: true,
                        // Include a dollar sign in the ticks
                        callback: function(value, index, values) {
                            return value + ' ГБ';
                        }
                    }
                }]
            }
        }
    });
    </script>
@endsection
