@extends('layouts.app')

@section('title')
    Объявления пользователя {{ Auth::user()->name }} - Булунский Портал
@endsection

@section('content')
<div class="row">
    <div class="col-lg-9 col-sm-12 content order-sm-1 order-2">
        @include('include._user_navigation')
        <div class="d-flex justify-content-between align-items-center">
            <h2 class="mt-3 mb-3 font-size-36px">Объявления</h2>
            <div>
                @if(Auth::user()->hasPermission('ads.add'))
                    <a href="{{ route('ads.add') }}" class="btn btn-sm btn-primary" role="button">Добавить</a>
                @endif
            </div>
        </div>
        <div class="items">
            @if($ads->count() == 0)
                Объявлений нет
            @else
                @foreach ($ads as $key => $ad)
                    <div class="item row p-2">
                        <div class="col-sm-3 p-0">
                            @foreach ($ad->images as $key => $image)
                                @if($loop->first)
                                    <img src="{{Storage::url($image->url)}}" class="object-fit-cover" alt="">
                                @endif
                            @endforeach
                        </div>
                        <div class="col-sm-9 right_item">
                            <div class="d-flex justify-content-between">
                                <h4><a href="{{route('ads.show', ['slug' => $ad->slug])}}">{{$ad->title}}</a></h4>
                                @if(Auth::check() && (Auth::user()->id == $ad->user_id || Auth::user()->group_id == 2))
                                    <div>
                                        <a href="{{ route('ads.edit', ['id' => $ad->id]) }}" class="badge badge-pill badge-success">Редактировать</a>
                                        <a href="#" class="badge badge-pill badge-danger" onclick="if(confirm('Удалить?')) {document.getElementById('ads-delete-{{$ad->id}}').submit()}; return false;">Редактировать</a>
                                        <form action="{{route('ads.delete', ['id' => $ad->id])}}" method="post" class="d-none" id="ads-delete-{{$ad->id}}">
                                            @csrf
                                            @method('delete')
                                            <button type="submit" name="button"></button>
                                        </form>
                                    </div>
                                @endif
                            </div>
                            <p class="mb-1">{{str_limit($ad->text, 200)}}</p>
                            <div class="price">{{number_format($ad->price, 0, '.', ' ')}} <span>руб.</span></div>
                            <div class="elements mt-2 border-top-none">
                                <span class="share d-flex align-items-center">
                                    <i class="icon star"></i>
                                    <i class="icon sharing"></i>
                                    <a href="{{route('ads.show', ['slug' => $ad->slug])}}" class="ml-4 link">Подробнее</a>
                                </span>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
        {{ $ads->links() }}
    </div>
</div>
@endsection
