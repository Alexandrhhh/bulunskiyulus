@extends('layouts.app')

@section('title')
    Личный кабинет - Булунский Портал
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="white p-4">
                @include('include._lk_navigate')
                <form action="{{ route('lk.update') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('put')
                    <div class="form-group d-flex">
                        <label for="" class="col-3 col-form-label">Фамилия</label>
                        <input type="text" name="last_name" class="form-control col-9" placeholder="Укажите вашу фамилию" value="{{ Auth::user()->last_name }}" required>
                    </div>
                    <div class="form-group d-flex">
                       <label for="" class="col-3 col-form-label">Имя</label>
                       <input type="text" name="first_name" class="form-control col-9"  placeholder="Укажите ваше имя" value="{{ Auth::user()->first_name }}" required>
                   </div>
                   <div class="form-group d-flex">
                       <label for="" class="col-3 col-form-label">Отчество</label>
                       <input type="text" name="middle_name" class="form-control col-9" placeholder="Укажите ваше отчество" value="{{ Auth::user()->middle_name }}">
                   </div>
                   <div class="form-group d-flex">
                       <label for="" class="col-3 col-form-label">Дата рождения</label>
                       <input class="form-control col-9 datepicker-here" name="date_birth" placeholder="Укажите дату рождения"  @if(Auth::user()->date_birth)value="{{ Auth::user()->date_birth->format('d.m.Y') }}"@endif>
                   </div>
                   @if(Auth::user()->photo)<img src="{{ Storage::url(Auth::user()->photo) }}" style="max-width: 150px" alt="">@endif
                   <div class="form-group d-flex">
                       <label for="" class="col-3 col-form-label">Аватар</label>
                       <input type="file" name="photo_id" accept="image/*, image/jpeg">
                   </div>
                   <button type="submit" class="btn btn-primary">Сохранить</button>
               </form>
            </div>
        </div>
    </div>
@endsection
