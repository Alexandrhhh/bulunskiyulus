@extends('layouts.app')

@section('title')
    Документы - Личный кабинет - Булунский Портал
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-9 col-sm-12 content order-sm-1 order-2">
            <div class="row">
                <div class="col-12">
                    <div class="white p3035">
                        <p>Мои документы</p>
                        <div class="row">
                            <div class="col-12">
                                <table class="table table-sm table-striped table-grey">
                                    <thead>
                                        <tr>
                                            <th>Название документа</th>
                                            <th>Дата документа</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="align-middle">Выписка по тарифам длинное название</td>
                                            <td class="align-middle">10.10.2019</td>
                                            <td class="text-right">
                                                <a href="#" class="btn btn-light btn-sm"><span class="oi oi-data-transfer-download"></span></a>
                                                <a href="#" class="btn btn-light btn-sm"><span class="oi oi-x"></span></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="align-middle">Выписка по тарифам длинное название</td>
                                            <td class="align-middle">10.10.2019</td>
                                            <td class="text-right">
                                                <a href="#" class="btn btn-light btn-sm"><span class="oi oi-data-transfer-download"></span></a>
                                                <a href="#" class="btn btn-light btn-sm"><span class="oi oi-x"></span></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="align-middle">Выписка по тарифам длинное название</td>
                                            <td class="align-middle">10.10.2019</td>
                                            <td class="text-right">
                                                <a href="#" class="btn btn-light btn-sm"><span class="oi oi-data-transfer-download"></span></a>
                                                <a href="#" class="btn btn-light btn-sm"><span class="oi oi-x"></span></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="align-middle">Выписка по тарифам длинное название</td>
                                            <td class="align-middle">10.10.2019</td>
                                            <td class="text-right">
                                                <a href="#" class="btn btn-light btn-sm"><span class="oi oi-data-transfer-download"></span></a>
                                                <a href="#" class="btn btn-light btn-sm"><span class="oi oi-x"></span></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="align-middle">Выписка по тарифам длинное название</td>
                                            <td class="align-middle">10.10.2019</td>
                                            <td class="text-right">
                                                <a href="#" class="btn btn-light btn-sm"><span class="oi oi-data-transfer-download"></span></a>
                                                <a href="#" class="btn btn-light btn-sm"><span class="oi oi-x"></span></a>
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
    var ctx = document.getElementById('myChart').getContext('2d');
    var gradientStroke = ctx.createLinearGradient(0, 0, 0, 500);
        gradientStroke.addColorStop(0, "#c3eaff");
        gradientStroke.addColorStop(1, "#93d8ff");
    var chart = new Chart(ctx, {
        // The type of chart we want to create
         type: 'bar',
        axisY: {
            title: "Views on YouTube",
        },
        // The data for our dataset
        data: {
            labels: ['1.01', '2.01', '3.01', '4.01', '5.01', '6.01', '7.01', '8.01', '9.01', '10.01'],
        datasets: [{
            label: 'ГБ',
            data: [12, 19, 3, 5, 2, 3, 33, 14, 13, 30],
            backgroundColor: gradientStroke,
            borderColor: gradientStroke,
            borderWidth: 1
        }]
        },

        // Configuration options go here
        options: {
            scales: {
                yAxes: [{
                    label: "Views on YouTube",
                    ticks: {
                        beginAtZero: true,
                        // Include a dollar sign in the ticks
                        callback: function(value, index, values) {
                            return value + ' ГБ';
                        }
                    }
                }]
            }
        }
    });
    </script>
@endsection
