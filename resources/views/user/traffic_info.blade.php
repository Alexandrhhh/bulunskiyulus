@extends('layouts.app')

@section('title')
    Трафик - Личный кабинет - Булунский Портал
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="white p-4">
                @include('include._lk_navigate')
                <div class="d-flex justify-content-between align-items-center mb-4">
                    <div>
                        <span class="current_balance mb-1">Расход трафика в текущем месяце</span>
                        <span class="current_balance_sum mb-3"><b>{{ round((float)explode(' / ', explode('||', Auth::user()->otherinfo)[18])[0]) }}</b> МБ, лимит
                            @if(strlen(explode('||', Auth::user()->otherinfo)[62]) > 0)
                                <b>{{str_replace(' Мб.', '', explode('||', Auth::user()->otherinfo)[62])}}</b> МБ.
                            @endif
                        </span>
                        <span class="current_balance mb-1">Расход за сутки</span>
                        <span class="current_balance_sum"><b>{{round((float)explode(' / ', Auth::user()->todaytraffic)[0]/1048576)}}</b> МБ</span>
                    </div>
                </div>
                <p>Трафик по дням</p>
                <div class="row">
                    <div class="col-12 d-flex">
                        <form action="" id="form-date">
                            <div class="datepicker-date-afisha-icon">
                                <input type="text" name="date" data-range="true" data-multiple-dates-separator=" - " class="datepicker-here date form-control" value="{{Request::get('date')}}" placeholder="Выберите дату"/>
                            </div>
                            <button style="display:none;" type="submit" id="submit-date"></button>
                        </form>
                    </div>
                    <div class="col-12">
                        <canvas id="myChart"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
    <script>
    var ctx = document.getElementById('myChart').getContext('2d');
    var gradientStroke = ctx.createLinearGradient(0, 0, 0, 500);
        gradientStroke.addColorStop(0, "#c3eaff");
        gradientStroke.addColorStop(1, "#93d8ff");
    var chart = new Chart(ctx, {
        // The type of chart we want to create
         type: 'bar',
        axisY: {
            title: "Views on YouTube",
        },
        // The data for our dataset
        data: {
            labels: [ {!! '\''.implode('\', \'', $traffics['dates']).'\'' !!}],
        datasets: [{
            label: 'МБ',
            data: [ {{ implode(', ', $traffics['traff']) }} ],
            backgroundColor: gradientStroke,
            borderColor: gradientStroke,
            borderWidth: 1
        }]
        },

        // Configuration options go here
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        // Include a dollar sign in the ticks
                        callback: function(value, index, values) {
                            return value + ' МБ';
                        }
                    }
                }]
            }
        }
    });
    </script>
@endsection
