@extends('layouts.app')

@section('title')
    Главная страница
@endsection

@section('content')
    <div class="row mb-2">
        <div class="col-md-4 col-12 order-md-0 order-1">
            <div class="block d-flex flex-column full white mb-4">
                <div class="header text-center">
                    Новости района
                </div>
                <div class="d-flex flex-grow-1 flex-column">
                    @foreach ($newses_bulunsk as $key => $news)
                        <div class="news flex-grow-1 d-flex align-items-center justify-content-start py-0 px-3">
                            <a href="{{ route('news.news', ['id' => $news->id]) }}">{{ $news->title }}</a>
                        </div>
                    @endforeach
                </div>
                <div class="all"><a href="{{ route('news.index') }}">ВСЕ НОВОСТИ</a></div>
            </div>
        </div>
        <div class="col-md-8 col-12 order-md-1 order-0">
            <div class="row">
                @foreach ($newses as $key => $news)
                    @if($loop->first)
                        <div class="col-12 mb-4">
                            <div class="news big white">
                                <div class="top" style="background-image: url('{{ Storage::url($news->image_url) }}')">
                                    <div class="content">
                                        <div class="text">
                                            <span class="category {{ $news->category->color->eng }}">{{ $news->category->title }}</span>
                                            <h3><a href="{{ route('news.news', ['id' => $news->id]) }}">{{ $news->title }}</a></h3>
                                        </div>
                                    </div>
                                    <a href="#" class="share"></a>
                                </div>
                                <div class="bottom justify-content-between align-items-center">
                                    <span>{{ $news->created_at->format('d.m.Y H:i') }}</span>
                                    <span class="d-flex align-items-center">
                                        <i class="icon read"></i>{{ $news->reads }} <i class="icon comments ml-4"></i>0
                                    </span>
                                    <span>Андрей Иванов</span>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="col-sm-6 col-12 mb-4">
                            <div class="news white">
                                <div class="top" style="background-image: url('{{ Storage::url($news->image_url) }}')">
                                    <div class="content">
                                        <div class="text">
                                            <span class="category {{ $news->category->color->eng }}">{{ $news->category->title }}</span>
                                            <h3><a href="{{ route('news.news', ['id' => $news->id]) }}">{{ $news->title }}</a></h3>
                                        </div>
                                    </div>
                                    <a href="#" class="share"></a>
                                </div>
                                <div class="bottom justify-content-between align-items-center">
                                    <span>{{ $news->created_at->format('d.m.Y H:i') }}</span>
                                    <span class="d-flex align-items-center">
                                        <i class="icon comments"></i>0
                                    </span>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
    <div class="row mb-5">
        <div class="col-12">
            <div class="white block forum">
                <div class="header d-flex justify-content-between align-items-center">
                    <span class="title">Обсуждаемые темы</span>
                    <a href="{{ route('forum.add') }}" class="create">Создать тему</a>
                </div>
                <div class="body">
                    <div class="row p-3 m-0">
                        @foreach ($topics as $key => $topic)
                            <div class="col-lg-4 col-md-6 col-12 my-3">
                                <div class="topic p-0">
                                    <div class="image">
                                        @if($topic->last)
                                            <img src="@if($topic->last->user && $topic->last->user->photo){{ Storage::url($topic->last->user->photo) }}@else{{ asset('images/noavatar.jpg') }}@endif" alt="">
                                        @else
                                            <img src="@if($topic->user && $topic->user->photo){{ Storage::url($topic->user->photo) }}@else{{ asset('images/noavatar.jpg') }}@endif" alt="">
                                        @endif
                                    </div>
                                    <div class="column">
                                        <h4 class="title"><a href="{{ route('forum.topic', ['id' => $topic->id]) }}">{{ $topic->title }}</a></h4>
                                        <span class="time">@if($topic->last){{ $topic->last->created_at->diffForHumans() }}@else{{ $topic->created_at->diffForHumans() }}@endif</span>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="all"><a href="{{ route('forum.index') }}">ВСЕ ТЕМЫ</a></div>
            </div>
        </div>
    </div>
    <div class="row mb-4">
        <div class="col-12 mb-3">
            <div class="d-flex justify-content-between align-items-center">
                <h3>Киноафиша</h3>
                <a href="{{ route('afisha') }}">Все фильмы</a>
            </div>
        </div>
        <div class="col-12">
            <div class="row afisha">
                @foreach ($films as $key => $film)
                    <div class="col-sm col-12 film mb-4">
                        <div class="image" style="background-image: url('{{ Storage::url($film->image) }}')"></div>
                        <h4 class="title">{{ $film->title }}</h4>
                        <span class="time">{{ Carbon\CarbonInterval::minutes($film->duration)->cascade()->forHumans() }}</span>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="row mb-5">
        <div class="col-12 mb-3">
            <div class="d-flex justify-content-between align-items-center">
                <h3>Объявления</h3>
                <a href="{{ route('ads.index') }}">Все объявления</a>
            </div>
        </div>
        <div class="col-12">
            <div class="row ads">
                @foreach ($ads as $key => $ad)
                    <div class="col-sm col-12 ">
                        <div class="ad white">
                            <div class="image" style="
                            @foreach ($ad->photos as $key => $photo)
                            @if($loop->first)
                            background-image: url('{{ Storage::url($photo->url) }}')
                            @endif
                            @endforeach
                            "></div>
                            <div class="content">
                                <h4 class="title">{{ $ad->title }}</h4>
                                <span class="price">{{ number_format($ad->price, 0, '.', ' ') }}₽</span>
                                <p>
                                    {{ Str::limit($ad->text, 100, '...') }}
                                </p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
