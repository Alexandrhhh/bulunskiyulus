@extends('layouts.closed')

@section('title')
    Страница входа
@endsection

@section('content')
    <div class="row">
        <div class="col-12 h-100">
            <h1>Добро пожаловать в сеть</h1>
            <h2>«Единый булун»</h2>
            <div class="vhod">
                <input type="text" placeholder="Фамилия">
                <input type="text" placeholder="Имя">
                <input type="text" placeholder="Отчество">
                <input type="text" class="phone" placeholder="Номер телефона">
                <div class="d-flex">
                    <input type = "checkbox" id="oferta">
                    <label for = "oferta">Я принимаю условия договора оферты</label>
                </div>
                <button class="button small">ЗАРЕГИСТРИРОВАТЬСЯ</button>
            </div>
        </div>
    </div>
@endsection
