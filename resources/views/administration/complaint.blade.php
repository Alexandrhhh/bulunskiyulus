@extends('layouts.app')

@section('title')
    Новости
@endsection

@section('content')
    <div class="d-flex align-items-center mb-3">
        <h2 class="m-0">Жалобы и предложения</h2>
    </div>
    <div class="row mb-4">
        <div class="col-12">
            <div class="white complaint-form">
                <div class="img"></div>
                <div class="content">
                    <h3>Заполните форму и мы рассмотрим вашу заявку</h3>
                    <form action="{{ route('administration.mail_to_administration') }}" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-12 mb-4">
                                <input type="text" name="theme" placeholder="Тема вашей заявки" required>
                            </div>
                            <div class="col-12 mb-4">
                                <div class="row">
                                    <div class="col-4">
                                        <input type="text" class="phone" name="phone" placeholder="Номер телефона" required>
                                    </div>
                                    <div class="col-4">
                                        <input type="email" name="email" placeholder="Email">
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 mb-4">
                                <input type="text" name="name" placeholder="Имя" required>
                            </div>
                            <div class="col-6 mb-4">
                                <input type="text" name="surname" placeholder="Фамилия" required>
                            </div>
                            <div class="col-12 mb-4">
                                <textarea class="autosize" name="message" rows="8" placeholder="Подробное описание" required></textarea>
                            </div>
                            <div class="col-12">
                                <button type="submit" name="button" class="button blue">Отправить</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
