@extends('layouts.app')

@section('title')
    {{ $measure->title }}
@endsection

@section('content')
    <div class="fullnews white @if($measure->files) mb-3 @else mb-5 @endif">
        <div class="header" style="background-image: url('{{ Storage::url($measure->image) }}')">
            <div class="absolute">
                <div class="info d-flex align-items-center">
                    <span class="date mr-3">{{ $measure->created_at->format('d.m.Y') }}</span>
                    <span class="time">{{ $measure->created_at->format('H:i') }}</span>
                </div>
                <h1 class="little">
                    {{ $measure->title }}
                </h1>
                <hr>
                <div class="stats d-flex align-items-center">
                    <span class="mr-3 d-flex align-items-center"><i class="icon read min"></i>{{ $measure->reads }}</span>
                    <span class="d-flex align-items-center"><i class="icon profile min"></i>@if($measure->user){{ $measure->user->first_name }} {{ $measure->user->last_name }}@endif</span>
                </div>
            </div>
            <a href="#"class="share"></a>
        </div>
        <div class="content">
            {!! html_entity_decode($measure->content) !!}
            <div class="info d-flex align-items-center justify-content-between">
                <div class="profile">
                    <div class="img">
                        <img src="@if($measure->user && $measure->user->photo){{ Storage::url($measure->user->photo) }}@else{{ asset('images/noavatar.jpg') }}@endif" alt="">
                    </div>
                    <span class="name">@if($measure->user){{ $measure->user->first_name }} {{ $measure->user->last_name }}@endif</span>
                </div>
                <div class="d-flex align-items-center">
                    <i class="icon like @auth click @if($like) active @endif @endauth" @auth data-class="{{ get_class($measure) }}" data-id="{{ $measure->id }}" data-user="{{ Auth::user()->id }}" @endauth></i><span>{{ $measure->likes->count() }}</span>
                    <i class="icon read min gray ml-4"></i>{{ $measure->reads }}
                </div>
            </div>
        </div>
    </div>
    @if($measure->files)
        <div class="white mb-4 poll">
            <div class="header">
                <div class="title">
                    Файлы
                </div>
            </div>
            <div class="content">
                <ul class="list-group">
                  @foreach ($measure->files as $key => $file)
                      <li class="list-group-item m-0"><img src="/images/filetype/{{ $file->extension }}.svg" style="max-width: 30px; margin-right: 10px;" alt="{{ $file->extension }}"><a href="{{ route('download_file', ['id' => $file->id]) }}">{{ $file->title }}</a></li>
                  @endforeach
                </ul>
            </div>
        </div>
    @endif
@endsection
