@extends('layouts.app')

@section('title')
    Новости
@endsection

@section('content')
    <div class="d-flex align-items-center mb-3">
        <h2 class="m-0">Администрация</h2>
    </div>
    <div class="row administration">
        <div class="col-6 col-md-3 mb-4 hover">
            <a href="#">
                <div class="white block">
                    <div class="content">
                        <i class="icon video"></i>
                    </div>
                </div>
            </a>
            <div class="d-block px-1">
                <a href="#">Официальный видео канал МО “Булунский улус(район)”</a>
            </div>
        </div>
        <div class="col-6 col-md-3 mb-4 hover">
            <a href="{{ route('administration.complaint') }}">
                <div class="white block">
                    <div class="content">
                        <i class="icon complaint"></i>
                    </div>
                </div>
            </a>
            <div class="d-block px-1">
                <a href="{{ route('administration.complaint') }}">Подача жалоб и предложений</a>
            </div>
        </div>
        <div class="col-6 col-md-3 mb-4 hover">
            <a href="{{ route('administration.measure.index') }}">
                <div class="white block">
                    <div class="content">
                        <i class="icon measures"></i>
                    </div>
                </div>
            </a>
            <div class="d-block px-1">
                <a href="{{ route('administration.measure.index') }}">Меры поддержки населения и организации</a>
            </div>
        </div>
        <div class="col-6 col-md-3 mb-4 hover">
            <a href="#">
                <div class="white block">
                    <div class="content">
                        <i class="icon copy-site"></i>
                    </div>
                </div>
            </a>
            <div class="d-block px-1">
                <a href="#">Копия муниципального сайта</a>
            </div>
        </div>
    </div>
@endsection
