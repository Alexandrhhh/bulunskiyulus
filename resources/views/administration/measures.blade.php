@extends('layouts.app')

@section('title')
    Муниципальные программы
@endsection

@section('content')
    <div class="d-flex align-items-center justify-content-between">
        <h2>Муниципальные программы</h2>
    </div>
    <div class="row news">
        <div class="col-12">
            <div class="row">
                @if($measures->count() == 0)
                    <p>Муниципальных программ нет</p>
                @else
                    @foreach ($measures as $key => $measure)
                        <div class="block flex-sm-row flex-column d-flex white">
                            <div class="image p-3" style="background-image: url('{{ Storage::url($measure->image) }}')">
                            </div>
                            <div class="content">
                                <div class="title">
                                    <a href="{{ route('administration.measure.show', ['id' => $measure->id]) }}">{{ $measure->title }}</a>
                                </div>
                                <div class="short">
                                    {{ $measure->short }}
                                </div>
                                <div class="info d-flex justify-content-between align-items-center py-3">
                                    <span class="time">{{ $measure->created_at->format('d.m.Y в H:i') }}</span>
                                    <div class="d-flex stats align-items-center">
                                        <i class="icon read"></i>{{ $measure->read }}<i class="icon comments ml-4"></i>0
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
@endsection
