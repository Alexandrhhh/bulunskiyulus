@extends('layouts.app')

@section('title')
    Новости
@endsection

@section('content')
    <div class="d-flex align-items-end mb-3 justify-content-between">
        <h2 class="m-0">Список объявлений</h2>
        <div class="breadcrumb">
            <a href='{{ route('index') }}'>Главная</a> / <a href='{{ route('ads.index') }}'>Объявления</a> / <span class="current">{{ $category->title }}</span>
        </div>
    </div>
    <div class="row ads">
        <div class="col-12">
            @foreach ($ads as $key => $ad)
                <div class="block d-flex white">
                    @foreach ($ad->photos as $key => $photo)
                        @if($loop->first)
                            <div class="image" style="background-image: url({{ Storage::url($photo->url) }})">
                        @endif
                    @endforeach
                    </div>
                    <div class="content">
                        <div class="title">
                            <a href="{{ route('ads.ad', ['id' => $ad->id]) }}">{{ $ad->title }}</a>
                        </div>
                        <div class="short">
                            {{ Str::limit($ad->text, 200, '...') }}
                        </div>
                        <div class="info d-flex justify-content-between align-items-center py-3">
                            <span class="time">{{ $ad->created_at->format('d.m.Y в H:i') }}</span>
                            <div class="d-flex stats align-items-center">
                                <a href="{{ route('ads.ad', ['id' => $ad->id]) }}">Подробнее</a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    {{ $ads->links() }}
@endsection
