@extends('layouts.app')

@section('title')
    Новость
@endsection

@section('content')
    <div class="fullad mb-4">
        <div class="d-flex align-items-end mb-3 justify-content-between">
            <h2 class="m-0">{{ $ad->title }}</h2>
            <div class="breadcrumb">
                <a href='{{ route('index') }}'>Главная</a> / <a href='{{ route('ads.index') }}'>Объявления</a> / <span class="current">{{ $ad->category->title }}</span>
            </div>
        </div>
        <div id="ad" class="carousel slide mb-4" data-ride="carousel">
            <ol class="carousel-indicators">
                @foreach ($ad->photos as $key => $photo)
                    <div class="carousel-item @if($loop->first) active @endif">
                        <li data-target="#carouselExampleIndicators" data-slide-to="{{ $key }}" @if($loop->first) class="active" @endif></li>
                    </div>
                @endforeach
            </ol>
            <div class="carousel-inner">
                @foreach ($ad->photos as $key => $photo)
                    <div class="carousel-item @if($loop->first) active @endif">
                        <img src="{{ Storage::url($photo->url) }}" class="d-block mx-auto" alt="...">
                    </div>
                @endforeach
            </div>
            <a class="carousel-control-prev" href="#ad" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#ad" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <div class="white content">
            <p>{{ $ad->text }}</p>
            <div class="info d-flex align-items-center justify-content-between border-bottom-0">
                <a href="#showphone" class="button small red">Показать телефон</a>
                <div class="d-flex align-items-center">
                    <i class="icon like @auth click @if($like) active @endif @endauth" data-class="{{ get_class($ad) }}" data-id="{{ $ad->id }}"></i><span>{{ $ad->likes->count() }}</span>
                    <i class="icon read min gray ml-4"></i>{{ $ad->reads }}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(function() {
            $('body').on('click', 'a[href="#showphone"]', function() {
                $(this).attr('href', 'tel:{{ $ad->phone }}');
                $(this).text('{{ $ad->phone }}');
            })
        })
    </script>
@endsection
