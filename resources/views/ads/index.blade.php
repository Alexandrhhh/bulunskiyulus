@extends('layouts.app')

@section('title')
    Новости
@endsection

@section('content')
    <div class="d-flex align-items-center mb-3">
        <h2 class="m-0">Доска объявлений</h2>
        @auth <a href="{{ route('ads.add') }}" class="button small red ml-4">Подать объявление</a> @endauth
    </div>
    <div class="row ads">
        @foreach ($categories as $key => $category)
            <div class="col-sm-6 col-12 mb-4">
                <div class="d-flex align-items-center title">
                    <h3>{{ $category->title }}</h3>
                    <a href="{{ route('ads.category', ['category_id' => $category->id]) }}">Еще {{ $category->ads->count() }} объявлений</a>
                </div>
                <div id="category{{ $category->id }}" class="carousel slide white" data-ride="carousel">
                    <div class="carousel-inner">
                        @foreach ($category->last3() as $key => $ad)
                            @if($loop->first)
                                @foreach ($ad->photos as $key2 => $photo)
                                    @if($loop->first)
                                        <div class="carousel-item active" style="background-image: url('{{ Storage::url($photo->url) }}')">
                                            <div class="content">
                                                <h4><a href="{{ route('ads.ad', ['id' => $ad->id]) }}">{{ $ad->title }}</a></h4>
                                                <span>{{ number_format($ad->price, 0, '.', ' ') }}₽</span>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            @else
                                @foreach ($ad->photos as $key2 => $photo)
                                    @if($loop->first)
                                        <div class="carousel-item" style="background-image: url('{{ Storage::url($photo->url) }}')">
                                            <div class="content">
                                                <h4><a href="{{ route('ads.ad', ['id' => $ad->id]) }}">{{ $ad->title }}</a></h4>
                                                <span>{{ number_format($ad->price, 0, '.', ' ') }}₽</span>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            @endif
                        @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#category{{ $category->id }}" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#category{{ $category->id }}" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        @endforeach
    </div>
@endsection
