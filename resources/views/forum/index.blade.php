@extends('layouts.app')

@section('title')
    Новости
@endsection

@section('content')
    <div class="d-flex align-items-center justify-content-between mb-3">
        <h2 class="m-0">Форум</h2>@if(Request::get('search'))<small>Поиск: {{ Request::get('search') }}</small>@endif
        <a href="{{ route('forum.add') }}" class="button small red">Cоздать тему</a>
    </div>
    <div class="row forum mb-4">
        <div class="col-12">
            <div class="white">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Новые темы</a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Все темы</a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Архивные темы</a>
                    </li>
                </ul>
                <div class="search">
                    <form action="">
                        <input type="text" placeholder="Поиск по темам" name="search" @if(Request::get('search')) value="{{ Request::get('search') }}" @endif>
                    </form>
                </div>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        @foreach ($topics->where('status', 1)->sortByDesc('created_at')->take(10) as $key => $topic)
                            <div class="topic d-flex align-items-center justify-content-between">
                                <div class="left">
                                    <h3><a href="{{ route('forum.topic', ['id' => $topic->id]) }}">{{ $topic->title }}</a></h3>
                                    <div class="info d-flex">
                                        <span>{{ Lang::choice(':count сообщение|:count сообщения|:count сообщений', $topic->messages->count()) }}</span>
                                        <span>@if($topic->last) Последнее сообщение {{ $topic->last->created_at->diffForHumans() }} @endif</span>
                                    </div>
                                </div>
                                <div class="right d-none d-sm-flex">
                                    <div class="img">
                                        @if($topic->last)
                                            <img src="@if($topic->last->user && $topic->last->user->photo){{ Storage::url($topic->last->user->photo) }}@else{{ asset('images/noavatar.jpg') }}@endif" alt="">
                                        @else
                                            <img src="@if($topic->user && $topic->user->photo){{ Storage::url($topic->user->photo) }}@else{{ asset('images/noavatar.jpg') }}@endif" alt="">
                                        @endif
                                    </div>
                                    <div class="info">
                                        @if($topic->last)
                                            <span class="name">@if($topic->last->user){{ $topic->last->user->first_name }} {{ $topic->last->user->last_name }}@endif</span>
                                            <span class="date">{{ $topic->last->created_at->diffForHumans() }}</span>
                                        @else
                                            <span class="name">@if($topic->user){{ $topic->user->first_name }} {{ $topic->user->last_name }}@endif</span>
                                            <span class="date">{{ $topic->created_at->diffForHumans() }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        @foreach ($topics->where('status', 1)->sortByDesc(function ($topic, $key) {
  if($topic->last) {
      return $topic->last->created_at;
  } else {
      return $topic->created_at;
  }
}) as $key => $topic)
                            <div class="topic d-flex align-items-center justify-content-between">
                                <div class="left">
                                    <h3><a href="{{ route('forum.topic', ['id' => $topic->id]) }}">{{ $topic->title }}</a></h3>
                                    <div class="info">
                                        <span>{{ Lang::choice(':count сообщение|:count сообщения|:count сообщений', $topic->messages->count()) }}</span>
                                        <span>@if($topic->last) Последнее сообщение {{ $topic->last->created_at->diffForHumans() }} @endif</span>
                                    </div>
                                </div>
                                <div class="right d-none d-sm-flex">
                                    <div class="img">
                                        @if($topic->last)
                                            <img src="@if($topic->last->user && $topic->last->user->photo){{ Storage::url($topic->last->user->photo) }}@else{{ asset('images/noavatar.jpg') }}@endif" alt="">
                                        @else
                                            <img src="@if($topic->user && $topic->user->photo){{ Storage::url($topic->user->photo) }}@else{{ asset('images/noavatar.jpg') }}@endif" alt="">
                                        @endif
                                    </div>
                                    <div class="info d-flex">
                                        @if($topic->last)
                                            <span class="name">@if($topic->last->user){{ $topic->last->user->first_name }} {{ $topic->last->user->last_name }}@endif</span>
                                            <span class="date">{{ $topic->last->created_at->diffForHumans() }}</span>
                                        @else
                                            <span class="name">@if($topic->user){{ $topic->user->first_name }} {{ $topic->user->first_name }}@endif</span>
                                            <span class="date">{{ $topic->created_at->diffForHumans() }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                        @foreach ($topics->where('status', 0)->sortByDesc('last.created_at') as $key => $topic)
                            <div class="topic d-flex align-items-center justify-content-between">
                                <div class="left">
                                    <h3><a href="{{ route('forum.topic', ['id' => $topic->id]) }}">{{ $topic->title }}</a></h3>
                                    <div class="info">
                                        <span>{{ Lang::choice(':count сообщение|:count сообщения|:count сообщений', $topic->messages->count()) }}</span>
                                        <span>@if($topic->last) Последнее сообщение {{ $topic->last->created_at->diffForHumans() }} @endif</span>
                                    </div>
                                </div>
                                <div class="right d-none d-sm-flex">
                                    <div class="img">
                                        @if($topic->last)
                                            <img src="@if($topic->last->user && $topic->last->user->photo){{ Storage::url($topic->last->user->photo) }}@else{{ asset('images/noavatar.jpg') }}@endif" alt="">
                                        @else
                                            <img src="@if($topic->user && $topic->user->photo){{ Storage::url($topic->user->photo) }}@else{{ asset('images/noavatar.jpg') }}@endif" alt="">
                                        @endif
                                    </div>
                                    <div class="info d-flex">
                                        @if($topic->last)
                                            <span class="name">@if($topic->last->user){{ $topic->last->user->first_name }} {{ $topic->last->user->last_name }}@endif</span>
                                            <span class="date">{{ $topic->last->created_at->diffForHumans() }}</span>
                                        @else
                                            <span class="name">@if($topic->user){{ $topic->user->first_name }} {{ $topic->user->first_name }}@endif</span>
                                            <span class="date">{{ $topic->created_at->diffForHumans() }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
