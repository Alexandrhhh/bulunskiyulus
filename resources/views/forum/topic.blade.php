@extends('layouts.app')

@section('title')
    {{ $topic->title }} - Булунский портал
@endsection

@section('content')
    <div class="fulltopic mb-4">
        <div class="d-flex align-items-end mb-3 justify-content-between">
            <h2 class="m-0">{{ $topic->title }}</h2>
            <div class="breadcrumb">
                <a href='{{ route('index') }}'>Главная</a> / <a href='{{ route('forum.index') }}'>Форум</a> / <span class="current">{{ $topic->title }}</span>
            </div>
        </div>
        <div class="white topic topicstarter mb-4">
            <div class="img">
                <img src="@if($topic->user && $topic->user->photo){{ Storage::url($topic->user->photo) }}@else{{ asset('images/noavatar.jpg') }}@endif" alt="">
            </div>
            <div class="content">
                <div class="header">
                    <div class="name">@if($topic->user){{ $topic->user->first_name }} {{ $topic->user->last_name }}@endif</div>
                    <div class="date">{{ $topic->created_at->diffForHumans() }}</div>
                </div>
                <p>{{ $topic->message }}</p>
                <div class="likes">
                    <i class="like"></i> {{ $topic->rating }} <i class="dislike"></i>
                </div>
            </div>
        </div>
        @auth
            <form action="{{ route('forum.create_message', ['id' => $topic->id]) }}" method="post">
                @csrf
                <div class="white new mb-4">
                    <div class="header">Ваш комментарий</div>
                    <div class="content">
                        <div class="img">
                            <img src="@if(Auth::user()->photo){{ Storage::url(Auth::user()->photo) }}@else{{ asset('images/noavatar.jpg') }}@endif" alt="">
                        </div>
                        <textarea class="autosize" name="message" rows="1" cols="80" placeholder="Напишите ваш комментарий"></textarea>
                        <button type="submit" class="send mt-auto"></button>
                    </div>
                </div>
            </form>
        @endauth
        @foreach ($messages as $key => $message)
            <div class="white topic mb-4">
                <div class="img">
                    <img src="@if($message->user && $message->user->photo){{ Storage::url($message->user->photo) }}@else{{ asset('images/noavatar.jpg') }}@endif" alt="">
                </div>
                <div class="content">
                    <div class="header">
                        <div class="name">@if($message->user){{ $message->user->first_name }} {{ $message->user->last_name }}@endif</div>
                        <div class="date">{{ $message->created_at->diffForHumans() }}</div>
                    </div>
                    <p>{{ $message->message }}</p>
                </div>
            </div>
        @endforeach
    </div>
@endsection
