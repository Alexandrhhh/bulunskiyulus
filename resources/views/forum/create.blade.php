@extends('layouts.app')

@section('title')
    Новое обсуждение
@endsection

@section('content')
    <div class="fullad mb-4">
        <div class="d-flex align-items-end mb-3 justify-content-between">
            <h2 class="m-0">Новое обсуждение</h2>
            <div class="breadcrumb">
                <a href='{{ route('index') }}'>Главная</a> / <a href='{{ route('forum.index') }}'>Форум</a> / <span class="current">Новое обсуждение</span>
            </div>
        </div>
        <form action="{{route('forum.create')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label>Заголовок</label>
                <input type="text" name="title" class="form-control" placeholder="Введите заголовок" required>
            </div>
            <div class="form-group">
                <label>Ваше сообщение</label>
                <textarea name="message" class="form-control autosize" placeholder="" required></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Создать</button>
        </form>
    </div>
@endsection
