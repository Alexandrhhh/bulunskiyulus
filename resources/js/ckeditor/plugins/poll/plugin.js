CKEDITOR.plugins.add('poll',{
  init: function(editor){
    var cmd = editor.addCommand('poll', {
      exec:function(editor){
        // Выделяем весь контент на странице
        editor.focus();
        editor.document.$.execCommand( 'SelectAll', false, null );
        // Получаем целиком содержимое контейнера редактора
        var text = editor.getData();
        // Заменяем ё на е
        var res1 = text.replace(/ё/gi, "е");
        // Убираем лишние пробелы
        var res2 = res1.replace(/&nbsp;/gi, "");
        // Заменяем кавычки на "елочки"
        var res3 = res2.replace(/&quot;/gi, "«");
        var res4 = res3.replace(/«\s/gi, "» ");
        // Заменяем дефис на тире, при этом дефис в нужных местах не трогая
        var res5 = res4.replace(/\s-\s/gi, " — ");
        // Вставляем готовый тескт в заранее выделенную область (выделено все)
        editor.insertHtml(res5);
      }
    });
    editor.ui.addButton('poll',{
      label: 'Единое форматирование типографики для новостей',
      icon: this.path + 'icons/clock.png',
      command: 'poll'
    });
  }
});