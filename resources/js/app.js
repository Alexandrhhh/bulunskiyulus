require('./bootstrap');

require('select2');

require('owl.carousel');

require('air-datepicker');

require('jquery-mask-plugin');

require('datatables.net-bs4');
require('datatables.net-searchpanes-bs4');
import 'jquery-ui/ui/widgets/dialog.js';
import 'jquery-ui/ui/widgets/draggable.js';
import 'jquery-ui/ui/widgets/droppable.js';

import bsCustomFileInput from 'bs-custom-file-input'

import autosize from 'autosize';

require(['lightgallery'], function () {
	require(["lg-zoom", "lg-thumbnail", "lg-video", "lg-autoplay"], function () {
		$("#lightgallery").lightGallery();
	});
});

$(function() {
    $('body').on('click', 'header .arrow', function() {
        $(this).toggleClass('active');
    })
    $('body').on('click', 'header .search .icon', function() {
        $(this).parent().toggleClass('active');
    });
    var oTable = $('.datatable').DataTable({
        "ordering": false,    // Ordering (Sorting on Each Column)will Be Disabled
        "lengthChange": false, // Will Disabled Record number per page);
        "paging": false
    });
    $('#search').keyup(function(){
          oTable.search($(this).val()).draw() ;
    })
    $("#tags").select2({
        theme: "bootstrap4",
        tags: true,
    });
    $(".form-control.dropdown").select2({
        theme: "bootstrap4"
    });
    $(".input.dropdown").select2({
        theme: "form",
        minimumResultsForSearch: 10,
        "language": {
            "noResults": function(){
                return "Ничего не найдено";
            }
        },
    });
    $('.datepicker-here.date').datepicker({
        range: true,
        theme: "bootstrap4",
        toggleSelected: false,
        autoClose: true,
        onHide: function(dp, animationCompleted){
           if (animationCompleted) {
               $("#form-date").submit();
           }
       }
    });
    $(".input.tags").select2({
        theme: "form",
        tags: true,
        minimumInputLength: 3,
        placeholder: "Теги",
        "language": {
            "noResults": function(){
                return "Пока пусто";
            },
            "inputTooShort": function() {
                return "Введите больше символов"
            }
        },
    });
    $('.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        items: 1,
        navText: ['', ''],
        autoHeight: true,
    })
    autosize($('textarea.autosize'));

    $('body').on('change', '.form-check-input', function() {
        if($(this).is(':checked')) {
            $(this).prev('input').val(1);
        } else {
            $(this).prev('input').val(0);
        }
    });

    $('body').on('click', 'a[href="#copy"]', function() {
        $(".form-control.dropdown").select2('destroy');
        $(this).prev('.d-none').before($(this).prev().clone().toggleClass('d-none'));
        $('.form-group:not(.d-none) .datepicker-here').each(function() {
            if($(this).val() != '') {
                var date = $(this).val();
                date = date.split(' ');
                var first = date[0].split('.');
                var second = date[1].split(':');
                $(this).datepicker().data('datepicker').selectDate(new Date(first[2], first[1] - 1, first[0], second[0], second[1]));
            }
        });
        $('.datepicker-here').datepicker();
        $(".form-control.dropdown").select2({
            theme: "bootstrap4"
        });
    });

    $('body').on('change', '.poll input:radio', function() {
        $('.button.btn-poll').show(150);
    });

    $('body').on('change', '.poll input:checkbox', function() {
        if($('.poll input:checkbox:checked').length > 0) {
            $('.button.btn-poll').show(150);
        } else {
            $('.button.btn-poll').hide(150);
        }
    });

    $('body').on('click', '.btn-poll', function() {
        send_answer();
    });

    bsCustomFileInput.init();

    $('.phone').mask('+7 (000) 000 00 00', {placeholder: "+7 (___) ___ __ __"});
    // $('a[href="#openDescription"]').each(function() {
    //     if($(this).prev('.description').height() < 60) {
    //         $(this).toggleClass('d-none');
    //         $(this).prev('.description').toggleClass('mb-3');
    //     }
    // });
    // $('a[href="#openDescription"]').on('click', function() {
    //     $(this).prev('.description').toggleClass('open');
    //     if($(this).prev('.description').hasClass('open')) {
    //         $(this).html('Скрыть');
    //     } else {
    //         $(this).html('Показать полностью');
    //     }
    // })

    $('body').on('click', '.icon.like.click', function() {
        var i = Number($(this).next('span').text());
        if($(this).hasClass('active')) {
            i--;
            $(this).next('span').html(i);
        } else {
            i++;
            $(this).next('span').html(i);
        }
        addLike($(this).data('id'), $(this).data('class'), $(this).data('user'));
        $(this).toggleClass('active');
	});

	$(".nav-list").on("click", "a.button", function() {
		$(".nav-list li.active").removeClass("active");
		$(".nav-list a.button").each(function(){
			$(this).text('Выбрать');
			$(this).removeClass('active');
		});
		$(this).text('Выбран');
		$(this).addClass('active');
        $(this)
            .parent()
			.addClass("active");
		return false;
	});
	$(".radio").on("click", function() {
		$(".radio.active").each(function() {
			$(this).removeClass('active');
		});
		$(this).addClass("active");
	})
})

function send_answer() {
    var form = new FormData(document.getElementById('poll'));
    axios.post('/api/poll', form)
    .then(function (response) {
        $('#poll').replaceWith(response.data);
    })
}

function addLike(id, model, user) {
    axios.post('/api/like', {id: id, class:model, user: user});
}
// jQuery.fn.load = function(callback){ $(window).on("load", callback) };
