const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js("resources/js/app.js", "public/js")
    .sass("resources/sass/app.scss", "public/css")
    .sass("resources/sass/cabinet.scss", "public/css")
    .copyDirectory("resources/js/ckeditor", "public/js/ckeditor")
    .copyDirectory("resources/js/photolabel", "public/js/photolabel")
    .copyDirectory("resources/images/icons/gm", "public/images/gm")
    .copyDirectory("resources/images/icons/filetype", "public/images/filetype")
    .copyDirectory("resources/js/gallery", "public/js/gallery")
    .sourceMaps();
if (mix.inProduction()) {
    mix.version();
}
